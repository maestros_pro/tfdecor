
const path = require('path');
const fs = require('fs');
const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const autoprefixer = require('autoprefixer');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const ENV = process.env.NODE_ENV;
const LOCAL = ENV === 'local';
const DEV = ENV === 'development';
const PROD = ENV === 'production';

const PATH = {
	src: path.resolve(__dirname, 'frontend/admin'),
	dist: path.resolve(__dirname, 'assets/admin'),
};

const config = {

	entry: ["@babel/polyfill", PATH.src + '/app.js'],

	output: {
		path: PATH.dist,
		filename: 'js/script.js'
	},

	stats: {
		colors: true,
		errorDetails: true,
	},

	mode: ENV === 'local' ? 'development' : 'production',

	devServer: {
		contentBase: PATH.dist,
		compress: true,
		hot: true,
		inline: true,
		open: true,
		historyApiFallback: true
	},

	plugins: [
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery'
		}),
		new webpack.DefinePlugin({
			'isLocal': JSON.stringify(LOCAL),
			'isDev': JSON.stringify(DEV),
			'isProd': JSON.stringify(PROD),
			'process.env': {
				NODE_ENV: JSON.stringify(ENV)
			}
		}),
		new MiniCssExtractPlugin({
			filename: 'css/style.css',
			chunkFilename: '[id].css'
		}),
		new CopyWebpackPlugin([
			// { from: `${PATH.src}/fonts`, to: `${PATH.dist}/fonts`},
			// { from: `${PATH.src}/img`, to: `${PATH.dist}/img`},
			{ from: `${PATH.src}/data`, to: `${PATH.dist}/data`},
			// { from: `${PATH.src}/robots.txt`, to: `${PATH.dist}/`}
		]),
		new HtmlWebpackPlugin({
			template: PATH.src + '/index.pug',
			inject: 'body'
		}),
		new VueLoaderPlugin()
	],

	module:  {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				options: {
					// postcss: [require('postcss-cssnext')()],
					// options: {
					//     extractCSS: true
					// },
					loaders: {
						// js: 'babel-loader'
					}
				}
			},
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env']
					}
				}
			},
			{
				test: /\.pug$/,
				oneOf: [
					// это применяется к `<template lang="pug">` в компонентах Vue
					{
						resourceQuery: /^\?vue/,
						use: ['pug-plain-loader']
					},
					// это применяется к файлам pug
					{
						resourceQuery: /^\?pug$/,
						use: ['pug-loader']
					},
					// это применяется к импортам pug внутри JavaScript
					{
						use: ['raw-loader', 'pug-plain-loader']
					}
				]
			},
			{
				test: /\.(gif|png|jpe?g|svg)$/i,
				include: PATH.src + '/img/',
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							outputPath: 'img/',
							useRelativePath: true
						}
					}, {
						loader: 'image-webpack-loader',
						options: {
							mozjpeg: {
								progressive: true,
								quality: 70
							}
						}
					}
				]
			},
			{
				test: /\.scss$/,
				exclude: /node_modules/,
				use: [
					LOCAL ? 'style-loader' : MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader'
					},
					{
						loader: 'postcss-loader',
						options: {
							plugins: [
								autoprefixer({
									browsers:['ie >= 8', 'last 4 version']
								})
							],
							sourceMap: true
						}
					},
					'sass-loader',
					'resolve-url-loader'
				]
			},
			{
				test: /\.(eot|svg|ttf|woff|woff2)$/,
				// include: PATH.src + '/fonts/',
				use: {
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: LOCAL ? '' : '../',
						useRelativePath: true
					}
				}
			},
			{
				test: /\.json/,
				use: {
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: 'data/',
						useRelativePath: true
					}
				}
			}
		]
	},

	optimization: PROD ? {
		minimizer: [
			new UglifyJsPlugin({
				sourceMap: true,
				uglifyOptions: {
					//ecma: 5,
					compress: {
						inline: true,
						warnings: false,
						drop_console: PROD,
						unsafe: true
					},
				},
			}),
			new OptimizeCSSAssetsPlugin({
				cssProcessorPluginOptions: {
					preset: ['default', { discardComments: { removeAll: true } }],
				},
			})
		],
	} : {}
};

module.exports = config;