let prodUrl = '/admin';

export default {
	/**
	 * authorization
	 */
	register: {
		method: 'register',
		customData: {},
		url: isLocal ? '/data/register.json' : prodUrl
	},
	login: {
		method: 'login',
		customData: {},
		url: isLocal ? '/data/register.json' : prodUrl
	},
	logout:  {
		method: 'logout',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},


	/**
	 * Project
	 */

	createProject:  {
		method: 'createProject',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},

	updateProject:  {
		method: 'updateProject',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},

	getProject:  {
		method: 'getProject',
		customData: {},
		url: isLocal ? '/data/getProject.json' : prodUrl
	},

	listProjects:  {
		method: 'listProjects',
		customData: {count: 0, offset: 0},
		url: isLocal ? '/data/listProject.json' : prodUrl
	},

	deleteProject:  {
		method: 'deleteProject',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},

	/**
	 * brands
	 */


	addBrand:  {
		method: 'addBrand',
		customData: {},
		url: isLocal ? '/data/addBrand.json' : prodUrl
	},
	updateBrand:  {
		method: 'updateBrand',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},
	getBrand:  {
		method: 'getBrand',
		customData: {},
		url: isLocal ? '/data/getBrand.json' : prodUrl
	},
	listBrands:  {
		method: 'listBrands',
		customData: {},
		url: isLocal ? '/data/listBrand.json' : prodUrl
	},
	deleteBrand:  {
		method: 'deleteBrand',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},

	/**
	 * tags
	 */

	listTags:  {
		method: 'listTags',
		customData: {},
		url: isLocal ? '/data/listTags.json' : prodUrl
	},
	getTag:  {
		method: 'getTag',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},
	addTag:  {
		method: 'addTag',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},
	updateTag:  {
		method: 'updateTag',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},
	deleteTag:  {
		method: 'deleteTag',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},

	/**
	 * equipment
	 */


	listEquipment:  {
		method: 'listEquipment',
		customData: {count: 0, offset: 0},
		url: isLocal ? '/data/listEquipment.json' : prodUrl
	},
	getEquipment:  {
		method: 'getEquipment',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},
	createEquipment:  {
		method: 'createEquipment',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},
	updateEquipment:  {
		method: 'updateEquipment',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},
	deleteEquipment:  {
		method: 'deleteEquipment',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},

	/**
	 * news
	 */

	listNews:  {
		method: 'listNews',
		customData: {count: 0, offset: 0},
		url: isLocal ? '/data/listNews.json' : prodUrl
	},
	getNew:  {
		method: 'getNew',
		customData: {},
		url: isLocal ? '/data/getNew.json' : prodUrl
	},
	createNew:  {
		method: 'createNew',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},
	updateNew:  {
		method: 'updateNew',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},
	deleteNew:  {
		method: 'deleteNew',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},

	/**
	 * users
	 */

	listUsers:  {
		method: 'listUsers',
		customData: {},
		url: isLocal ? '/data/listUsers.json' : prodUrl
	},

	getUser:  {
		method: 'getUser',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},

	addUser:  {
		method: 'addUser',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},

	updateUser:  {
		method: 'updateUser',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},

	deleteUser:  {
		method: 'deleteUser',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},

	/**
	 * feedback
	 */

	listRequestStatuses:  {
		method: 'listRequestStatuses',
		customData: {},
		url: isLocal ? '/data/listRequestStatuses.json' : prodUrl
	},

	getRequest:  {
		method: 'getRequest',
		customData: {},
		url: isLocal ? '/data/getRequest.json' : prodUrl
	},

	listRequests:  {
		method: 'listRequests',
		customData: {count: 0, offset: 0},
		url: isLocal ? '/data/listRequests.json' : prodUrl
	},

	setRequestExecutor:  {
		method: 'setRequestExecutor',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},

	setRequestResult:  {
		method: 'setRequestResult',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},

	deleteRequest:  {
		method: 'deleteRequest',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},

	/**
	 * check Slug
	 */

	checkSlug:  {
		method: 'checkSlug',
		customData: {section: '', slug: ''},
		url: isLocal ? '/data/login.json' : prodUrl
	}

};