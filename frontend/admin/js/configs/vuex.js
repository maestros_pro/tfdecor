import Vue from 'vue'
import Vuex from 'vuex';
import _ from 'lodash';

Vue.use(Vuex);



export default new Vuex.Store({
	state: {
		loadData: 0,

		brands: [],

		tags: [],

		directions: [],

		feedbacks: [],

		tagDirectionActual: [],

		tagDirection: [
			{
				value: 'news',
				label: 'Новости',
				color: '#2e9fce'
			},
			{
				value: 'equipment',
				label: 'Оборудование',
				color: '#d1b029'
			},
			{
				value: 'projects',
				label: 'Проекты',
				color: '#9b51a5'
			},
			{
				value: 'brands',
				label: 'Бренды',
				color: '#138f2a'
			}
		],

		aside: {
			show: false,
			compact: false,
		},

		popup: {
			name: null,
			data: null
		},

		loading: false,

		user: {
			info: {}
		},

	},

	mutations: {
		// popups
		popup (state, options) {

			if (options === 'close'){
				state.popup.name = null;
				state.popup.data = null;
				document.documentElement.classList.remove('is-popup-open');
			}
			else if ( typeof options === 'string') {
				state.popup.name = options;
			} else {
				state.popup.name = options.name;
				state.popup.data = options.data;
			}
			if (name !== null) document.documentElement.classList.add('is-popup-open');
		},

		aside (state){
			state.aside = !state.aside;
		},

		loading (state, event){
			if ( event === 'start') {
				state.loading = true;
			} else if ( event === 'end') {
				state.loading = false;
			}
		},

		loadTags(state, list){
			state.tags = list;

			let tags = {};
			state.tagDirectionActual = [];

			for (let i = 0; i < list.length; i++){
				if (!tags[list[i].section]){
					tags[list[i].section] = true;
					state.tagDirectionActual.push(list[i].section);
				}
			}
			state.loadData++;
		},

		loadBrands(state, list){
			state.brands = list;
			state.loadData++;
		},

		loadDirections(state, list){
			state.directions = list;
			state.loadData++;
		},

		loadUsers(state, list){
			state.users = list;
			state.loadData++;
		},

		loadFeedbackInfo(state, list){
			state.feedbacks = list;
			state.loadData++;
		},
	},

	actions: {
		loadTags(context){
			window.app.$api.listTags({},
				(res)=>{
					context.commit('loadTags', res.data.list);
				}, ()=>{
					context.commit('loadTags', []);
				}
			);
		},
		loadBrands(context){
			window.app.$api.listBrands({},
				(res)=>{
					context.commit('loadBrands', res.data.list);
				}, ()=>{
					context.commit('loadBrands', []);
				}
			);
		},
		loadFeedbackInfo(context){
			window.app.$api.listRequestStatuses({},
				(res)=>{
					context.commit('loadFeedbackInfo', res.data.list);
				}, ()=>{
					context.commit('loadFeedbackInfo', []);
				}
			);
		},
		loadUsers(context){
			window.app.$api.listUsers({},
				(res)=>{
					context.commit('loadUsers', res.data.list);
				}, ()=>{
					context.commit('loadUsers', []);
				}
			);
		}
	},

	getters: {
		getPopupName: state => state.popup.name,
		getPopupData: state => state.popup.data,

		getTags: state => section => {
			if ( section ) return _.filter(state.tags, (e)=>e.section === section);
			else return state.tags;
		},

		getTagItem: state => (key, value) => {
			if ( value ) return _.find(state.tags, (e)=>e[key] === value.toString());
			else return state.directions;
		},

		getBrand: state => id => {
			return _.find(state.brands, (e)=>e.id === id);
		},

		getFeedbackStatus: state => id => {
			return id ? _.find(state.feedbacks, (e)=>e.id === id) : {};
		},

		getUser: state => id => {
			// console.info(state.users, id);
			return id ? _.find(state.users, (e)=>e.id === id) : {};
		},

		getDataLoaded: state => {
			return state.loadData >= ( state.user.info.level < 1 ? 5 : 2 ) ;
		},

		getDirection: state => (key, value) => {
			if ( value ) return _.find(state.directions, (e)=>e[key] === value.toString());
			else return state.directions;
		},

		getTagDirection: state => (key, value) => {
			if ( value ) return _.find(state.tagDirection, (e)=>e[key] === value);
			else return state.tagDirection;
		}
	}
});



