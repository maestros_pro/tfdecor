import Vue from 'vue'
import Router from 'vue-router'

import Home from '../../view/home.vue';
import News from '../../view/news.vue';
import NewsItem from '../../view/news-item.vue';
import Projects from '../../view/projects.vue';
import ProjectItem from '../../view/project-item.vue';
import Equipments from '../../view/equipments.vue';
import EquipmentItem from '../../view/equipment-item.vue';
import Brands from '../../view/brands.vue';
import BrandItem from '../../view/brand-item.vue';
import Tags from '../../view/tags.vue';
import TagsItem from '../../view/tags-item.vue';
import Users from '../../view/users.vue';
import UsersItem from '../../view/user-item.vue';
import Feedback from '../../view/feedback.vue';
import FeedbackItem from '../../view/feedback-item.vue';

import Error from '../../view/error.vue';


import Components from '../../view/components.vue';


Vue.use(Router);

const routes = [
	{
		path: '/',
		component: Home,
		name: 'home',
		meta: {
			title: 'Главная'
		}
	},

	{
		path: '/news',
		component: News,
		name: 'news',
		meta: {
			title: 'Новости',
			group: 'news'
		},
	},
	{
		path: '/news/add',
		component: NewsItem,
		name: 'newsAdd',
		meta: {
			title: 'Добавить новость',
			group: 'news',
			create: true
		},
	},

	{
		path: '/news/:id',
		component: NewsItem,
		name: 'newsItem',
		meta: {
			title: 'Новость',
			group: 'news'
		}
	},

	{
		path: '/projects',
		component: Projects,
		name: 'projects',
		meta: {
			title: 'Проекты',
			group: 'projects'
		},
	},

	{
		path: '/projects/add',
		component: ProjectItem,
		meta: {
			title: 'Добавить проект',
			group: 'projects',
			create: true
		},
	},
	{
		path: '/projects/:id',
		component: ProjectItem,
		meta: {
			title: 'Проект',
			group: 'projects'
		},
	},
	{
		path: '/equipments',
		component: Equipments,
		name: 'equipments',
		meta: {
			title: 'Оборудование',
			group: 'equipments'
		}
	},
	{
		path: '/equipments/add',
		component: EquipmentItem,
		name: 'equipmentAdd',
		meta: {
			title: 'Добавить оборудование',
			group: 'equipments',
			create: true
		}
	},
	{
		path: '/equipments/:id',
		component: EquipmentItem,
		name: 'equipmentItem',
		meta: {
			title: 'Оборудование',
			group: 'equipments'
		}
	},

	{
		path: '/brands',
		component: Brands,
		name: 'brands',
		meta: {
			title: 'Бренды',
			group: 'brands'
		}
	},
	{
		path: '/brands/add',
		component: BrandItem,
		name: 'brandAdd',
		meta: {
			title: 'Добавить бренд',
			group: 'brands',
			create: true
		}
	},
	{
		path: '/brands/:id',
		component: BrandItem,
		name: 'brandItem',
		meta: {
			title: 'Бренд',
			group: 'brands'
		}
	},

	{
		path: '/tags',
		component: Tags,
		name: 'tags',
		meta: {
			title: 'Теги',
			group: 'tags'
		}
	},
	{
		path: '/tags/add',
		component: TagsItem,
		name: 'tagAdd',
		meta: {
			title: 'Добавить тег',
			group: 'tags',
			create: true
		}
	},
	{
		path: '/tags/:id',
		component: TagsItem,
		name: 'tagItem',
		meta: {
			title: 'Тег',
			group: 'tags'
		}
	},

	{
		path: '/users',
		component: Users,
		name: 'users',
		meta: {
			title: 'Сотрудники',
			group: 'users'
		}
	},
	{
		path: '/users/add',
		component: UsersItem,
		name: 'userAdd',
		meta: {
			title: 'Добавить сотрудника',
			group: 'users',
			create: true
		}
	},
	{
		path: '/users/:id',
		component: UsersItem,
		name: 'userItem',
		meta: {
			title: 'Пользователи',
			group: 'users'
		}
	},

	{
		path: '/feedback',
		component: Feedback,
		name: 'feedback',
		meta: {
			title: 'Заявки',
			group: 'feedback'
		}
	},
	{
		path: '/feedback/:id',
		component: FeedbackItem,
		name: 'feedbackItem',
		meta: {
			title: 'Заявка',
			group: 'feedback'
		}
	},

	{
		path: '/components',
		component: Components,
		name: 'components',
		meta: {
			title: 'Сomponents example',
			group: 'components'
		}
	},
	{
		path: '*/*',
		component: Error,
		name: 'error',
		meta: {
			title: 'Страница не существует'
		}
	}
];

const router = new Router({
	base: isLocal ? '/' : 'admin/',
	mode: 'history',
	routes,
	linkActiveClass: '',
	linkExactActiveClass: 'is-active',
	transitionOnLoad: true,
	root: '/'
});


router.afterEach((to, from) => {
	document.title = to.meta.title;
});

export default router