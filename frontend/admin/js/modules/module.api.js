import axios from 'axios'
import rules from '../configs/request'

let request = (url, data, response, error, always)=>{
	if (isLocal) console.info('request', data);

	return axios({
			method: isLocal ? 'get' : 'post',
			url: url,
			headers: {
				'Content-Type': 'multipart/form-data'
				// 'Content-type': 'application/x-www-form-urlencoded'
			},
			transformRequest: [function (data) {

				let formData = new FormData();

				Object.keys(data).map(function(key) {
					if (Array.isArray(data[key])) {

						for (let i = 0; i < data[key].length; i++) {
							formData.append(`${key}[]`, data[key][i]);
						}

					} else {
						formData.append(key, data[key]);
					}
				});

				return formData;
			}],
			data: data
		}).then(response).catch(error).then(always);
	},

	methods = ()=>{
		let data = {};

		Object.keys(rules).map((key)=>{
			data[key] = (d, res, err, alw)=>{
				let obj = rules[key].customData || {};
				obj.request = rules[key].method;

				// console.info();

				if ( !!window.app.$store ) window.app.$store.commit('loading', 'start');

				request(rules[key].url, Object.assign(obj, d),
					e => {
						// console.info(key, e);

						console.info(e.data, typeof e.data);


						if ( e.data.message && window.app && window.app.$notify ){
							window.app.$notify({
								title: e.data.message,
								type: e.data.status ? 'success' : 'error' // success warn error
							})
						}
						res(e);
					},
					e => {
						if ( window.app && window.app.$notify ){
							window.app.$notify({
								title: 'Ошибка загрузки',
								text: err,
								type: 'error'
							});
						}
						if (err && typeof err === 'function') err(e);
					},
					e => {
						if (alw && typeof alw === 'function') alw(e);
						if ( !!window.app.$store ) window.app.$store.commit('loading', 'end');
					}
				);
			};
		});

		return data;	// apiMethod(data, response, error, always);
	}
;

export default {
	install: (Vue, options)=>{
		Vue.prototype.$api = methods();
	}
};
