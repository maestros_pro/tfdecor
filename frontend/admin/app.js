import style from './css/style.scss';

import './img/svg/logo.svg';

// import rangy from 'rangy';
// import "rangy/lib/rangy-highlighter";
// import "rangy/lib/rangy-classapplier";
// import "rangy/lib/rangy-textrange";
// import "rangy/lib/rangy-serializer";
// Object.defineProperty(Vue.prototype, '$rangy', { value: rangy });

try{console.log('%cdeveloped by %c soft.maestros.ru ','font:bold 12px/18px monospace;color:green','font:12px/18px monospace;color:white;background-color:green;border-radius:3px;');}catch(e){}

import Vue from 'vue';
import router from './js/configs/router.js'
import store from './js/configs/vuex.js'
import './js/configs/filters.js'

import './js/modules/module.helper.js'
import Notifications from 'vue-notification'
import velocity from 'velocity-animate'
Vue.use(Notifications, { velocity });

import VueScroll from 'vuescroll';
Vue.use(VueScroll, {
	ops: {
		rail: {
			background: '#cccccc',
			opacity: 0,
			size: '6px',
			specifyBorderRadius: false,
			gutterOfEnds: '5px',
			gutterOfSide: '10px',
			keepShow: false
		},
		bar: {
			showDelay: 500,
			onlyShowBarOnScroll: true,
			keepShow: true,
			background: '#cccccc',
			opacity: 1
		}
	}

});


import Api from './js/modules/module.api'
Vue.use(Api, window.app);

import VTooltip from 'v-tooltip'
Vue.use(VTooltip);


import index from './view/index.vue';

window.app = new Vue({
	el: '#app',
	filters: {},
	router,
	store,
	navbar: false,
	render: h => h(index)
});
