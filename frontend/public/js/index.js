import '../css/style.scss'

import 'slick-carousel'
import 'selectize'
import './modules/module.form'
import './modules/module.app'
import './modules/module.helper'
import BrowserDetect from './modules/module.browserdetect'
import 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min'

import 'lightgallery'

try{console.log('%cdeveloped by %c soft.maestros.ru ','font:bold 12px/18px monospace;color:green','font:12px/18px monospace;color:white;background-color:green;border-radius:3px;');}catch(e){}

$(()=>{

	$('.scrollable').mCustomScrollbar();

	let $b = $('body'), app = {},
	browser = new BrowserDetect();

	$('html').addClass(`browser-${browser.browserInfo.name}`);

	console.info(browser);

	if ('ontouchstart' in window) $b.addClass('is-touch');

	$b
		.on('mousedown', (e)=>{
			if ( $('.show-search').length ){
				if ( !$(e.target).closest('.header__search').length ) $('.show-search').removeClass('show-search');
			}
		})
		.on('click', '[data-scroll]', function(e){
			e.preventDefault();
			$('html,body').animate({scrollTop: $($(this).attr('data-scroll')).offset().top - 100}, 500);
		})
		.on('click', '.header__search-ico', function(e){
			let $t = $(this),
				$form = $t.closest('form'),
				$header = $t.closest('.header')
			;

			if ( $header.hasClass('show-search') && $.trim($form.find('input').val()) ){
				$form.submit();
			} else {
				$header.addClass('show-search').find('input').focus();
			}

		})
		.on('click', '.header__call', function(e){
			if (browser.browserInfo.desktop && window.breakpoint === 'desktop'){
				e.preventDefault();
				window.popup.open('.popup_feedback');
			}
		})
	;




	$('.welcome__carousel').each(function () {
		let $t = $(this),
			$slider = $t.find('.welcome__carousel-list')
		;


		$slider
			.on('init', function (event, slick) {

			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

			})
			.on('afterChange', function () {

			})
			.on('lazyLoaded', function (event, slick, image, imageSource) {
				//-console.info(image,imageSource);

				$(image).closest('._backgroundimage').css({
					backgroundImage: `url(${imageSource})`
				})

			})
			.slick({
				infinite: true,
				lazyLoad: 'progressive',
				speed: 800,
				swipe: false,
				swipeToSlide: false,
				arrows: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				prevArrow: $t.find('._goprev'),
				nextArrow: $t.find('._gonext'),
				fade: true,
				dots: true
			});

	});


	$('.partners').each(function () {
		let $t = $(this),
			$slider = $t.find('.partners__list')
		;

		$slider
			.on('init', function (event, slick) {

			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

			})
			.on('afterChange', function () {

			})
			.slick({
				infinite: true,
				speed: 300,
				// variableWidth: true,
				swipe: true,
				swipeToSlide: true,
				arrows: true,
				slidesToShow: 8,
				slidesToScroll: 1,
				prevArrow: '<svg class="slick-arrow slick-prev" width="21" height="38" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 38"><path class="cls-1" d="M21.008,2.041L4.081,19.069l16.8,16.9L18.867,38,0,19.092l0.035-.035L0,19.021,18.979,0Z"/></svg>',
				nextArrow: '<svg class="slick-arrow slick-next" width="21" height="38" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 38"><path d="M-0.008,35.959L16.919,18.931l-16.8-16.9L2.133,0,21,18.908l-0.035.035L21,18.979,2.021,38Z"/></svg>',
				responsive: [
					{
						breakpoint: 1400,
						settings: {
							slidesToShow: 6
						}
					},
					{
						breakpoint: 1060,
						settings: {
							slidesToShow: 3
						}
					}
				]
			});

	});


	$('.article__equipment').each(function () {
		let $t = $(this),
			$thumbnail = $t.find('.article__equipment-thumbnail'),
			$slider = $t.find('.article__equipment-carousel')
		;

		$slider
			.on('init', function (event, slick) {

			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
				$thumbnail.removeClass('is-active');
				$thumbnail.eq(nextSlide).addClass('is-active');
			})
			.on('afterChange', function () {

			})
			.slick({
				infinite: true,
				speed: 300,
				fade: true,
				swipe: true,
				swipeToSlide: true,
				arrows: false,
				slidesToShow: 1,
				slidesToScroll: 1
			});

		$thumbnail.on('click', function () {
			$slider.slick('slickGoTo', $(this).index());
		})

	});

	$('.article__carousel').each(function () {
		let $slider = $(this),
			l = $slider.find('.article__carousel-item').length
		;

		$slider
			.on('init', function (event, slick) {
			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

			})
			.on('afterChange', function () {

			})
			.on('breakpoint', ()=>{
				gallery();
			})
			.slick({
				infinite: false,
				speed: 300,
				// variableWidth: true,
				swipe: true,
				swipeToSlide: true,
				arrows: true,
				slidesToShow: 3,
				slidesToScroll: 1,
				prevArrow: '<svg class="slick-arrow slick-arrow_prev" width="21" height="38" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 38"><path class="cls-1" d="M21.008,2.041L4.081,19.069l16.8,16.9L18.867,38,0,19.092l0.035-.035L0,19.021,18.979,0Z"/></svg>',
				nextArrow: '<svg class="slick-arrow slick-arrow_next" width="21" height="38" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 38"><path d="M-0.008,35.959L16.919,18.931l-16.8-16.9L2.133,0,21,18.908l-0.035.035L21,18.979,2.021,38Z"/></svg>',
				responsive: [
					{
						breakpoint: 1060,
						settings: {
							slidesToShow: 1
						}
					}
				]
			});

		gallery();
		function gallery() {

			$slider.lightGallery({
				counter: false,
				download: false,
				loop: false,
				hideControlOnEnd: true,
				selector: '.article__carousel-item'
			})
				.on('onBeforeSlide.lg', (e, p, i)=>{ arrow(i) })
				.on('onAfterSlide.lg', (e, p, i)=>{ arrow(i) })
			;
		}

		function arrow(i){
			$('.lg-actions .lg-icon').removeClass('disabled');
			if ( i === 0 ){
				$('.lg-actions .lg-prev').addClass('disabled');
			} else if (i === l - 1){
				$('.lg-actions .lg-next').addClass('disabled');
			}
		}
	});


	$('.project__gallery').each(function () {
		let $t = $(this),
			$slider = $t.find('.project__gallery-list'),
			$preview = $t.find('.project__gallery-preview'),
			list = []
		;

		$slider
			.on('init', function (event, slick) {

			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

			})
			.on('afterChange', function () {

			})
			.on('lazyLoaded', function () {

			})
			.slick({
				slidesToShow: 1,
				lazyLoad: 'progressive',
				slidesToScroll: 1,
				arrows: true,
				fade: true,
				prevArrow: '<svg class="slick-arrow slick-arrow_prev" width="21" height="38" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 38"><path class="cls-1" d="M21.008,2.041L4.081,19.069l16.8,16.9L18.867,38,0,19.092l0.035-.035L0,19.021,18.979,0Z"/></svg>',
				nextArrow: '<svg class="slick-arrow slick-arrow_next" width="21" height="38" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 38"><path d="M-0.008,35.959L16.919,18.931l-16.8-16.9L2.133,0,21,18.908l-0.035.035L21,18.979,2.021,38Z"/></svg>',
				adaptiveHeight: true,
				asNavFor: $preview
			});

		$preview
			.slick({
				slidesToShow: 5,
				slidesToScroll: 1,
				swipe: true,
				swipeToSlide: true,
				infinite: true,
				asNavFor: $slider,
				centerMode: false,
				focusOnSelect: true,
				variableWidth: true
			});


	});





	$('.form__input select').selectize({
		sortField: 'field',
		create: true,
		createOnBlur: true,
		onInitialize: function () {
			let autocomplete = $(this.$input).attr('data-autocomplete');

			if ( !autocomplete ){
				$(this.$control_input).prop('readonly', true);
				console.info(this.$control_input);
			}

		},
		onFocus: function(e){
			$(this.$input).closest('.form__field').addClass('f-focus');
		},
		onBlur: function(e){
			$(this.$input).closest('.form__field').removeClass('f-focus');

			if ( this.items.length ){
				$(this.$input).closest('.form__field').addClass('f-filled');
			} else {
				$(this.$input).closest('.form__field').removeClass('f-filled');
			}

		},
		onChange: function(value){
			console.info(this);

			let $select = $(this.$input);

			if ( value !== '' ){
				$select.closest('.form__field').addClass('f-filled');
			} else {
				$select.closest('.form__field').removeClass('f-filled');
			}

		}
	});




	$('.openmap').lightGallery({
		counter: false,
		download: false,
		loop: false,
		hideControlOnEnd: true,
		selector: 'this'
	});

	headerPos();
	window.addEventListener('scroll', headerPos);
	function headerPos() {
		if (window.pageYOffset > 30){
			$b.addClass('is-fall');
		} else {
			$b.removeClass('is-fall');
		}
	}


});

