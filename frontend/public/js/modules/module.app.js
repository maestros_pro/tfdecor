import anime from 'animejs'
import scrollmonitor from 'scrollmonitor'
import Popup from './module.popup'
import ViewPort from "./module.viewport";
import styles from './google.map.styles'
import objectFitImages from 'object-fit-images';

/*setTimeout(()=>{
	$('.ribbon').each((e, el)=>{
		let ribbon = scrollmonitor.create(el);
		ribbon.enterViewport(()=>{
			$(el).addClass('is-animated');
		});
		if (ribbon.isInViewport) $(el).addClass('is-animated');
	});
}, 1000);*/

let app = {

	contact: {

		el: {
			map: null,
			marker: null,
		},

		current: {
			city: 'spb',
			item: 0,
			busy: false
		},

		adjustment: {
			zoom: 15,
		},

		map: {

		},

		setMap: (city, item)=>{

			if (!document.querySelectorAll('.contact__map-layout')[0] || app.contact.current.busy) return false;

			if ( !city ){
				city = app.contact.current.city;
				item = app.contact.current.item;
			}

			let $tab = $('[data-map-city]'),
				$infoWrap = $('.contact__info'),
				$infoWrapInner = $('.contact__info').find('.inner'),
				$info = $('[data-map-info]'),
				$control = $('[data-map-control]'),
				$btn = $control.filter(`[data-map-control="${city}"]`).find('[data-map-item]'),
				stay = false
			;

			$tab
				.removeClass('is-active')
				.filter(`[data-map-city="${city}"]`)
				.addClass('is-active')
			;

			$control
				.removeClass('is-active')
				.filter(`[data-map-control="${city}"]`)
				.addClass('is-active')
			;

			if ( !item ){
				item = $btn.filter('.is-active').attr('data-map-item');
				stay = true;
			}

			app.contact.current.city = city;
			app.contact.current.item = item;


			$infoWrap.addClass('is-hide');
			app.contact.current.busy = true;

			setTimeout(()=>{
				$infoWrap.height($infoWrapInner.outerHeight());
				$info
					.removeClass('is-active')
					.filter(`[data-map-info="${city}[${item}]"]`)
					.addClass('is-active')
				;

				$infoWrap.animate({height: $infoWrapInner.outerHeight()}, ()=>{
					$infoWrap.removeAttr('style');
				});

				$infoWrap.removeClass('is-hide');
				app.contact.current.busy = false;

				let coord = $btn.filter(`[data-map-item="${item}"]`).attr('data-map-coord').split(',');

				console.info(coord);
				app.contact.el.marker.setMap(null);
				app.contact.el.marker = new google.maps.Marker({
					position: new google.maps.LatLng(coord[0], coord[1]),
					map: app.contact.el.map,
					icon: {
						url: isLocal ? '../img/logo.svg' : 'assets/img/logo.svg',
						scaledSize: new google.maps.Size(66, 45)
					}
				});
				app.contact.el.map.setZoom(app.contact.adjustment.zoom);
				app.contact.el.map.panTo(new google.maps.LatLng(coord[0], parseFloat(coord[1]) - ( window.breakpoint === 'desktop' ? 0.01 : 0)))

			}, 500);


			if ( !stay ){
				$btn
					.removeClass('is-active')
					.filter(`[data-map-item="${item}"]`)
					.addClass('is-active')
				;
			}

		},

		init: ()=>{

			if (!document.querySelectorAll('.contact__map-layout')[0]) return false;

			let coord = $('[data-map-control]')
				.filter('.is-active')
				.find('[data-map-item]')
				.filter('.is-active')
				.attr('data-map-coord')
				.split(',')
			;

			app.contact.el.map = new google.maps.Map(document.querySelectorAll('.contact__map-layout')[0], {
					center: new google.maps.LatLng(coord[0], parseFloat(coord[1]) - ( window.breakpoint === 'desktop' ? 0.01 : 0)),
					zoom: app.contact.adjustment.zoom,
					disableDefaultUI: true,
					zoomControl: false,
					styles: styles,
					scrollwheel: false,
					backgroundColor: 'none'
				});

			app.contact.el.marker = new google.maps.Marker({
					position: new google.maps.LatLng(coord[0], coord[1]),
					map: app.contact.el.map,
					icon: {
						url: isLocal ? '../img/svg/logo-compact.svg' : 'assets/img/svg/logo-compact.svg',
						scaledSize: new google.maps.Size(66, 45)
					}
				});



			$('body')
				.on('click', '[data-map-item]', function(){
					let $t = $(this),
						item = $t.attr('data-map-item'),
						city = $t.closest('[data-map-control]').attr('data-map-control')
					;

					if ( $t.hasClass('is-active')) return false;

					app.contact.setMap(city, item);
				})
				.on('click', '[data-map-city]', function(){
					let $t = $(this),
						city = $t.attr('data-map-city')
					;

					if ( $t.hasClass('is-active')) return false;

					app.contact.setMap(city);


				})

		}
	},


	popup: {
		el: {

		},

		open: false,
		state: [
			'M1020,648H51c0,0,0-94.6,0-323C51,109.8,51,2,51,2h969c0,0,0,115.6,0,323C1020,533.9,1020,648,1020,648z',
			'M1020,648H51c0,0-49-94.6-49-323C2,109.8,51,2,51,2h969c0,0-49,115.6-49,323C971,533.9,1020,648,1020,648z'
		],

		togglePopup: ($popup)=>{

			let $path = $popup.find('.popup__bg').find('path'),
				$layout = $popup.find('.popup__layout')
			;

			$path.attr('d', app.popup.state[0]);

			if ( $popup.hasClass('is-open') ) {
				$popup.addClass('is-view');

				anime({
					targets: $layout[0],
					loop: false,
					left: [
						{
							easing: 'easeInQuad',
							duration: 500,
							value: 0
						}
					],
					complete: ()=>{}
				});

				anime({
					targets: $path[0],
					loop: false,
					d: [
						{
							easing: 'easeInQuad',
							duration: 500,
							value: app.popup.state[1]
						},
						{
							easing: 'easeOutElastic',
							duration: 600,
							value: app.popup.state[0]
						}
					],
					complete: ()=>{

					}
				})
			} else {

				anime({
					targets: $layout[0],
					loop: false,
					left: [
						{
							easing: 'easeInQuad',
							duration: 500,
							value: '-100%'
						}
					],
					complete: ()=>{
						$layout.removeAttr('style');
						$popup.removeClass('is-view');
					}
				});

				anime({
					targets: $path[0],
					loop: false,
					d: [
						{
							easing: 'easeInQuad',
							duration: 100,
							value: app.popup.state[1]
						}
					],
					complete: ()=>{

					}
				})

			}
		},

		init: ()=>{


			window.popup = new Popup({
				closePopupOnEsc: true,
				closePopupOnOverlay: true,

				onPopupOpen: (pop)=>{
					app.popup.togglePopup($(pop));
				},
				onPopupClose: (pop)=>{
					app.popup.togglePopup($(pop));
				},
			});
		}
	},

	menu: {
		busy: false,
		open: false,
		state: [
			'M20.7,800H0V0h20.7c0,0,0,123.5,0,397.7S20.7,800,20.7,800z',
			'M113.4,800H0V0h113.4c0,0,170.5,123.5,170.5,397.7S113.4,800,113.4,800z',
			'M300,800H0V0h300c0,0,0,123.5,0,397.7S300,800,300,800z'
		],

		el: {
			svg: document.querySelectorAll('.nav__bg')[0]
		},

		toggleMenu: ()=>{
			app.menu.open = !app.menu.open;
			app.menu.el.$body.toggleClass('show-menu');
		},

		init: ()=>{

			app.menu.el.$body = $('body');

			app.menu.el.$body
				.on('mousedown', (e)=>{
					if ( app.menu.open && !$(e.target).closest('.nav').length ) {
						app.menu.toggleMenu();
					}
				})

				.on('click', '.header__handler, .nav__close', ()=>{
					app.menu.toggleMenu();
				})
			;

		}
	},


	cards: {

		getCard: (data)=>{
			return `<div class="card__item card__item_${data.type}">
						<a class="card__item-inner" href="${data.url}">
							<div class="card__item-image">
								<img class="object-fit" src="${data.image}" alt="${data.title ? data.title : ''}">
							</div>
							${ data.tag ? '<div class="card__item-tag">' + data.tag + '</div>' : ''}
							${data.date || data.title ? '<div class="card__item-info">' : ''}
								${ data.date ? '<div class="card__item-date">' + data.date + '</div>' : ''}
								${ data.title ? '<div class="card__item-title">' + data.title + '</div>' : ''}
							${data.date || data.title ? '</div>' : ''}
						</a>
                    </div>`
		},

		init: ()=>{

			$('body').on('click', '[data-request]', function () {
				let $t = $(this),
					url = $t.attr('data-request'),
					tag = $t.attr('data-tag'),
					count = $t.attr('data-count') || 8,
					direction = $t.attr('data-direction'),
					$cards = $t.closest('.card').find('.card__list'),
					data = {}
				;

				if ( $t.hasClass('is-loading')) return false;
				$t.addClass('is-loading');

				data.offset = $cards.find('.card__item').length;
				data.count = count;

				if (tag) data.tag = tag;
				if (direction) data.direction = direction;

				$.ajax({
					url: url,
					method: 'post',
					data,
					dataType: 'json',
					success: ((res)=>{
						console.info(res);

						if (res.list) $cards.append(res.list);

						if (res.all_loaded) $t.closest('.card__more').remove();

						/*for(let i = 0; i < res.data.length; i++){
						 	let html = app.cards.getCard(res.data[i]),
								card = $(html)
							;

						 	$cards.append(card);

							objectFitImages(card.find('.object-fit'));
						}*/

					}),
					complete: ()=>{
						$t.removeClass('is-loading');
					}
				});
			})
		}
	},

	animate: {

		concat(el){

			let start = parseInt(el.innerHTML),
				end = parseInt($(el).attr('data-animate-num')),
				data = {value: start}
				;

			anime({
				targets: data,
				value: end,
				round: 1,
				easing: 'easeInOutQuart',
				update: function() {
					el.innerHTML = data.value.discharge();
				}
			});
		},

		show(el){
			let dim = el.getBoundingClientRect(),
				offset = $(el).offset(),
				$line = $('<div>', {class: 'fadeslide'}).appendTo('body')
			;

			$line.css({
				width: 0,
				height: dim.height,
				position: 'absolute',
				top: offset.top,
				left: offset.left,
				backgroundColor: $(el).attr('data-animate-color') || ''
			});

			anime({
				targets: $line[0],
				loop: false,
				width: [
					{
						easing: 'easeInQuart',
						duration: 300,
						value: dim.width
					},
				],
				complete: ()=>{

					el.removeAttribute('data-animate-fade');

					anime({
						targets: $line[0],
						loop: false,
						width: [
							{
								delay: 300,
								easing: 'easeOutQuart',
								duration: 300,
								value: 0
							}
						],
						left:[
							{
								delay: 300,
								easing: 'easeOutQuart',
								duration: 300,
								value: offset.left + dim.width
							}
						],
						complete: ()=>{
							$line.remove();
						}
					});
				}
			});

			console.info(dim)
		},

		init:()=>{

			$('[data-animate-fade]').each((i, e)=>{
				let $t = $(e),
					offset = $t.attr('data-animate-offset') ? parseInt($(e).attr('data-animate-offset')) : -200,
					delay = $t.attr('data-animate-delay') ? parseInt($(e).attr('data-animate-delay')) : 0,
					item = scrollMonitor.create(e, offset)
				;


				if ( item.isInViewport ){
					setTimeout(()=>{
						app.animate.show(e);
					}, delay);
					item.destroy();

				} else {

					item.enterViewport(()=>{
						setTimeout(()=>{
							app.animate.show(e);
						}, delay);
						item.destroy();
					});
				}

			});

			$('[data-animate-num]').each((i, e)=>{
				let $t = $(e),
					offset = $t.attr('data-animate-offset') ? parseInt($(e).attr('data-animate-offset')) : -200,
					delay = $t.attr('data-animate-delay') ? parseInt($(e).attr('data-animate-delay')) : 0,
					item = scrollMonitor.create(e, offset)
				;

				if ( item.isInViewport ){

					setTimeout(()=>{
						app.animate.concat(e);
					}, delay);
					item.destroy();

				} else {

					item.enterViewport(()=>{
						setTimeout(()=>{
							app.animate.concat(e);
						}, delay);
						item.destroy();
					});
				}
			});

			$('[data-animate-class]').each((i, e)=>{
				let $t = $(e),
					offset = $t.attr('data-animate-offset') ? parseInt($(e).attr('data-animate-offset')) : -200,
					delay = $t.attr('data-animate-delay') ? parseInt($(e).attr('data-animate-delay')) : 0,
					item = scrollMonitor.create(e, offset)
				;

				if ( item.isInViewport ){

					setTimeout(()=>{
						$t.addClass($t.attr('data-animate-class'))
					}, delay);
					item.destroy();

				} else {

					item.enterViewport(()=>{
						setTimeout(()=>{
							$t.addClass($t.attr('data-animate-class'))
						}, delay);
						item.destroy();
					});
				}
			});

			$('.ribbon').each((e, el)=>{
				setTimeout(()=>{
					$(el).addClass('is-animated');
				}, 500 * e);
			});
		}

	},

	init: ()=>{

		window.initGoogleMap = ()=>{};
		new ViewPort({
			'0': ()=>{
				window.breakpoint = 'mobile';
				app.contact.setMap();
			},
			'1060': ()=>{
				window.breakpoint = 'desktop';
				app.contact.setMap();
			}}
		);

		app.menu.init();
		app.popup.init();
		app.cards.init();

		if ( window.google ) {
			app.contact.init();
		} else {
			window.initGoogleMap = app.contact.init;
		}


		let windowLoad = false;
		window.onload = ()=>{
			if (!windowLoad){
				windowLoad = true;
				app.animate.init();
			}
		};

		setTimeout(()=>{
			if (!windowLoad){
				windowLoad = true;
				app.animate.init();
			}
		}, 1500);

		objectFitImages('.object-fit');
	}

};

export default app.init();