<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Crud_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->clean_sessions();
    }

    //Чистка сессий (на всякий случай, чтобы не плодились)
    public function clean_sessions()
    {
        $this->db->where('timestamp <', (time() - $this->config->item('sess_expiration')));
        $this->db->delete('ci_sessions');
    }

    //Основные выборки
    public function get_status_db()
    {
        $query = $this->db->query("SHOW TABLE STATUS");
        return $query->result_array();
    }

    public function get_all($table)
    {
		if ($table == 'brands') $this->db->order_by('(-sequence)', 'DESC');
		if ($table == 'equipment') $this->db->order_by('(-sequence)', 'DESC');
		$this->db->order_by('id', 'DESC');
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_sort($table, $column, $sort)
    {
        $this->db->order_by($column, $sort);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_limit_sort($limit, $table, $column, $sort)
    {
        $this->db->limit($limit);
        $this->db->order_by($column, $sort);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_sort_pagination($table, $column, $sort, $start_from, $limit)
    {
        $this->db->limit($limit, $start_from);
        $this->db->order_by($column, $sort);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_by_id($table, $field_id, $id)
    {
        $this->db->where($field_id, $id);
        $query = $this->db->get($table);
        return $query->row_array();
    }

    public function get_all_by_id($table, $field_id, $id)
    {
        $this->db->where($field_id, $id);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_by_id_sort($table, $field_id, $id, $column, $sort)
    {
        $this->db->order_by($column, $sort);
        $this->db->where($field_id, $id);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_like_id_sort($table, $field_id, $id, $column, $sort)
    {
        $this->db->order_by($column, $sort);
        $this->db->like($field_id, $id);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_by_id_sort_pagination($table, $field_id, $id, $column, $sort, $start_from, $limit)
    {
        $this->db->limit($limit, $start_from);
        $this->db->order_by($column, $sort);
        $this->db->where($field_id, $id);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_by_array($table, $field_id, $array)
    {
        $this->db->where_in($field_id, $array);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_all_by_array_sort($table, $field_id, $array, $column, $sort)
    {
        $this->db->order_by($column, $sort);
        $this->db->where_in($field_id, $array);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function update_by_id($table, $field_id, $id, $upddata)
    {
        $this->db->where($field_id, $id);
        $this->db->update($table, $upddata);
    }

    public function insert_data($table, $insdata)
    {
        $this->db->insert($table, $insdata);
        return $this->db->insert_id();
    }

    public function count_all($table)
    {
        return $this->db->count_all($table);
    }

    public function count_by_id($table, $field_id, $id)
    {
        $this->db->where($field_id, $id);
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    public function count_by_id_null($table, $field_id)
    {
        $this->db->where($field_id . " IS NULL");
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    public function count_by_id_notnull($table, $field_id)
    {
        $this->db->where($field_id . " IS NOT NULL");
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    public function count_like_id($table, $field_id, $id)
    {
        $this->db->like($field_id, $id);
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    public function count_all_by_array($table, $field_id, $array)
    {
        $this->db->where_in($field_id, $array);
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    public function delete_by_id($table, $field_id, $id)
    {
        $this->db->where($field_id, $id);
        $this->db->delete($table);
    }

    public function delete_by_array($table, $field_id, $array)
    {
        $this->db->where_in($field_id, $array);
        $this->db->delete($table);
    }

    //Конец основных выборок

    public function count_employees_all($companion = 0)
    {
        $this->db->where('companion', $companion);
        $this->db->from('employees');
        return $this->db->count_all_results();
    }

    public function count_fired_employees($companion = 0)
    {
        $this->db->where('companion', $companion);
        $this->db->where("fired IS NOT NULL");
        $this->db->from('employees');
        return $this->db->count_all_results();
    }

    public function count_not_fired_employees($companion = 0)
    {
        $this->db->where('companion', $companion);
        $this->db->where("fired IS NULL");
        $this->db->from('employees');
        return $this->db->count_all_results();
    }

    public function count_employees_by_sex($sex, $companion = 0)
    {
        $this->db->where('companion', $companion);
        $this->db->where('sex', $sex);
        $this->db->from('employees');
        return $this->db->count_all_results();
    }

    public function count_employees_on_state_by_sex($sex, $companion = 0)
    {
        $this->db->where('companion', $companion);
        $this->db->where("fired IS NULL");
        $this->db->where('sex', $sex);
        $this->db->from("employees");
        return $this->db->count_all_results();
    }

    public function count_employees_by_unit($units, $companion = 0)
    {
        $this->db->where('companion', $companion);
        $this->db->where_in('id_unit', $units);
        $this->db->from('employees');
        return $this->db->count_all_results();
    }

    public function count_employees_by_position($positions, $companion = 0)
    {
        $this->db->where('companion', $companion);
        $this->db->where_in('id_position', $positions);
        $this->db->from('employees');
        return $this->db->count_all_results();
    }

    public function count_employees_by_education($id_education_type, $companion = 0)
    {
        $this->db->join('employees', 'employees.id = employees_education.id_employee');
        $this->db->where('employees.companion', $companion);
        $this->db->where('employees_education.id_education_type', $id_education_type);
        $this->db->from('employees_education');
        return $this->db->count_all_results();
    }

    public function get_employees_young($companion = 0)
    {
        $this->db->join('employees', 'employees.id = employees_young.id_employee');
        $this->db->where('employees.companion', $companion);
        $this->db->where("employees_young.begin <= ", date("Y-m-d"));
        $this->db->where("employees_young.end >= ", date("Y-m-d"));
        $query = $this->db->get("employees_young");
        return $query->result_array();
    }

    public function get_employees_years_old($with_fired, $companion = 0)
    {
        $sql = "SELECT sex,dob, ((YEAR(CURRENT_DATE) - YEAR(dob)) - (DATE_FORMAT(CURRENT_DATE, '%m%d') < DATE_FORMAT(dob, '%m%d'))) AS age FROM employees";
        $sql .= " WHERE companion = " . $companion;
        if (!$with_fired) $sql .= " AND fired IS NULL";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function count_employess_by_units_years_old($units_ids, $with_fired, $companion = 0)
    {
        $sql = "SELECT sex,dob, ((YEAR(CURRENT_DATE) - YEAR(dob)) - (DATE_FORMAT(CURRENT_DATE, '%m%d') < DATE_FORMAT(dob, '%m%d'))) AS age FROM employees WHERE id_unit IN(" . implode(",", $units_ids) . ")";
        $sql .= " AND companion = " . $companion;
        if (!$with_fired) $sql .= " AND fired IS NULL";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function get_employees_for_ill_list()
    {
        $this->db->select('id, fio, dob');
        $this->db->where("fired IS NULL");
        $query = $this->db->get('employees');
        return $query->result_array();
    }

    public function get_employees_id_ill_group()
    {
        $this->db->group_by("employees_ill.id_employee");
        $this->db->order_by('employees.fio', 'ASC');
        $this->db->select('employees_ill.id_employee');
        $this->db->join('employees', 'employees.id = employees_ill.id_employee');
        $query = $this->db->get('employees_ill');
        return $query->result_array();
    }

    public function get_employees_for_whours_list()
    {
        $this->db->select('id, fio, dob');
        $this->db->where("fired IS NULL");
        $query = $this->db->get('employees');
        return $query->result_array();
    }

    public function get_employees_id_whours_group()
    {
        $this->db->group_by("employees_whours.id_employee");
        $this->db->order_by('employees.fio', 'ASC');
        $this->db->select('employees_whours.id_employee');
        $this->db->join('employees', 'employees.id = employees_whours.id_employee');
        $query = $this->db->get('employees_whours');
        return $query->result_array();
    }

}