<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends Crud_model
{
    public function __construct()
    {
        parent::__construct();
        $t = time() - 10800; //не 3х часов
        $this->clean_news($t);
        $this->clean_equipment($t);
        $this->clean_projects($t);
    }

    private function clean_news($t)
    {
        $this->db->where('time_elapsed IS NOT NULL');
        $this->db->where('time_elapsed <', $t);
        $this->db->delete('news');
    }

    private function clean_equipment($t)
    {
        $this->db->where('time_elapsed IS NOT NULL');
        $this->db->where('time_elapsed <', $t);
        $this->db->delete('equipment');
    }

    private function clean_projects($t)
    {
        $this->db->where('time_elapsed IS NOT NULL');
        $this->db->where('time_elapsed <', $t);
        $this->db->delete('projects');
    }

    public function get_auth($login, $password)
    {
        $this->db->where('login', $login);
        $this->db->where('password', $password);
        $query = $this->db->get('users');
        return $query->row_array();
    }

    public function get_brand_tags($id)
    {
        $this->db->select('tags.id');
        $this->db->join('tags', 'tags.id = brands_tags.id_tag');
        $this->db->where('brands_tags.id_brand', $id);
        $query = $this->db->get("brands_tags");
        return $query->result_array();
    }

    public function get_list_news($count, $offset)
    {
        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
        $this->db->order_by('dt', 'DESC');
        $this->db->where('time_elapsed IS NULL');
        $query = $this->db->get('news');
        return $query->result_array();
    }

    public function get_list_equipment($count, $offset)
    {
        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
        $this->db->order_by('id', 'DESC');
        $this->db->where('time_elapsed IS NULL');
        $query = $this->db->get('equipment');
        return $query->result_array();
    }

    public function get_list_projects($count, $offset)
    {
        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
        $this->db->order_by('dt', 'DESC');
        $this->db->order_by('id', 'DESC');
        $this->db->where('time_elapsed IS NULL');
        $query = $this->db->get('projects');
        return $query->result_array();
    }

    public function get_project_brands($id)
    {
        $this->db->select('brands.id, brands.name');
        $this->db->join('brands', 'brands.id = projects_brands.id_brand');
        $this->db->where('projects_brands.id_project', $id);
        $query = $this->db->get("projects_brands");
        return $query->result_array();
    }

    public function get_list_requests($count, $offset, $id_user = '', $admin = true)
    {
        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
        if(!$admin)
            $this->db->where('id_user', $id_user);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('requests');
        return $query->result_array();
    }

    public function count_news($publish = false)
    {
        if($publish == true)
            $this->db->where('public', 1);
        $this->db->where('time_elapsed IS NULL');
        $this->db->from('news');
        return $this->db->count_all_results();
    }

    public function count_projects($id_direction, $publish = false)
    {
        if($publish == true)
            $this->db->where('public', 1);
        $this->db->where('id_direction', $id_direction);
        $this->db->where('time_elapsed IS NULL');
        $this->db->from('projects');
        return $this->db->count_all_results();
    }

    public function count_equipment($publish = false)
    {
        if($publish == true)
            $this->db->where('public', 1);
        $this->db->where('time_elapsed IS NULL');
        $this->db->from('equipment');
        return $this->db->count_all_results();
    }

    public function count_new_requests()
    {
        $this->db->where('id_status is NULL');
        $this->db->from('requests');
        return $this->db->count_all_results();
    }
}