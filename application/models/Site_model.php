<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Site_model extends Crud_model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_carousel()
    {
        $this->db->order_by('id', 'DESC');
        $this->db->where('time_elapsed IS NULL');
        $this->db->where('public', 1);
        $this->db->where('public_carousel', 1);
        $query = $this->db->get('projects');
        return $query->result_array();
    }

    public function get_last_news($num)
    {
        $this->db->limit($num);
        $this->db->order_by('secure', 'DESC');
        $this->db->order_by('dt', 'DESC');
        $this->db->where('public', 1);
        $this->db->where('time_elapsed IS NULL');
        $query = $this->db->get('news');
        return $query->result_array();
    }

    public function get_projects_on_main($count, $offset, $id_direction)
    {
        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
		$this->db->order_by('secure', 'DESC');
        $this->db->order_by('dt', 'DESC');
        $this->db->order_by('id', 'DESC');
        $this->db->where('public', 1);
        $this->db->where('id_direction', $id_direction);
        $this->db->where('time_elapsed IS NULL');
        $query = $this->db->get('projects');
        return $query->result_array();
    }

    public function get_projects_tags($id_direction)
    {
        $this->db->distinct();
        $this->db->select('tags.name, tags.slug');
        $this->db->join('tags', 'tags.id = projects.id_tag');
        $this->db->where('projects.public', 1);
        $this->db->where('projects.time_elapsed IS NULL');
        $this->db->where('projects.id_direction', $id_direction);
        $this->db->where('tags.section', 'projects');
        $query = $this->db->get("projects");
        return $query->result_array();
    }

    public function get_news_tags()
    {
        $this->db->distinct();
        $this->db->select('tags.name, tags.slug');
        $this->db->join('tags', 'tags.id = news.id_tag');
        $this->db->where('news.public', 1);
        $this->db->where('news.time_elapsed IS NULL');
        $this->db->where('tags.section', 'news');
        $query = $this->db->get("news");
        return $query->result_array();
    }

    public function get_equipment_tags()
    {
        $this->db->distinct();
        $this->db->select('tags.name, tags.slug');
        $this->db->join('tags', 'tags.id = equipment.id_tag');
        $this->db->where('equipment.public', 1);
        $this->db->where('equipment.time_elapsed IS NULL');
        $this->db->where('tags.section', 'equipment');
        $query = $this->db->get("equipment");
        return $query->result_array();
    }

    public function get_brands_tags()
    {
        $this->db->distinct();
        $this->db->select('tags.name, tags.slug');
        $this->db->join('tags', 'tags.id = brands_tags.id_tag');
        $this->db->where('tags.section', 'brands');
        $query = $this->db->get("brands_tags");
        return $query->result_array();
    }

    public function get_projects($count, $offset, $id_direction, $id_tag = "")
    {
        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
        $this->db->order_by('dt', 'DESC');
        $this->db->order_by('id', 'DESC');
        $this->db->where('public', 1);
        $this->db->where('id_direction', $id_direction);
        if(!empty($id_tag)) {
            $this->db->where('id_tag', $id_tag);
        }
        $this->db->where('time_elapsed IS NULL');
        $query = $this->db->get('projects');
        return $query->result_array();
    }

    public function get_news($count, $offset, $id_tag = "")
    {
        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
        $this->db->order_by('secure', 'DESC');
        $this->db->order_by('dt', 'DESC');
        $this->db->where('public', 1);
        if(!empty($id_tag)) {
            $this->db->where('id_tag', $id_tag);
        }
        $this->db->where('time_elapsed IS NULL');
        $query = $this->db->get('news');
        return $query->result_array();
    }

    public function get_random_news($count, $id_exclude, $id_tag = "")
    {
        $this->db->limit($count);
        $this->db->order_by('id', 'RANDOM');
        $this->db->where('public', 1);
        $this->db->where('id !=', $id_exclude);
        if(!empty($id_tag)) {
            $this->db->where('id_tag', $id_tag);
        }
        $this->db->where('time_elapsed IS NULL');
        $query = $this->db->get('news');
        return $query->result_array();
    }

    public function get_equipment($count, $offset, $id_tag = "")
    {
        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
		$this->db->order_by('(-sequence)', 'DESC');
        $this->db->order_by('id', 'DESC');
        $this->db->where('public', 1);
        if(!empty($id_tag)) {
            $this->db->where('id_tag', $id_tag);
        }
        $this->db->where('time_elapsed IS NULL');
        $query = $this->db->get('equipment');
        return $query->result_array();
    }

    public function get_equipment_by_brand($id_brand)
    {
		$this->db->order_by('(-sequence)', 'DESC');
        $this->db->order_by('id', 'DESC');
        $this->db->where('public', 1);
        $this->db->where('brand', $id_brand);
        $this->db->where('time_elapsed IS NULL');
        $query = $this->db->get('equipment');
        return $query->result_array();
    }

    public function get_brands($count, $offset, $id_tag = "")
    {
        $this->db->select('brands.id, brands.name, brands.slug, brands.sequence');
        if ($count != 0) {
            $this->db->limit($count, $offset);
        }
		$this->db->order_by('(-sequence)', 'DESC');
        $this->db->order_by('brands.id', 'DESC');
        if(!empty($id_tag)) {
            $this->db->join('brands_tags', 'brands_tags.id_brand = brands.id');
            $this->db->where('brands_tags.id_tag', $id_tag);
        }
        $query = $this->db->get('brands');
        return $query->result_array();
    }

    public function search_equipment($query)
    {
        $this->db->select('equipment.id, equipment.id_tag, equipment.name, equipment.slug');
        $this->db->order_by('equipment.id', 'DESC');
        $this->db->join('brands', 'brands.id = equipment.brand');
        $this->db->where('equipment.public', 1);
        $this->db->like('equipment.name', $query);
        $this->db->or_like('brands.name', $query);
        $this->db->where('equipment.time_elapsed IS NULL');
        $query = $this->db->get('equipment');
        return $query->result_array();
    }
}