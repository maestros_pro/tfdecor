<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Display_lib
{
    public function display_page($data, $content_view, $aside = false)
    {
        $CI = &get_instance();

        $directions = $CI->site_model->get_all('directions');
        $menu = array();
        foreach ($directions as $dir) {
            if(!empty($CI->site_model->get_projects(1, 0, $dir['id'])))
                $menu['menu_show'][$dir['subdomain']] = true;
            else
                $menu['menu_show'][$dir['subdomain']] = false;
        }

        $CI->load->view('cake/preheader',$data);

        if($aside) {
            $data['show_eq_link'] = false;
            if($CI->site_model->count_all('equipment') > 0)
                $data['show_eq_link'] = true;
            $CI->load->view('cake/aheader',$data);
        } else {
            $CI->load->view('cake/header',$data);
        }

        $CI->load->view('cake/menu', $menu);
        $CI->load->view($content_view, $data);
        $CI->load->view('cake/footer',$data);

    }

    public function display_404()
    {
        $CI = &get_instance();

        $data['title'] = "404 - Страница не найдена";
        $data['h1'] = "404 - Страница не найдена";
        $data['description'] = "404 - Страница не найдена";

        $CI->load->view('cake/preheader',$data);

        $CI->load->view('cake/header',$data);

        $CI->load->view('cake/menu');
        $CI->load->view("error_404/index", $data);
        $CI->load->view('cake/footer',$data);
    }
}