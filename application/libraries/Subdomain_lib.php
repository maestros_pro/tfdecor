<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Subdomain_lib
{
    private $CI;

    function __construct()
    {
        $this->CI = &get_instance();
    }

    public function subdomain()
    {
        $this->CI->config->set_item('protocol', (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://");
        $subdomain = "";

        if (isset ($_SERVER['HTTP_HOST'])) {
            $url_path = explode('.', $_SERVER['HTTP_HOST']);
            if (count($url_path) == 3 AND $url_path[0] != 'www') {
                $subdomain = $url_path[0];
            }
            if (count($url_path) >= 4) {
                $subdomain = $url_path[1];
            }
        }

        $this->CI->config->set_item('main_domain', str_ireplace($this->CI->config->item('protocol'), '', base_url()));

        if (!empty($subdomain)) {
            $direction = $this->CI->crud_model->get_by_id('directions', 'subdomain', $subdomain);
            $this->CI->config->set_item('id_direction', $direction['id']);
            $this->CI->config->set_item('base_url', str_ireplace($this->CI->config->item('protocol'), $this->CI->config->item('protocol') . $subdomain . '.', base_url()));   //Присвоение базового адреса сайта для Регионов отличных от Москвы
        } else {
            $this->CI->config->set_item('id_direction', 0);
        }

        $this->CI->config->set_item('subdomain', $subdomain);
    }
}