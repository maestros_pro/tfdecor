<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller
{

    private $data = array();

    function __construct()
    {
        parent::__construct();
        $this->subdomain_lib->subdomain();
        if (!empty($this->config->item('subdomain')))
            redirect($this->config->item('protocol') . $this->config->item('main_domain') . "contact");
    }

    public function index()
    {
        $this->data['title'] = "О компании";
        $this->data['h1'] = "О компании";
        $this->data['description'] = "О компании";

        $this->display_lib->display_page($this->data, "about/index");
    }

}