<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brands extends CI_Controller
{

    private $data = array();

    function __construct()
    {
        parent::__construct();
        $this->subdomain_lib->subdomain();
        if (!empty($this->config->item('subdomain')))
            redirect($this->config->item('protocol') . $this->config->item('main_domain') . "brands");
    }

    public function index($slug = "")
    {
        $this->get_nav_tags();

        if(isset($_GET['tag'])) {
            $this->data['tag'] = $this->site_model->get_by_id('tags', 'slug', $this->input->get('tag'));
            if(empty($this->data['tag'])) {
                $this->display_lib->display_404();
            } else {
                $this->data['title'] = "Бренды - ".$this->data['tag']['name'];
                $this->data['h1'] = "Бренды - ".$this->data['tag']['name'];
                $this->data['description'] = "Бренды - ".$this->data['tag']['name'];

                $this->data['brands'] = $this->site_model->get_brands(12, 0, $this->data['tag']['id']);

                $this->data['is_all_loaded'] = $this->is_all_loaded($this->data['tag']);

                $this->display_lib->display_page($this->data, "brands/index", true);

            }
        } elseif (!empty($slug)) {
            $this->data['brand'] = $this->site_model->get_by_id('brands', 'slug', $slug);
            if(empty($this->data['brand'])) {
                $this->display_lib->display_404();
            } else {
                $this->data['title'] = $this->data['brand']['name'];
                $this->data['h1'] = $this->data['brand']['name'];
                $this->data['description'] = strip_tags($this->data['brand']['description']);

                $equipment = $this->site_model->get_equipment_by_brand($this->data['brand']['id']);
                if(!empty($equipment))
                    $this->data['has_equipment'] = true;
                else
                    $this->data['has_equipment'] = false;

                $this->display_lib->display_page($this->data, "brands/show", true);
            }
        } else {
            $this->data['title'] = "Бренды";
            $this->data['h1'] = "Бренды";
            $this->data['description'] = "Бренды";

            $this->data['brands'] = $this->site_model->get_brands(12, 0);

            $this->data['is_all_loaded'] = $this->is_all_loaded("");

            $this->display_lib->display_page($this->data, "brands/index", true);
        }
    }

    private function get_nav_tags()
    {
        $tags = $this->site_model->get_brands_tags();
        $this->data['nav_tags'] = array();
        foreach ($tags as $tag) {
            $this->data['nav_tags'][] = array(
                'name' => $tag['name'],
                'link' => base_url().'brands/?tag='.$tag['slug']
            );
        }
    }

    private function is_all_loaded($tag)
    {
        if(!empty($tag))
            $check = $this->site_model->get_brands(1, 12, $tag['id']);
        else
            $check = $this->site_model->get_brands(1, 12);

        return count($check) == 0 ? true : false;
    }

}