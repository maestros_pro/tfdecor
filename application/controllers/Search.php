<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller
{

    private $data = array();

    function __construct()
    {
        parent::__construct();
        $this->subdomain_lib->subdomain();
        if (!empty($this->config->item('subdomain')))
            redirect($this->config->item('protocol') . $this->config->item('main_domain') . "search?query=".$this->input->get('query'));
        //$this->output->enable_profiler(TRUE);
    }

    public function index()
    {
        $this->get_nav_tags();

        $this->data['title'] = "Поиск";
        $this->data['h1'] = "поиск";
        $this->data['description'] = "Поиск";

        if(isset($_GET['query']))
            $this->data['equipment'] = $this->site_model->search_equipment($this->input->get('query'));
        else
            $this->data['equipment'] = array();

        for($i = 0; $i < count($this->data['equipment']); $i++) {
            $this->data['equipment'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['equipment'][$i]['id_tag']);
        }


        $this->display_lib->display_page($this->data, "search/index", true);
    }

    private function get_nav_tags()
    {
        $tags = $this->site_model->get_equipment_tags();
        $this->data['nav_tags'] = array();
        foreach ($tags as $tag) {
            $this->data['nav_tags'][] = array(
                'name' => $tag['name'],
                'link' => base_url().'equipment/?tag='.$tag['slug']
            );
        }
    }
}
