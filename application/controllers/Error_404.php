<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error_404 extends CI_Controller
{

    private $data = array();

    function __construct()
    {
        parent::__construct();
        $this->subdomain_lib->subdomain();
    }

    public function index()
    {
        $this->data['title'] = "404 - Страница не найдена";
        $this->data['h1'] = "404 - Страница не найдена";
        $this->data['description'] = "404 - Страница не найдена";

        $this->display_lib->display_page($this->data, "error_404/index");
    }
}
