<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller
{

    private $data = array();

    function __construct()
    {
        parent::__construct();
        $this->subdomain_lib->subdomain();
        if (!empty($this->config->item('subdomain')))
            redirect($this->config->item('protocol') . $this->config->item('main_domain') . "news");
    }

    public function index($slug = "")
    {
        $this->get_nav_tags();
        $this->load->helper('month_helper');

        if(isset($_GET['tag'])) {
            $this->data['tag'] = $this->site_model->get_by_id('tags', 'slug', $this->input->get('tag'));
            if(empty($this->data['tag'])) {
                $this->display_lib->display_404();
            } else {
                $this->data['title'] = "Новости - ".$this->data['tag']['name'];
                $this->data['h1'] = "Новости - ".$this->data['tag']['name'];
                $this->data['description'] = "Новости - ".$this->data['tag']['name'];

                $this->data['news'] = $this->site_model->get_news(13, 0, $this->data['tag']['id']);
                for($i = 0; $i < count($this->data['news']); $i++) {
                    $this->data['news'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['news'][$i]['id_tag']);
                    $this->data['news'][$i]['day'] = date('d', strtotime($this->data['news'][$i]['dt']));
                    $this->data['news'][$i]['month'] = get_month(date('m', strtotime($this->data['news'][$i]['dt'])));
                    $this->data['news'][$i]['year'] = date('Y', strtotime($this->data['news'][$i]['dt']));
                }

                $this->data['is_all_loaded'] = $this->is_all_loaded($this->data['tag']);

                $this->display_lib->display_page($this->data, "news/index", true);

            }
        } elseif (!empty($slug)) {
            $this->data['new'] = $this->site_model->get_by_id('news', 'slug', $slug);
            if(empty($this->data['new'])) {
                $this->display_lib->display_404();
            } else {
                $this->data['title'] = $this->data['new']['name'];
                $this->data['h1'] = $this->data['new']['name'];
                $this->data['description'] = $this->data['new']['announce'];

                $this->data['new']['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['new']['id_tag']);

                $this->data['new']['day'] = date('d', strtotime($this->data['new']['dt']));
                $this->data['new']['month'] = get_month(date('m', strtotime($this->data['new']['dt'])));
                $this->data['new']['year'] = date('Y', strtotime($this->data['new']['dt']));

                $this->load->helper('directory');
                $path = './assets/uploads/news/' . $this->data['new']['id'] . '/';
                $map = directory_map($path, 1);
                $this->data['new']['gallery'] = array();

                $counter = 0;
                for ($i = 0; $i < count($map); $i++) {
                    if(mb_stripos($map[$i], 'gallery') !== false AND mb_stripos($map[$i], 'thumb') === false) {
                        $this->data['new']['gallery'][] = array(
                            'normal' => base_url().'assets/uploads/news/' . $this->data['new']['id'] . '/gallery'.$counter.'.jpg',
                            'thumb' => base_url().'assets/uploads/news/' . $this->data['new']['id'] . '/gallery'.$counter.'_thumb.jpg',
                        );
                        $counter++;
                    }
                }

                $this->data['random'] = $this->site_model->get_random_news(4, $this->data['new']['id']);
                for($i = 0; $i < count($this->data['random']); $i++) {
                    $this->data['random'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['random'][$i]['id_tag']);
                }

                $this->display_lib->display_page($this->data, "news/show", true);
            }
        } else {
            $this->data['title'] = "Новости";
            $this->data['h1'] = "Новости";
            $this->data['description'] = "Новости";

            $this->data['news'] = $this->site_model->get_news(13, 0);
            for($i = 0; $i < count($this->data['news']); $i++) {
                $this->data['news'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['news'][$i]['id_tag']);
                $this->data['news'][$i]['day'] = date('d', strtotime($this->data['news'][$i]['dt']));
                $this->data['news'][$i]['month'] = get_month(date('m', strtotime($this->data['news'][$i]['dt'])));
                $this->data['news'][$i]['year'] = date('Y', strtotime($this->data['news'][$i]['dt']));
            }

            $this->data['is_all_loaded'] = $this->is_all_loaded("");

            $this->display_lib->display_page($this->data, "news/index", true);
        }
    }

    private function get_nav_tags()
    {
        $tags = $this->site_model->get_news_tags();
        $this->data['nav_tags'] = array();
        foreach ($tags as $tag) {
            $this->data['nav_tags'][] = array(
                'name' => $tag['name'],
                'link' => base_url().'news/?tag='.$tag['slug']
            );
        }
    }

    private function is_all_loaded($tag)
    {
        if(!empty($tag))
            $check = $this->site_model->get_news(1, 13, $tag['id']);
        else
            $check = $this->site_model->get_news(1, 13);

        return count($check) == 0 ? true : false;
    }
}