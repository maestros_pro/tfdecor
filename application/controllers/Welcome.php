<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    private $data = array();

    function __construct()
    {
        parent::__construct();
        $this->subdomain_lib->subdomain();
    }

    public function index()
    {
        switch ($this->config->item('subdomain')) {
            case 'kitchen':
                $this->kitchen();
                break;
            case 'bakery':
                $this->bakery();
                break;
            case 'hotel':
                $this->hotel();
                break;
            case 'retail':
                $this->retail();
                break;
            case 'flot':
                $this->flot();
                break;
            default:
                $this->main();
        }
    }

    private function main()
    {
        $this->data['title'] = "Технофлот";
        $this->data['h1'] = "Технофлот";
        $this->data['wrapper_class'] = "wrapper_main";
        $this->data['description'] = "Технофлот";

        $this->data['carousel'] = $this->site_model->get_carousel();

        for($i = 0; $i < count($this->data['carousel']); $i++) {
            $this->data['carousel'][$i]['counter'] = str_pad($i + 1, 2, "0", STR_PAD_LEFT);
            $this->data['carousel'][$i]['background'] = base_url().'assets/uploads/projects/'.$this->data['carousel'][$i]['id'].'/main.jpg';
            $this->data['carousel'][$i]['thumbnail'] = base_url().'assets/uploads/projects/'.$this->data['carousel'][$i]['id'].'/main_thumb.jpg';
            if(count($this->data['carousel']) > 1) {
                if(isset($this->data['carousel'][$i + 1]))
                    $this->data['carousel'][$i]['next'] = base_url().'assets/uploads/projects/'.$this->data['carousel'][$i + 1]['id'].'/main_thumb.jpg';
                else
                    $this->data['carousel'][$i]['next'] = base_url().'assets/uploads/projects/'.$this->data['carousel'][0]['id'].'/main_thumb.jpg';
            }
            $direction = $this->site_model->get_by_id('directions', 'id', $this->data['carousel'][$i]['id_direction']);
            $this->data['carousel'][$i]['link'] = $this->config->item('protocol') . $direction['subdomain'] . "." . $this->config->item('main_domain') . "projects/" .$this->data['carousel'][$i]['slug'];
        }

        $this->data['brands'] = $this->site_model->get_all('brands');

        $this->load->helper('month_helper');
        $this->data['news'] = $this->site_model->get_last_news(5);
        for($i = 0; $i < count($this->data['news']); $i++) {
            $this->data['news'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['news'][$i]['id_tag']);
            $this->data['news'][$i]['day'] = date('d', strtotime($this->data['news'][$i]['dt']));
            $this->data['news'][$i]['month'] = get_month(date('m', strtotime($this->data['news'][$i]['dt'])));
            $this->data['news'][$i]['year'] = date('Y', strtotime($this->data['news'][$i]['dt']));
            $this->data['news'][$i]['date'] = date('d.m.Y', strtotime($this->data['news'][$i]['dt']));
        }


        $this->display_lib->display_page($this->data, "welcome/main");
    }

    private function kitchen()
    {
        $this->data['title'] = "Рестораны";
        $this->data['h1'] = "рестораны";
        $this->data['description'] = "Рестораны";
        $this->data['animate_color'] = '#8b0e3d';

        $this->data['p1'] = $this->site_model->get_projects_on_main(4, 0, 1);
        $this->data['p2'] = $this->site_model->get_projects_on_main(3, 5, 1);

        for($i = 0; $i < count($this->data['p1']); $i++)
            $this->data['p1'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['p1'][$i]['id_tag']);
        for($i = 0; $i < count($this->data['p2']); $i++)
            $this->data['p2'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['p2'][$i]['id_tag']);

        $this->get_nav_tags(1);

        $this->display_lib->display_page($this->data, "welcome/direction", true);
    }

    private function bakery()
    {
        $this->data['title'] = "Пекарни";
        $this->data['h1'] = "пекарни";
        $this->data['description'] = "Пекарни";
        $this->data['animate_color'] = '#d5a25a';

        $this->data['p1'] = $this->site_model->get_projects_on_main(4, 0, 2);
        $this->data['p2'] = $this->site_model->get_projects_on_main(3, 5, 2);

        for($i = 0; $i < count($this->data['p1']); $i++)
            $this->data['p1'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['p1'][$i]['id_tag']);
        for($i = 0; $i < count($this->data['p2']); $i++)
            $this->data['p2'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['p2'][$i]['id_tag']);

        $this->get_nav_tags(2);

        $this->display_lib->display_page($this->data, "welcome/direction", true);
    }

    private function hotel()
    {
        $this->data['title'] = "Отели";
        $this->data['h1'] = "отели";
        $this->data['description'] = "Отели";
        $this->data['animate_color'] = '#735586';

        $this->data['p1'] = $this->site_model->get_projects_on_main(4, 0, 3);
        $this->data['p2'] = $this->site_model->get_projects_on_main(3, 5, 3);

        for($i = 0; $i < count($this->data['p1']); $i++)
            $this->data['p1'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['p1'][$i]['id_tag']);
        for($i = 0; $i < count($this->data['p2']); $i++)
            $this->data['p2'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['p2'][$i]['id_tag']);

        $this->get_nav_tags(3);

        $this->display_lib->display_page($this->data, "welcome/direction", true);
    }

    private function retail()
    {
        $this->data['title'] = "Ритэйл";
        $this->data['h1'] = "ритейл";
        $this->data['description'] = "Ритэйл";
        $this->data['animate_color'] = '#f5d20f';

        $this->data['p1'] = $this->site_model->get_projects_on_main(4, 0, 4);
        $this->data['p2'] = $this->site_model->get_projects_on_main(3, 5, 4);

        for($i = 0; $i < count($this->data['p1']); $i++)
            $this->data['p1'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['p1'][$i]['id_tag']);
        for($i = 0; $i < count($this->data['p2']); $i++)
            $this->data['p2'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['p2'][$i]['id_tag']);

        $this->get_nav_tags(4);

        $this->display_lib->display_page($this->data, "welcome/direction", true);
    }

    private function flot()
    {
        $this->data['title'] = "Корабли";
        $this->data['h1'] = "корабли";
        $this->data['description'] = "Корабли";
        $this->data['animate_color'] = '#42b3cc';

        $this->data['p1'] = $this->site_model->get_projects_on_main(4, 0, 5);
        $this->data['p2'] = $this->site_model->get_projects_on_main(3, 5, 5);

        for($i = 0; $i < count($this->data['p1']); $i++)
            $this->data['p1'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['p1'][$i]['id_tag']);
        for($i = 0; $i < count($this->data['p2']); $i++)
            $this->data['p2'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['p2'][$i]['id_tag']);

        $this->get_nav_tags(5);

        $this->display_lib->display_page($this->data, "welcome/direction", true);
    }

    private function get_nav_tags($id_direction)
    {
        $tags = $this->site_model->get_projects_tags($id_direction);
        $this->data['nav_tags'] = array();
        foreach ($tags as $tag) {
            $this->data['nav_tags'][] = array(
                'name' => $tag['name'],
                'link' => base_url().'projects/?tag='.$tag['slug']
            );
        }
    }
}
