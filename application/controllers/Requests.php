<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define('SECRET_KEY', '6LdbMooUAAAAAOQn4rqyEGqUn6qguk3gmCeeoB7S');

class Requests extends CI_Controller
{

    private $json = array();

    function __construct()
    {
        parent::__construct();
        $this->subdomain_lib->subdomain();
    }

    public function index()
    {

		function getCaptcha($SecretKey){
			$Response = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.SECRET_KEY.'&response='.$SecretKey);
			return json_decode($Response);
		}

        if(isset($_POST) AND !empty($_POST)) {


        	$Verify = getCaptcha($_POST['g-recaptcha-response']);

            $data = $this->input->post(NULL, FALSE);
            $data['location'] = $this->get_location_by_ip();

        	if ($Verify->success == true && $Verify->score > 0.5){

				$insdata = array(
					'dt' => date("Y-m-d H:i:s", time()),
					'data' => json_encode($data)
				);

				$ID = $this->site_model->insert_data('requests', $insdata);

				$this->json['id'] = $ID;
				$this->json['status'] = true;
				$this->json['message'] = "Сообщение отправлено";

				$message = "<html>\n";
				$message .= "<body style='font-family:Arial,Helvetica,sans-serif; font-size: 12px; padding:10px;'>\n";
				$message .= '<p style="margin-top:10px;">Здравствуйте, на сайте <a href="'.base_url().'admin/feedback/'.$ID.'">'.base_url().'</a> новый запрос!</p>';
				$message .= '<p style="margin-top:10px;"><small>Письмо создано автоматически. Отвечать на него не надо.</small></p></body></html>';
				$headers  = "Content-type:text/html;charset=utf-8 \r\n";
				$headers .= "From:Технофлот <noreply@tfdecor.ru>\r\n";

				$admins = $this->site_model->get_all_by_id('users', 'level', 0);

				foreach ($admins as $admin) {
				    mail($admin['email'],'Запрос на '.base_url(),$message,$headers);
                }


			} else {
				$this->json['errors'] = $Verify['error-codes'];
				$this->json['status'] = false;
				$this->json['message'] = "Ошибка верификации";
			}

        } else {
            $this->json['status'] = false;
            $this->json['message'] = "Ошибка запроса";
        }

        echo json_encode($this->json);
    }

    private function get_location_by_ip()
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_HEADER, 0);
        curl_setopt($c, CURLOPT_TIMEOUT, 10);
        curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36');
        curl_setopt($c, CURLOPT_URL, "http://ip-api.com/json/".$this->input->ip_address());
        $data = curl_exec($c);
        curl_close($c);
        return $data;
    }

    public function get_more_projects()
    {
        if(isset($_POST) AND !empty($_POST)) {
            $tag = $this->site_model->get_by_id('tags', 'slug', $this->input->post('tag'));
            $direction = $this->site_model->get_by_id('directions', 'subdomain', $this->input->post('direction'));
            if(!empty($tag))
                $data['projects'] = $this->site_model->get_projects($this->input->post('count'), $this->input->post('offset'), $direction['id'], $tag['id']);
            else
                $data['projects'] = $this->site_model->get_projects($this->input->post('count'), $this->input->post('offset'), $direction['id']);

            for($i = 0; $i < count($data['projects']); $i++)
                $data['projects'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $data['projects'][$i]['id_tag']);

            $this->json['status'] = true;
            $this->json['list'] = $this->load->view('projects/item',$data,true);
            $this->json['message'] = 'Карточки загружены';

            if(!empty($tag))
                $check = $this->site_model->get_projects(1, $this->input->post('offset') + $this->input->post('count'), $direction['id'], $tag['id']);
            else
                $check = $this->site_model->get_projects(1, $this->input->post('offset') + $this->input->post('count'), $direction['id']);

            if(count($check) == 0)
                $this->json['all_loaded'] = true;
            else
                $this->json['all_loaded'] = false;

        } else {
            $this->json['status'] = false;
            $this->json['message'] = "Ошибка запроса";
        }

        echo json_encode($this->json);
    }

    public function get_more_news()
    {
        if(isset($_POST) AND !empty($_POST)) {
            $tag = $this->site_model->get_by_id('tags', 'slug', $this->input->post('tag'));
            if(!empty($tag))
                $data['news'] = $this->site_model->get_news($this->input->post('count'), $this->input->post('offset'), $tag['id']);
            else
                $data['news'] = $this->site_model->get_news($this->input->post('count'), $this->input->post('offset'));

            for($i = 0; $i < count($data['news']); $i++)
                $data['news'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $data['news'][$i]['id_tag']);

            $this->json['status'] = true;
            $this->json['list'] = $this->load->view('news/item',$data,true);
			$this->json['message'] = 'Карточки загружены';

            if(!empty($tag))
                $check = $this->site_model->get_news(1, $this->input->post('offset') + $this->input->post('count'), $tag['id']);
            else
                $check = $this->site_model->get_news(1, $this->input->post('offset') + $this->input->post('count'));

            if(count($check) == 0)
                $this->json['all_loaded'] = true;
            else
                $this->json['all_loaded'] = false;

        } else {
            $this->json['status'] = false;
            $this->json['message'] = "Ошибка запроса";
        }

        echo json_encode($this->json);
    }

    public function get_more_equipment()
    {
        if(isset($_POST) AND !empty($_POST)) {
            $tag = $this->site_model->get_by_id('tags', 'slug', $this->input->post('tag'));
            if(!empty($tag))
                $data['equipment'] = $this->site_model->get_equipment($this->input->post('count'), $this->input->post('offset'), $tag['id']);
            else
                $data['equipment'] = $this->site_model->get_equipment($this->input->post('count'), $this->input->post('offset'));

            for($i = 0; $i < count($data['equipment']); $i++)
                $data['equipment'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $data['equipment'][$i]['id_tag']);

            $this->json['status'] = true;
            $this->json['list'] = $this->load->view('equipment/item',$data,true);
			$this->json['message'] = 'Карточки загружены';

            if(!empty($tag))
                $check = $this->site_model->get_equipment(1, $this->input->post('offset') + $this->input->post('count'), $tag['id']);
            else
                $check = $this->site_model->get_equipment(1, $this->input->post('offset') + $this->input->post('count'));

            if(count($check) == 0)
                $this->json['all_loaded'] = true;
            else
                $this->json['all_loaded'] = false;

        } else {
            $this->json['status'] = false;
            $this->json['message'] = "Ошибка запроса";
        }

        echo json_encode($this->json);
    }

    public function get_more_brands()
    {
        if(isset($_POST) AND !empty($_POST)) {
            $tag = $this->site_model->get_by_id('tags', 'slug', $this->input->post('tag'));
            if(!empty($tag))
                $data['brands'] = $this->site_model->get_brands($this->input->post('count'), $this->input->post('offset'), $tag['id']);
            else
                $data['brands'] = $this->site_model->get_brands($this->input->post('count'), $this->input->post('offset'));

            $this->json['status'] = true;
            $this->json['list'] = $this->load->view('brands/item',$data,true);
			$this->json['message'] = 'Карточки загружены';

            if(!empty($tag))
                $check = $this->site_model->get_brands(1, $this->input->post('offset') + $this->input->post('count'), $tag['id']);
            else
                $check = $this->site_model->get_brands(1, $this->input->post('offset') + $this->input->post('count'));

            if(count($check) == 0)
                $this->json['all_loaded'] = true;
            else
                $this->json['all_loaded'] = false;

        } else {
            $this->json['status'] = false;
            $this->json['message'] = "Ошибка запроса";
        }

        echo json_encode($this->json);
    }
}