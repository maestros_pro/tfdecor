<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends CI_Controller
{

    private $data = array();

    function __construct()
    {
        parent::__construct();
        $this->subdomain_lib->subdomain();
        if (!empty($this->config->item('subdomain')))
            redirect($this->config->item('protocol') . $this->config->item('main_domain') . "feedback");
    }

    public function index()
    {
        switch ($this->config->item('subdomain')) {
            case 'kitchen':
                break;
            case 'bakery':
                break;
            case 'hotel':
                break;
            case 'retail':
                break;
            case 'flot':
                break;
            default:
        }

        $this->data['title'] = $this->config->item('subdomain') . " Обратная связь";
        $this->data['h1'] = $this->config->item('subdomain') . " Обратная связь";
        $this->data['description'] = $this->config->item('subdomain') . " Обратная связь";

        $this->display_lib->display_page($this->data, "feedback/index");
    }
}