<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    private $auth = array();

    private $json = array();

    function __construct()
    {
        parent::__construct();
        $this->subdomain_lib->subdomain();
        if (!empty($this->config->item('subdomain')))
            redirect($this->config->item('protocol') . $this->config->item('main_domain') . "admin");

        $this->load->model('admin_model');
        $this->auth = $this->admin_model->get_auth($this->session->userdata('login'), $this->session->userdata('password'));
        //$this->output->enable_profiler(TRUE);
    }

    /**
     * Функция распределения запросов
     */
    public function index()
    {
        if (!isset($_POST['request'])) {
            $this->load->view('admin');
        } else {
            $post = $this->input->post(NULL, FALSE);

            $this->allow_access($post);

            switch ($post['request']) {
                case 'login':
                    $this->login($post);
                    break;
                case 'logout':
                    $this->logout();
                    break;

                case 'register':
                    $this->register();
                    break;

                case 'listUsers':
                    $this->listUsers();
                    break;
                case 'getUser':
                    $this->getUser($post);
                    break;
                case 'addUser':
                    $this->addUser($post);
                    break;
                case 'updateUser':
                    $this->updateUser($post);
                    break;
                case 'deleteUser':
                    $this->deleteUser($post);
                    break;

                case 'checkSlug':
                    $this->checkSlug($post);
                    break;

                case 'listTags':
                    $this->listTags($post);
                    break;
                case 'getTag':
                    $this->getTag($post);
                    break;
                case 'addTag':
                    $this->addTag($post);
                    break;
                case 'updateTag':
                    $this->updateTag($post);
                    break;
                case 'deleteTag':
                    $this->deleteTag($post);
                    break;

                case 'listBrands':
                    $this->listBrands();
                    break;
                case 'getBrand':
                    $this->getBrand($post);
                    break;
                case 'addBrand':
                    $this->addBrand($post);
                    break;
                case 'updateBrand':
                    $this->updateBrand($post);
                    break;
                case 'deleteBrand':
                    $this->deleteBrand($post);
                    break;

                case 'listNews':
                    $this->listNews($post);
                    break;
                case 'getNew':
                    $this->getNew($post);
                    break;
                case 'createNew':
                    $this->createNew();
                    break;
                case 'updateNew':
                    $this->updateNew($post);
                    break;
                case 'deleteNew':
                    $this->deleteNew($post);
                    break;

                case 'listEquipment':
                    $this->listEquipment($post);
                    break;
                case 'getEquipment':
                    $this->getEquipment($post);
                    break;
                case 'createEquipment':
                    $this->createEquipment();
                    break;
                case 'updateEquipment':
                    $this->updateEquipment($post);
                    break;
                case 'deleteEquipment':
                    $this->deleteEquipment($post);
                    break;

                case 'listProjects':
                    $this->listProjects($post);
                    break;
                case 'getProject':
                    $this->getProject($post);
                    break;
                case 'createProject':
                    $this->createProject();
                    break;
                case 'updateProject':
                    $this->updateProject($post);
                    break;
                case 'deleteProject':
                    $this->deleteProject($post);
                    break;

                case 'listRequestStatuses':
                    $this->listRequestStatuses();
                    break;
                case 'listRequests':
                    $this->listRequests($post);
                    break;
                case 'setRequestExecutor':
                    $this->setRequestExecutor($post);
                    break;
                case 'setRequestResult':
                    $this->setRequestResult($post);
                    break;
                case 'deleteRequest':
                    $this->deleteRequest($post);
                    break;
                case 'getRequest':
                    $this->getRequest($post);
                    break;

                default:
                    $this->json['status'] = false;
                    $this->json['message'] = "Ошибка запроса";
            }
            echo json_encode($this->json);
        }
    }

    /*--------------------------------------Вспомогательные функции---------------------------------------------------*/

    /**
     * Функция проверки доступа к админке
     * @param $post: request
     * RESPONSE JSON (провал): status, message, level, auth
     * RESPONSE JSON (успех): level, auth
     */
    private function allow_access($post)
    {
        if (empty($this->auth)) {
            $this->json['status'] = false;
            $this->json['message'] = "Пользователь не авторизован";
            $this->json['level'] = -1;
            $this->json['auth'] = false;
            if ($post['request'] != 'login' AND $post['request'] != 'logout') {
                header("HTTP/1.1 401 Unauthorized");
                exit(json_encode($this->json));
            }
        } else {
            $this->json['level'] = $this->auth['level'];
            $this->json['auth'] = true;
        }
    }

    /**
     * Функция проверки прав администратора
     * @param $level: значение минимального уровня доступа
     * RESPONSE JSON (провал): status, message
     * (успех): продолжение выполнения кода
     */
    private function minLevel($level)
    {
        if ($this->auth['level'] > $level) {
            $this->json['status'] = false;
            $this->json['message'] = "Недостаточно прав доступа";
            exit(json_encode($this->json));
        }
    }

    /**
     * Функция генерирует изображение из base64
     * @param $base64: строка base64
     * @param $pfn: путь, куда сохранить изображение (без расширения)
     */
    private function generateImage($base64, $pfn)
    {
        $image_parts = explode(";base64,", $base64);
        //$image_type_aux = explode("image/", $image_parts[0]);
        //$image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        //$file = $pfn . '.'.$image_type;
        $file = $pfn . '.jpg';

        if(!empty($image_base64))
            file_put_contents($file, $image_base64);
    }

    /**
     * Функция генерирует изображение миниатюры
     * @param $source: путь к изображению из которого надо сделать миниатюру
     * @param $pfn: путь, куда сохранить изображение
     * @param $w: ширина изображения
     * @param $h: высота изображения
     */
    private function generateThumb($source, $pfn, $w, $h)
    {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $source;
        $config['new_image'] = $pfn;
        $config['maintain_ratio'] = TRUE;
        $d = getimagesize($source);
//        ($d[0] < $d[1]) ? $config['master_dim'] = 'width' : $config['master_dim'] = 'height';
//        $config['width'] = $w;
//        $config['height'] = $h;
        $config['master_dim'] = 'auto';
        $config['width'] = $w;

        $this->image_lib->initialize($config);

        if ( ! $this->image_lib->resize())
        {
            //echo $this->image_lib->display_errors();
        }
    }

    /**
     * Функция проверки slug
     * @param $post: section (tags, news, equipment, projects, brands)
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message
     */
    private function checkSlug($post)
    {
        $this->minLevel(0);

        $check = $this->admin_model->count_by_id($post['section'], 'slug', $post['slug']);

        if($check > 0) {
            $this->json['status'] = false;
            $this->json['message'] = "Slug занят";
        } else {
            $this->json['status'] = true;
            $this->json['message'] = "Slug свободен";
        }

    }

    /**
     * Функция генерирует base64 из изображения
     * @param $pfn: путь к изображению
     * @return string: base64
     */
    private function generateBase64($pfn)
    {
        $d = file_get_contents($pfn);
        $image_info = getimagesize($pfn);
        $base64 = 'data:' . $image_info['mime'] . ';base64,' . base64_encode($d);
        return $base64;
    }

    /*--------------------------------------Функции авторизации-------------------------------------------------------*/

    /**
     * Функция авторизации пользователя
     * @param $post: login, password
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message, user
     */
    private function login($post)
    {
        $user = $this->admin_model->get_auth($post['login'], $post['password']);
        if (empty($user)) {
            $this->json['status'] = false;
            $this->json['message'] = "Неправильные логин или пароль";
        } else {
            $this->session->set_userdata('login', $user['login']);
            $this->session->set_userdata('password', $user['password']);
            $this->json['status'] = true;
            $this->json['message'] = "Успешный вход";

            $this->json['directions'] = $this->admin_model->get_all('directions');
            $this->json['level'] = $user['level'];
            $this->json['auth'] = true;
            $this->json['auth_info'] = $user;
        }
    }

    /**
     * Функция выхода пользователя
     * RESPONSE JSON (успех): status, message
     */
    private function logout()
    {
        $this->session->unset_userdata('login');
        $this->session->unset_userdata('password');
        $this->json['status'] = true;
        $this->json['message'] = "Успешный выход";
    }

    /**
     * Функция регистрации сессии
     * RESPONSE JSON: directions, auth_info(если пользователь авторизован)
     */
    private function register() {
        $this->json['directions'] = $this->admin_model->get_all('directions');
        if (!empty($this->auth)) {
            $this->json['auth_info'] = $this->auth;

            $this->json['statistic']['news']['count'] = $this->admin_model->count_news();
            $this->json['statistic']['news']['publish'] = $this->admin_model->count_news(true);

            $this->json['statistic']['projects'][0]['direction'] = 1;
            $this->json['statistic']['projects'][0]['count'] = $this->admin_model->count_projects(1);
            $this->json['statistic']['projects'][0]['publish'] = $this->admin_model->count_projects(1, true);

            $this->json['statistic']['projects'][1]['direction'] = 2;
            $this->json['statistic']['projects'][1]['count'] = $this->admin_model->count_projects(2);
            $this->json['statistic']['projects'][1]['publish'] = $this->admin_model->count_projects(2, true);

            $this->json['statistic']['projects'][2]['direction'] = 3;
            $this->json['statistic']['projects'][2]['count'] = $this->admin_model->count_projects(3);
            $this->json['statistic']['projects'][2]['publish'] = $this->admin_model->count_projects(3, true);

            $this->json['statistic']['projects'][3]['direction'] = 4;
            $this->json['statistic']['projects'][3]['count'] = $this->admin_model->count_projects(4);
            $this->json['statistic']['projects'][3]['publish'] = $this->admin_model->count_projects(4, true);

            $this->json['statistic']['projects'][4]['direction'] = 5;
            $this->json['statistic']['projects'][4]['count'] = $this->admin_model->count_projects(5);
            $this->json['statistic']['projects'][4]['publish'] = $this->admin_model->count_projects(5, true);

            $this->json['statistic']['equipment']['count'] = $this->admin_model->count_equipment();
            $this->json['statistic']['equipment']['publish'] = $this->admin_model->count_equipment(true);

            $this->json['num_requests'] = $this->admin_model->count_new_requests();
        }
    }

    /*--------------------------------------Функции работы с пользователями-------------------------------------------*/

    /**
     * Функция получения списка пользователей
     * RESPONSE JSON (успех): status, message, list
     */
    private function listUsers()
    {
        $this->minLevel(0);

        $this->json['list'] = $this->admin_model->get_all('users');
        $this->json['status'] = true;
        $this->json['message'] = "Список сотрудников";
    }

    /**
     * Функция получения сотрудника
     * @param $post: id
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message, data
     */
    private function getUser($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('users', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Сотрудник не найден";
            exit(json_encode($this->json));
        }

        $this->json['data'] = $data;
        $this->json['status'] = true;
        $this->json['message'] = "Сотрудник найден";
    }

    /**
     * Функция добавления сотрудника
     * @param $post: login, password, email
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message
     */
    private function addUser($post)
    {
        $this->minLevel(0);

        $check = $this->admin_model->get_by_id('users', 'login', $post['login']);
        if(!empty($check)) {
            $this->json['status'] = false;
            $this->json['message'] = "Такой логин уже существует";
            exit(json_encode($this->json));
        }

        $insdata = array(
            'login' => $post['login'],
            'password' => $post['password'],
            'description' => $post['description'],
            'email' => $post['email'],
            'level' => 1
        );

        $id = $this->admin_model->insert_data('users', $insdata);
        $this->json['status'] = true;
        $this->json['message'] = "Сотрудник успешно добавлен";
    }

    /**
     * Функция обновления сотрудника
     * @param $post: id, login, password, email
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message
     */
    private function updateUser($post)
    {
        $this->minLevel(0);

        $check = $this->admin_model->get_by_id('users', 'login', $post['login']);
        if(!empty($check) AND $check['id'] != $post['id']) {
            $this->json['status'] = false;
            $this->json['message'] = "Такой логин уже существует";
            exit(json_encode($this->json));
        }

        $upddata = array(
            'login' => $post['login'],
            'password' => $post['password'],
            'description' => $post['description'],
            'email' => $post['email']
        );

        $this->admin_model->update_by_id('users', 'id', $post['id'], $upddata);
        $this->json['status'] = true;
        $this->json['message'] = "Сотрудник успешно обновлён";
    }

    /**
     * Функция удаления сотрудника
     * @param $post: id
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message
     */
    private function deleteUser($post)
    {
        $this->minLevel(0);

        $check = $this->admin_model->get_by_id('users', 'id', $post['id']);
        if(empty($check)) {
            $this->json['status'] = false;
            $this->json['message'] = "Сотрудник не найден";
            exit(json_encode($this->json));
        }
        if($check['level'] == 0) {
            $this->json['status'] = false;
            $this->json['message'] = "Администратора невозможно удалить";
            exit(json_encode($this->json));
        }

        $this->admin_model->delete_by_id('users', 'id', $post['id']);
        $this->json['status'] = true;
        $this->json['message'] = "Сотрудник успешно удалён";
    }

    /*--------------------------------------Функции работы с тэгами---------------------------------------------------*/

    /**
     * Функция получения списка тэгов
     * @param $post: section (news | equipment | projects | brands) - can be empty
     * RESPONSE JSON (успех): status, message, list
     */
    private function listTags($post)
    {
        $this->minLevel(0);

        if(!empty($post['section']) AND in_array($post['section'], array('news', 'equipment', 'projects', 'brands')))
            $this->json['list'] = $this->admin_model->get_all_by_id('tags', 'section', $post['section']);
        else
            $this->json['list'] = $this->admin_model->get_all('tags');
        $this->json['status'] = true;
        $this->json['message'] = "Список тэгов";
    }

    /**
     * Функция получения тэга
     * @param $post: id
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message, data
     */
    private function getTag($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('tags', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Тэг не найден";
            exit(json_encode($this->json));
        }

        $this->json['data'] = $data;
        $this->json['status'] = true;
        $this->json['message'] = "Тэг найден";

    }

    /**
     * Функция добавления тэга
     * @param $post: name, slug, section(news | equipment | projects | brands)
     * RESPONSE JSON (успех): status, message
     */
    private function addTag($post)
    {
        $this->minLevel(0);

        $insdata = array(
            'name' => $post['name'],
            'slug' => $post['slug'],
            'section' => $post['section']
        );

        $id = $this->admin_model->insert_data('tags', $insdata);
        $this->json['status'] = true;
        $this->json['message'] = "Тэг успешно добавлен";
    }

    /**
     * Функция обновления тэга
     * @param $post: id, name, slug, section
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message
     */
    private function updateTag($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('tags', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Тэг не найден";
            exit(json_encode($this->json));
        }

        $upddata = array(
            'name' => $post['name'],
            'slug' => $post['slug'],
            'section' => $post['section']
        );

        $this->admin_model->update_by_id('tags', 'id', $post['id'], $upddata);
        $this->json['status'] = true;
        $this->json['message'] = "Тэг успешно обновлён";
    }

    /**
     * Функция удаления тэга
     * @param $post: id
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message
     */
    private function deleteTag($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('tags', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Тэг не найден";
            exit(json_encode($this->json));
        }

        $this->admin_model->delete_by_id('tags', 'id', $post['id']);
        $this->json['status'] = true;
        $this->json['message'] = "Тэг успешно удалён";
    }

    /*--------------------------------------Функции работы с брендами--------------------------------------------------*/

    /**
     * Функция получения списка брендов
     * RESPONSE JSON (успех): status, message, list
     */
    private function listBrands()
    {
        $this->minLevel(0);

        $this->json['list'] = $this->admin_model->get_all('brands');

        for($i = 0; $i < count($this->json['list']); $i++) {
            $this->json['list'][$i]['tags'] = array();
            $tags = $this->admin_model->get_brand_tags($this->json['list'][$i]['id']);
            foreach ($tags as $tag)
                $this->json['list'][$i]['tags'][] = $tag['id'];
            $this->json['list'][$i]['image'] = base_url().'assets/uploads/brands/'.$this->json['list'][$i]['id'].'.jpg';
            $this->json['list'][$i]['image_gs'] = base_url().'assets/uploads/brands/'.$this->json['list'][$i]['id'].'_gs.jpg';
        }

        $this->json['status'] = true;
        $this->json['message'] = "Список брендов";
    }

    /**
     * Функция получения бренда
     * @param $post: id
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message, data
     */
    private function getBrand($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('brands', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Бренд не найден";
            exit(json_encode($this->json));
        }

        $data['tags'] = array();
        $tags = $this->admin_model->get_brand_tags($post['id']);
        foreach ($tags as $tag)
            $data['tags'][] = $tag['id'];

        $data['image'] = base_url().'assets/uploads/brands/' . $post['id'] . '.jpg';
        $data['image_gs'] = base_url().'assets/uploads/brands/' . $post['id'] . '_gs.jpg';

        $this->json['data'] = $data;
        $this->json['status'] = true;
        $this->json['message'] = "Бренд найден";

    }

    /**
     * Функция добавления бренда
     * @param $post: name, slug, description, history, id_tag, image, image_gs
     * RESPONSE JSON (успех): status, message
     */
    private function addBrand($post)
    {
        $this->minLevel(0);

        $insdata = array(
            'name' => $post['name'],
            'slug' => $post['slug'],
            'description' => $post['description'],
            'history' => $post['history']
        );

        $id = $this->admin_model->insert_data('brands', $insdata);

        if(isset($post['tags']) AND is_array($post['tags'])) {
            foreach ($post['tags'] as $id_tag) {
                $insdata = array(
                    'id_brand' => $id,
                    'id_tag' => $id_tag
                );
                $this->admin_model->insert_data('brands_tags', $insdata);
            }
        }

        $pfn = './assets/uploads/brands/' . $id;
        $this->generateImage($post['image'], $pfn);
        $pfn = './assets/uploads/brands/' . $id . '_gs';
        $this->generateImage($post['image_gs'], $pfn);
        $this->json['status'] = true;
        $this->json['message'] = "Бренд успешно добавлен";
    }

    /**
     * Функция обновления бренда
     * @param $post: id, name, slug, description, history, id_tag, image, image_gs
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message
     */
    private function updateBrand($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('brands', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Бренд не найден";
            exit(json_encode($this->json));
        }

        $this->admin_model->delete_by_id('brands_tags', 'id_brand', $post['id']);

        $upddata = array(
            'name' => $post['name'],
            'slug' => $post['slug'],
            'sequence' => $post['sequence'] > 0 ? $post['sequence'] : NULL,
            'description' => $post['description'],
            'history' => $post['history']
        );

        $this->admin_model->update_by_id('brands', 'id', $post['id'], $upddata);

        if(isset($post['tags']) AND is_array($post['tags'])) {
            foreach ($post['tags'] as $id_tag) {
                $insdata = array(
                    'id_brand' => $post['id'],
                    'id_tag' => $id_tag
                );
                $this->admin_model->insert_data('brands_tags', $insdata);
            }
        }

        @unlink('./assets/uploads/brands/'.$post['id'].'.jpg');

        $pfn = './assets/uploads/brands/' . $post['id'];
        $this->generateImage($post['image'], $pfn);

        @unlink('./assets/uploads/brands/'.$post['id'].'_gs.jpg');

        $pfn = './assets/uploads/brands/' . $post['id'] . '_gs';
        $this->generateImage($post['image_gs'], $pfn);

        $this->json['status'] = true;
        $this->json['message'] = "Бренд успешно обновлен";
    }

    /**
     * Функция удаления бренда
     * @param $post: id
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message
     */
    private function deleteBrand($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('brands', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Бренд не найден";
            exit(json_encode($this->json));
        }

        $this->admin_model->delete_by_id('brands', 'id', $post['id']);
        @unlink('./assets/uploads/brands/'.$post['id'].'.jpg');
        @unlink('./assets/uploads/brands/'.$post['id'].'_gs.jpg');

        $this->json['status'] = true;
        $this->json['message'] = "Бренд успешно удален";
    }

    /*--------------------------------------Функции работы с новостями------------------------------------------------*/

    /**
     * Функция получения списка новостей
     * @param $post: count, offset
     * RESPONSE JSON (успех): status, message, list
     */
    private function listNews($post)
    {
        $this->minLevel(0);

        $items = $this->admin_model->get_list_news($post['count'], $post['offset']);
        $this->load->helper('directory');
        for($i = 0; $i < count($items); $i++) {
            $items[$i]['main'] = base_url().'assets/uploads/news/' . $items[$i]['id'] . '/main_thumb.jpg';
            ($items[$i]['public'] == 1) ? $items[$i]['public'] = true : $items[$i]['public'] = false;
            ($items[$i]['secure'] == 1) ? $items[$i]['secure'] = true : $items[$i]['secure'] = false;
        }

        $this->json['list'] = $items;
        $this->json['status'] = true;
        $this->json['message'] = "Список новостей";
    }

    /**
     * Функция получения новости
     * @param $post: id
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message, data
     */
    private function getNew($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('news', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Новость не найдена";
            exit(json_encode($this->json));
        }

        ($data['public'] == 1) ? $data['public'] = true : $data['public'] = false;
        ($data['secure'] == 1) ? $data['secure'] = true : $data['secure'] = false;

        $this->load->helper('directory');
        $path = './assets/uploads/news/' . $post['id'] . '/';
        $map = directory_map($path, 1);
        $data['gallery'] = array();
        for ($i = 0; $i < count($map); $i++) {
            if(mb_stripos($map[$i], 'gallery') !== false AND mb_stripos($map[$i], 'thumb') === false) {
                $data['gallery'][] = base_url().'assets/uploads/news/' . $post['id'] . '/'.$map[$i];
            }
        }
        $data['main'] = base_url().'assets/uploads/news/' . $post['id'] . '/main.jpg';

        $this->json['data'] = $data;
        $this->json['status'] = true;
        $this->json['message'] = "Новость найдена";
    }

    /**
     * Функция создания новой новости
     * RESPONSE JSON (успех): status, message, id
     */
    private function createNew()
    {
        $this->minLevel(0);

        $insdata = array(
            'dt' => date("Y-m-d", time()),
            'time_elapsed' => time()
        );

        $this->json['id'] = $this->admin_model->insert_data('news', $insdata);
        $this->json['status'] = true;
        $this->json['message'] = "Шаблон новости создан";
    }

    /**
     * Функция обновления новости
     * @param $post: id, name, description, slug, public, secure, id_tag, gallery(array), main
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message
     */
    private function updateNew($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('news', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Новость не найдена";
            exit(json_encode($this->json));
        }

        $this->load->library('image_lib');

        $path = './assets/uploads/news/' . $post['id'] . '/';
        if(!file_exists($path))
            mkdir("./assets/uploads/news/" . $post['id'], 0755);

        $upddata = array(
            'id_tag' => $post['id_tag'],
            'name' => $post['name'],
            'description' => $post['description'],
            'announce' => $post['announce'],
            'dt' => DateTime::createFromFormat('D M d Y H:i:s T +',$post['dt']) -> format('Y-m-d'),
            'slug' => $post['slug'],
            'public' => ($post['public'] == 'true') ? 1 : 0,
            'secure' => ($post['secure'] == 'true') ? 1 : 0,
            'time_elapsed' => NULL
        );

        $this->admin_model->update_by_id('news', 'id', $post['id'], $upddata);

        $this->load->helper('file');
        delete_files($path);

        if(isset($post['gallery']) AND is_array($post['gallery'])) {
            $gallery = $post['gallery'];
            for($i = 0; $i < count($gallery); $i++) {
                $this->generateImage($gallery[$i], $path.'gallery'.$i);
                $this->generateThumb($path.'gallery'.$i.'.jpg', $path.'gallery'.$i.'_thumb.jpg', 600, 600);
            }
        }

        $this->generateImage($post['main'], $path.'main');
        $this->generateThumb($path.'main.jpg', $path.'main_thumb.jpg', 700, 700);

        $this->json['status'] = true;
        $this->json['message'] = "Новость сохранена";
    }

    /**
     * Функция удаления новости
     * @param $post: id
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message
     */
    private function deleteNew($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('news', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Новость не найдена";
            exit(json_encode($this->json));
        }

        $this->admin_model->delete_by_id('news', 'id', $post['id']);

        $path = './assets/uploads/news/' . $post['id'] . '/';
        $this->load->helper('file');
        delete_files($path);
        rmdir($path);

        $this->json['status'] = true;
        $this->json['message'] = "Новость успешно удалена";
    }

    /*--------------------------------------Функции работы с оборудованием--------------------------------------------*/

    /**
     * Функция получения списка оборудования
     * @param $post: count, offset
     * RESPONSE JSON (успех): status, message, list
     */
    private function listEquipment($post)
    {
        $this->minLevel(0);

        $items = $this->admin_model->get_list_equipment($post['count'], $post['offset']);
        $this->load->helper('directory');
        for($i = 0; $i < count($items); $i++) {
            ($items[$i]['public'] == 1) ? $items[$i]['public'] = true : $items[$i]['public'] = false;
            $items[$i]['thumb'] = base_url().'assets/img/nophoto.png';
            $path = './assets/uploads/equipment/' . $items[$i]['id'] . '/';
            $map = directory_map($path, 1);
            for ($j = 0; $j < count($map); $j++) {
                if(mb_stripos($map[$j], 'image0_thumb') !== false) {
                    $items[$i]['thumb'] = base_url().'assets/uploads/equipment/' . $items[$i]['id'] . '/'.$map[$j];
                    break;
                }
            }
        }

        $this->json['list'] = $items;
        $this->json['status'] = true;
        $this->json['message'] = "Список оборудования";
    }

    /**
     * Функция получения оборудования
     * @param $post: id
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message, data
     */
    private function getEquipment($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('equipment', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Оборудование не найдено";
            exit(json_encode($this->json));
        }

        ($data['public'] == 1) ? $data['public'] = true : $data['public'] = false;

        $this->load->helper('directory');
        $path = './assets/uploads/equipment/' . $post['id'] . '/';
        $map = directory_map($path, 1);
        $data['images'] = array();
        for ($i = 0; $i < count($map); $i++) {
            if(mb_stripos($map[$i], 'thumb') === false) {
                $data['images'][] = base_url().'assets/uploads/equipment/' . $post['id'] . '/'.$map[$i];
            }
        }

        $this->json['data'] = $data;
        $this->json['status'] = true;
        $this->json['message'] = "Оборудование найдено";
    }

    /**
     * Функция создания нового оборудования
     * RESPONSE JSON (успех): status, message, id
     */
    private function createEquipment()
    {
        $this->minLevel(0);

        $insdata = array(
            'time_elapsed' => time()
        );

        $this->json['id'] = $this->admin_model->insert_data('equipment', $insdata);
        $this->json['status'] = true;
        $this->json['message'] = "Шаблон оборудования создан";
    }

    /**
     * Функция обновления оборудования
     * @param $post: id, name, description, slug, brand, public, id_tag, images(array)
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message
     */
    private function updateEquipment($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('equipment', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Оборудование не найдено";
            exit(json_encode($this->json));
        }

        $this->load->library('image_lib');

        $path = './assets/uploads/equipment/' . $post['id'] . '/';
        if(!file_exists($path))
            mkdir("./assets/uploads/equipment/" . $post['id'], 0755);

        $upddata = array(
            'id_tag' => $post['id_tag'],
            'name' => $post['name'],
            'description' => $post['description'],
            'slug' => $post['slug'],
            'brand' => $post['brand'],
            'feature_dimensions' => $post['feature_dimensions'],
            'feature_voltage' => $post['feature_voltage'],
			'sequence' => $post['sequence'] > 0 ? $post['sequence'] : NULL,
            'feature_power' => $post['feature_power'],
            'public' => ($post['public'] == 'true') ? 1 : 0,
            'time_elapsed' => NULL
        );

        $this->admin_model->update_by_id('equipment', 'id', $post['id'], $upddata);

        $this->load->helper('file');
        delete_files($path);

        if(isset($post['images']) AND is_array($post['images'])) {
            $images = $post['images'];
            for($i = 0; $i < count($images); $i++) {
                $this->generateImage($images[$i], $path.'image'.$i);
                $this->generateThumb($path.'image'.$i.'.jpg', $path.'image'.$i.'_thumb.jpg', 600, 600);
            }
        }

        $this->json['status'] = true;
        $this->json['message'] = "Оборудование сохранено";
    }

    /**
     * Функция удаления оборудования
     * @param $post: id
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message
     */
    private function deleteEquipment($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('equipment', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Оборудование не найдено";
            exit(json_encode($this->json));
        }

        $this->admin_model->delete_by_id('equipment', 'id', $post['id']);

        $path = './assets/uploads/equipment/' . $post['id'] . '/';
        $this->load->helper('file');
        delete_files($path);
        rmdir($path);

        $this->json['status'] = true;
        $this->json['message'] = "Оборудование успешно удалено";
    }

    /*--------------------------------------Функции работы с проектами------------------------------------------------*/

    /**
     * Функция получения списка проектов
     * @param $post: count, offset
     * RESPONSE JSON (успех): status, message, list
     */
    private function listProjects($post)
    {
        $this->minLevel(0);

        $items = $this->admin_model->get_list_projects($post['count'], $post['offset']);
        $this->load->helper('directory');
        for($i = 0; $i < count($items); $i++) {
            $items[$i]['brands'] = $this->admin_model->get_project_brands($items[$i]['id']);
            $items[$i]['main'] = base_url().'assets/uploads/projects/' . $items[$i]['id'] . '/main_thumb.jpg';
            ($items[$i]['public'] == 1) ? $items[$i]['public'] = true : $items[$i]['public'] = false;
			($items[$i]['secure'] == 1) ? $items[$i]['secure'] = true : $items[$i]['secure'] = false;
            ($items[$i]['public_carousel'] == 1) ? $items[$i]['public_carousel'] = true : $items[$i]['public_carousel'] = false;
        }

        $this->json['list'] = $items;
        $this->json['status'] = true;
        $this->json['message'] = "Список проектов";
    }

    /**
     * Функция получения проекта
     * @param $post: id
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message, data
     */
    private function getProject($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('projects', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Проект не найден";
            exit(json_encode($this->json));
        }

        ($data['public'] == 1) ? $data['public'] = true : $data['public'] = false;
		($data['secure'] == 1) ? $data['secure'] = true : $data['secure'] = false;
        ($data['public_carousel'] == 1) ? $data['public_carousel'] = true : $data['public_carousel'] = false;

        $data['brands'] = array();
        $temp = $this->admin_model->get_project_brands($post['id']);
        foreach ($temp as $t) $data['brands'][] = $t['id'];

        $this->load->helper('directory');
        $path = './assets/uploads/projects/' . $post['id'] . '/';
        $map = directory_map($path, 1);
        $data['gallery'] = array();
        for ($i = 0; $i < count($map); $i++) {
            if(mb_stripos($map[$i], 'gallery') !== false AND mb_stripos($map[$i], 'thumb') === false) {
                $data['gallery'][] = base_url().'assets/uploads/projects/' . $post['id'] . '/'.$map[$i];
            }
        }
        $data['main'] = base_url().'assets/uploads/projects/' . $post['id'] . '/main.jpg';

        if (file_exists('./assets/uploads/projects/' . $post['id'] . '/logo.jpg'))
            $data['logo'] = base_url().'assets/uploads/projects/' . $post['id'] . '/logo.jpg';
        else
            $data['logo'] = "";

        $this->json['data'] = $data;
        $this->json['status'] = true;
        $this->json['message'] = "Проект найден";
    }

    /**
     * Функция создания нового проекта
     * RESPONSE JSON (успех): status, message, id
     */
    private function createProject()
    {
        $this->minLevel(0);

        $insdata = array(
            'dt' => date("Y-m-d", time()),
            'time_elapsed' => time()
        );

        $this->json['id'] = $this->admin_model->insert_data('projects', $insdata);
        $this->json['status'] = true;
        $this->json['message'] = "Шаблон проекта создан";
    }

    /**
     * Функция обновления проекта
     * @param $post: id, id_direction, name, description, slug, area, address, seats, public, public_carousel, secure, id_tag, brands(array), main, logo, gallery(array)
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message
     */
    private function updateProject($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('projects', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Проект не найден";
            exit(json_encode($this->json));
        }

        $this->load->library('image_lib');

        $path = './assets/uploads/projects/' . $post['id'] . '/';
        if(!file_exists($path))
            mkdir("./assets/uploads/projects/" . $post['id'], 0755);

        $this->admin_model->delete_by_id('projects_brands', 'id_project', $post['id']);

        $upddata = array(
            'id_direction' => $post['id_direction'],
            'id_tag' => $post['id_tag'],
            'name' => $post['name'],
            'description' => $post['description'],
            'slug' => $post['slug'],
            'dt' => DateTime::createFromFormat('D M d Y H:i:s T +',$post['dt']) -> format('Y-m-d'),
            'area' => $post['area'],
            'address' => $post['address'],
            'seats' => $post['seats'],
            'public' => ($post['public'] == 'true') ? 1 : 0,
            'public_carousel' => ($post['public_carousel'] == 'true') ? 1 : 0,
			'secure' => ($post['secure'] == 'true') ? 1 : 0,
            'time_elapsed' => NULL
        );

        $this->admin_model->update_by_id('projects', 'id', $post['id'], $upddata);

        if(isset($post['brands']) AND is_array($post['brands'])) {
            foreach ($post['brands'] as $id_equipment) {
                $insdata = array(
                    'id_project' => $post['id'],
                    'id_brand' => $id_equipment
                );
                $this->admin_model->insert_data('projects_brands', $insdata);
            }
        }

        $this->load->helper('file');
        delete_files($path);

        if(isset($post['gallery']) AND is_array($post['gallery'])) {
            $gallery = $post['gallery'];
            for($i = 0; $i < count($gallery); $i++) {
                $this->generateImage($gallery[$i], $path.'gallery'.$i);
                $this->generateThumb($path.'gallery'.$i.'.jpg', $path.'gallery'.$i.'_thumb.jpg', 600, 600);
            }
        }

        if(isset($post['logo']) AND !empty($post['logo']))
            $this->generateImage($post['logo'], $path.'logo');

        $this->generateImage($post['main'], $path.'main');
        $this->generateThumb($path.'main.jpg', $path.'main_thumb.jpg', 600, 600);

        $this->json['status'] = true;
        $this->json['message'] = "Проект сохранен";
    }

    /**
     * Функция удаления проекта
     * @param $post: id
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message
     */
    private function deleteProject($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('projects', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Проект не найден";
            exit(json_encode($this->json));
        }

        $this->admin_model->delete_by_id('projects', 'id', $post['id']);

        $path = './assets/uploads/projects/' . $post['id'] . '/';
        $this->load->helper('file');
        delete_files($path);
        rmdir($path);

        $this->json['status'] = true;
        $this->json['message'] = "Проект успешно удален";
    }

    /*--------------------------------------Функции работы с запросами------------------------------------------------*/

    /**
     * Функция получения списка возможных статусов запроса
     * RESPONSE JSON (успех): status, message, list
     */
    private function listRequestStatuses()
    {
        $this->json['list'] = $this->admin_model->get_all('request_statuses');
        $this->json['status'] = true;
        $this->json['message'] = "Список статусов";
    }

    /**
     * Функция получения списка запросов
     * @param $post: count, offset
     * RESPONSE JSON (успех): status, message, list
     */
    private function listRequests($post)
    {
        if ($this->auth['level'] == 0) {
            $items = $this->admin_model->get_list_requests($post['count'], $post['offset'], $this->auth['id'], true);
        } else {
            $items = $this->admin_model->get_list_requests($post['count'], $post['offset'], $this->auth['id'], false);
        }

        $this->json['list'] = $items;
        $this->json['status'] = true;
        $this->json['message'] = "Список заявок";
    }

    /**
     * Функция получения запроса
     * @param $post: id
     * RESPONSE JSON (успех): status, message, data
     */
    private function getRequest($post)
    {
		$data = $this->admin_model->get_by_id('requests', 'id', $post['id']);

		if(empty($data)) {
			$this->json['status'] = false;
			$this->json['message'] = "Заявка не найдена";
			exit(json_encode($this->json));
		}

        if ($this->auth['level'] == 0 AND $data['id_status'] == NULL) {
            $upddata = array(
                'id_status' => 1
            );

            $this->admin_model->update_by_id('requests', 'id', $data['id'], $upddata);
        }

		$this->json['data'] = $data;
		$this->json['status'] = true;
		$this->json['message'] = "Заявка найдена";
    }

    /**
     * Функция назначения исполнителя запроса
     * @param $post: id, id_user
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message
     */
    private function setRequestExecutor($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('requests', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Запрос не найден";
            exit(json_encode($this->json));
        }

        $data = $this->admin_model->get_by_id('users', 'id', $post['id_user']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Исполнитель не найден";
            exit(json_encode($this->json));
        }

        $upddata = array(
            'id_user' => $post['id_user'],
            'id_status' => 2
        );

        $this->admin_model->update_by_id('requests', 'id', $post['id'], $upddata);

        $message = "<html>\n";
        $message .= "<body style='font-family:Arial,Helvetica,sans-serif; font-size: 12px; padding:10px;'>\n";
        $message .= '<p style="margin-top:10px;">Здравствуйте, Вас назначили исполнителем нового запроса на <a href="'.base_url().'admin/feedback/'.$post['id'].'">'.base_url().'</a>!</p>';
        $message .= '<p style="margin-top:10px;"><small>Письмо создано автоматически. Отвечать на него не надо.</small></p></body></html>';
        $headers  = "Content-type:text/html;charset=utf-8 \r\n";
        $headers .= "From:Технофлот <noreply@tfdecor.ru>\r\n";

        mail($data['email'],'Исполнитель на '.base_url(),$message,$headers);

        $this->json['status'] = true;
        $this->json['message'] = "Исполнитель назначен";
    }

    /**
     * Функция обновления результата запроса
     * @param $post: id, id_status, result
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message
     */
    private function setRequestResult($post)
    {
        $data = $this->admin_model->get_by_id('requests', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Запрос не найден";
            exit(json_encode($this->json));
        }

        if($this->auth['level'] == 0 OR $this->auth['id'] == $data['id_user'])
        {
            $upddata = array(
                'id_status' => $post['id_status'],
                'result' => $post['result']
            );

            $this->admin_model->update_by_id('requests', 'id', $post['id'], $upddata);

            $this->json['status'] = true;
            $this->json['message'] = "Запрос обработан";
        } else {
            $this->json['status'] = false;
            $this->json['message'] = "Неверный исполнитель";
        }
    }

    /**
     * Функция удаления запроса
     * @param $post: id
     * RESPONSE JSON (провал): status, message
     * RESPONSE JSON (успех): status, message
     */
    private function deleteRequest($post)
    {
        $this->minLevel(0);

        $data = $this->admin_model->get_by_id('requests', 'id', $post['id']);

        if(empty($data)) {
            $this->json['status'] = false;
            $this->json['message'] = "Запрос не найден";
            exit(json_encode($this->json));
        }

        $this->admin_model->delete_by_id('requests', 'id', $post['id']);
        $this->json['status'] = true;
        $this->json['message'] = "Запрос успешно удалён";
    }

}