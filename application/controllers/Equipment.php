<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipment extends CI_Controller
{

    private $data = array();

    function __construct()
    {
        parent::__construct();
        $this->subdomain_lib->subdomain();
        if (!empty($this->config->item('subdomain')))
            redirect($this->config->item('protocol') . $this->config->item('main_domain') . "equipment");
    }

    public function index($slug = "")
    {
        $this->get_nav_tags();

        if(isset($_GET['tag'])) {
            $this->data['tag'] = $this->site_model->get_by_id('tags', 'slug', $this->input->get('tag'));
            if(empty($this->data['tag'])) {
                $this->display_lib->display_404();
            } else {
                $this->data['title'] = "Оборудование - ".$this->data['tag']['name'];
                $this->data['h1'] = "Оборудование - ".$this->data['tag']['name'];
                $this->data['description'] = "Оборудование - ".$this->data['tag']['name'];

                $this->data['equipment'] = $this->site_model->get_equipment(12, 0, $this->data['tag']['id']);
                for($i = 0; $i < count($this->data['equipment']); $i++) {
					$this->data['equipment'][$i]['brandInfo'] = $this->site_model->get_by_id('brands', 'id', $this->data['equipment'][$i]['brand']);
                    $this->data['equipment'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['equipment'][$i]['id_tag']);
                }

                $this->data['is_all_loaded'] = $this->is_all_loaded($this->data['tag']);

                $this->display_lib->display_page($this->data, "equipment/index", true);

            }
        } elseif (isset($_GET['brand'])) {
            $this->data['brand'] = $this->site_model->get_by_id('brands', 'slug', $this->input->get('brand'));
            if(empty($this->data['brand'])) {
                $this->display_lib->display_404();
            } else {
                $this->data['title'] = "Оборудование - ".$this->data['brand']['name'];
                $this->data['h1'] = "Оборудование - ".$this->data['brand']['name'];
                $this->data['description'] = "Оборудование - ".$this->data['brand']['name'];

                $this->data['equipment'] = $this->site_model->get_equipment_by_brand($this->data['brand']['id']);
                for($i = 0; $i < count($this->data['equipment']); $i++) {
                    $this->data['equipment'][$i]['brandInfo'] = $this->site_model->get_by_id('brands', 'id', $this->data['equipment'][$i]['brand']);
                    $this->data['equipment'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['equipment'][$i]['id_tag']);
                }

                $this->display_lib->display_page($this->data, "equipment/index", true);
            }
        } elseif (!empty($slug)) {
            $this->data['equipment'] = $this->site_model->get_by_id('equipment', 'slug', $slug);
            if(empty($this->data['equipment'])) {
                $this->display_lib->display_404();
            } else {
                $this->data['title'] = $this->data['equipment']['name'];
                $this->data['h1'] = $this->data['equipment']['name'];
                $this->data['description'] = $this->data['equipment']['name'];

				$this->data['equipment']['brandInfo']['brandInfo'] = $this->site_model->get_by_id('brands', 'id', $this->data['equipment']['brand']);
                $this->data['equipment']['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['equipment']['id_tag']);

                $this->load->helper('directory');
                $path = './assets/uploads/equipment/' . $this->data['equipment']['id'] . '/';
                $map = directory_map($path, 1);
                $this->data['equipment']['images'] = array();

                $counter = 0;
                for ($i = 0; $i < count($map); $i++) {
                    if(mb_stripos($map[$i], 'image') !== false AND mb_stripos($map[$i], 'thumb') === false) {
                        $this->data['equipment']['images'][] = array(
                            'normal' => base_url().'assets/uploads/equipment/' . $this->data['equipment']['id'] . '/image'.$counter.'.jpg',
                            'thumb' => base_url().'assets/uploads/equipment/' . $this->data['equipment']['id'] . '/image'.$counter.'_thumb.jpg',
                        );
                        $counter++;
                    }
                }

                $this->data['equipment']['brand'] = $this->site_model->get_by_id('brands', 'id', $this->data['equipment']['brand']);

                $this->display_lib->display_page($this->data, "equipment/show", true);
            }
        } else {
            $this->data['title'] = "Оборудование";
            $this->data['h1'] = "Оборудование";
            $this->data['description'] = "Оборудование";

            $this->data['equipment'] = $this->site_model->get_equipment(12, 0);
            for($i = 0; $i < count($this->data['equipment']); $i++) {
				$this->data['equipment'][$i]['brandInfo'] = $this->site_model->get_by_id('brands', 'id', $this->data['equipment'][$i]['brand']);
                $this->data['equipment'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['equipment'][$i]['id_tag']);
            }

            $this->data['is_all_loaded'] = $this->is_all_loaded("");

            $this->display_lib->display_page($this->data, "equipment/index", true);
        }
    }

    private function get_nav_tags()
    {
        $this->data['nav_tags'] = array();
        $brands = $this->site_model->get_brands(0,0);
        // показывать только бренды
        /*if(count($brands) > 0) {
            $this->data['nav_tags'][] = array(
                'name' => "Бренды",
                'link' => ""
            );
        }*/
        foreach ($brands as $brand) {
            if($this->site_model->count_by_id('equipment', 'brand', $brand['id']))
                $this->data['nav_tags'][] = array(
                    'name' => $brand['name'],
                    'link' => base_url().'equipment/?brand='.$brand['slug']
                );
        }


		// показывать только бренды
		/* $tags = $this->site_model->get_equipment_tags();
        if(count($brands) > 0) {
            $this->data['nav_tags'][] = array(
                'name' => "Теги",
                'link' => ""
            );
        }
        foreach ($tags as $tag) {
            $this->data['nav_tags'][] = array(
                'name' => $tag['name'],
                'link' => base_url().'equipment/?tag='.$tag['slug']
            );
        }*/
    }

    private function is_all_loaded($tag)
    {
        if(!empty($tag))
            $check = $this->site_model->get_equipment(1, 12, $tag['id']);
        else
            $check = $this->site_model->get_equipment(1, 12);

        return count($check) == 0 ? true : false;
    }

}