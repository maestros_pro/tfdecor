<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends CI_Controller
{

    private $data = array();

    function __construct()
    {
        parent::__construct();
        $this->subdomain_lib->subdomain();
    }

    public function index($slug = "")
    {
        $direction = $this->site_model->get_by_id('directions', 'subdomain', $this->config->item('subdomain'));

        $this->get_nav_tags($direction['id']);

        if(isset($_GET['tag'])) {
            $this->data['tag'] = $this->site_model->get_by_id('tags', 'slug', $this->input->get('tag'));
            if(empty($this->data['tag'])) {
                $this->display_lib->display_404();
            } else {
                $this->data['title'] = $direction['name'] . " - ".$this->data['tag']['name'];
                $this->data['h1'] = $direction['name'] . " - ".$this->data['tag']['name'];
                $this->data['description'] = $direction['name'] . " - ".$this->data['tag']['name'];

                $this->data['projects'] = $this->site_model->get_projects(12, 0, $direction['id'], $this->data['tag']['id']);
                for($i = 0; $i < count($this->data['projects']); $i++)
                    $this->data['projects'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['projects'][$i]['id_tag']);

                $this->data['is_all_loaded'] = $this->is_all_loaded($this->data['tag'], $direction['id']);

                $this->display_lib->display_page($this->data, "projects/index", true);

            }
        } elseif (!empty($slug)) {
            $this->data['project'] = $this->site_model->get_by_id('projects', 'slug', $slug);
            if(empty($this->data['project'])) {
                $this->display_lib->display_404();
            } else {
                $this->data['title'] = $this->data['project']['name'];
                $this->data['h1'] = $this->data['project']['name'];
                $this->data['description'] = strip_tags($this->data['project']['description']);

                $this->data['project']['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['project']['id_tag']);

                $this->load->helper('directory');
                $path = './assets/uploads/projects/' . $this->data['project']['id'] . '/';
                $map = directory_map($path, 1);
                $this->data['project']['gallery'] = array();

                $counter = 0;
                for ($i = 0; $i < count($map); $i++) {
                    if(mb_stripos($map[$i], 'gallery') !== false AND mb_stripos($map[$i], 'thumb') === false) {
                        $this->data['project']['gallery'][] = array(
                            'normal' => base_url().'assets/uploads/projects/' . $this->data['project']['id'] . '/gallery'.$counter.'.jpg',
                            'thumb' => base_url().'assets/uploads/projects/' . $this->data['project']['id'] . '/gallery'.$counter.'_thumb.jpg',
                        );
                        $counter++;
                    }
                }

                $brands = $this->site_model->get_all_by_id('projects_brands', 'id_project', $this->data['project']['id']);
                $this->data['project']['brands'] = array();
                foreach ($brands as $brand) {
                    $this->data['project']['brands'][] = $this->site_model->get_by_id('brands', 'id', $brand['id_brand']);
                }

                $this->display_lib->display_page($this->data, "projects/show", true);
            }
        } else {
            $direction = $this->site_model->get_by_id('directions', 'id', $direction['id']);
            $this->data['title'] = $direction['name'];
            $this->data['h1'] = $direction['name'];
            $this->data['description'] = $direction['name'];

            $this->data['projects'] = $this->site_model->get_projects(12, 0, $direction['id']);
            for($i = 0; $i < count($this->data['projects']); $i++)
                $this->data['projects'][$i]['tag'] = $this->site_model->get_by_id('tags', 'id', $this->data['projects'][$i]['id_tag']);

            $this->data['is_all_loaded'] = $this->is_all_loaded("", $direction['id']);

            $this->display_lib->display_page($this->data, "projects/index", true);
        }
    }

    private function get_nav_tags($id_direction)
    {
        $tags = $this->site_model->get_projects_tags($id_direction);
        $this->data['nav_tags'] = array();
        foreach ($tags as $tag) {
            $this->data['nav_tags'][] = array(
                'name' => $tag['name'],
                'link' => base_url().'projects/?tag='.$tag['slug']
            );
        }
    }

    private function is_all_loaded($tag, $id_direction)
    {
        if(!empty($tag))
            $check = $this->site_model->get_projects(1, 12, $id_direction, $tag['id']);
        else
            $check = $this->site_model->get_projects(1, 12, $id_direction);

        return count($check) == 0 ? true : false;
    }

}