<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map extends CI_Controller
{

    private $data = array();

    function __construct()
    {
        parent::__construct();
        $this->subdomain_lib->subdomain();
        if (!empty($this->config->item('subdomain')))
            redirect($this->config->item('protocol') . $this->config->item('main_domain') . "map");
    }

    public function index()
    {
        $this->data['title'] = "Карта сайта";
        $this->data['h1'] = "Карта сайта";

        $this->display_lib->display_page($this->data, "map/index");
    }
}
