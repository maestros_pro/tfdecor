<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tinymce_uploader extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $config['upload_path'] = './assets/uploads/tinymce/';
        $config['allowed_types'] = 'gif|jpg|png|pdf';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file'))
        {
            header("HTTP/1.1 500 Server Error");
            echo $this->upload->display_errors();
        }
        else
        {
            $data = $this->upload->data();
            echo json_encode(array('location' => base_url().'assets/uploads/tinymce/'.$data['file_name']));
        }
    }
}