<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller
{

    private $data = array();

    function __construct()
    {
        parent::__construct();
        $this->subdomain_lib->subdomain();
        if (!empty($this->config->item('subdomain')))
            redirect($this->config->item('protocol') . $this->config->item('main_domain') . "contact");
    }

    public function index()
    {
        $this->data['title'] = "Контакты";
        $this->data['h1'] = "Контакты";
        $this->data['description'] = "Контакты";

        $this->display_lib->display_page($this->data, "contact/index");
    }

}