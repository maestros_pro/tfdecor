<div class="website">
    <div class="page page_contact">
        <div class="head">
            <div class="inner">
                <div class="head__title"><?=$h1;?></div>
                <div class="head__tab">
                    <div class="head__tab-item is-active" data-map-city="spb">Санкт-Петербург</div>
                    <div class="head__tab-item" data-map-city="msk">Москва</div>
                </div>
            </div>
        </div>
        <div class="contact">
            <div class="contact__wrap">
                <div class="contact__info">
                    <div class="inner">
                        <div class="contact__info-item is-active" data-map-info="spb[0]">
                            <div class="contact__info-phone">+7 (812) 454-69-19</div>
                            <div class="contact__info-address">ул. Заневский пост, д. 3, стр. 1</div>
                            <div class="contact__info-text">Мы работаем с 9:00 - 18:30, выходные: сб, вс</div>
                            <div class="contact__info-text"><span>Бесплатная парковка</span><a class="openmap" href="<?=base_url()?>assets/img/map/spb.jpg" target="_blank">Открыть схему проезда</a></div>
                            <div class="contact__info-email"><a href="mailto:info@tfdecor.ru">info@tfdecor.ru</a></div>
                        </div>
                        <div class="contact__info-item" data-map-info="spb[1]">
                            <div class="contact__info-phone">+7 (812) 454-00-76 <br>+7 (812) 972-22-99</div>
                            <div class="contact__info-address">ул. Заневский пост, д. 3, стр. 2</div>
                            <div class="contact__info-text">Мы работаем с 9:00 - 18:30, выходные: сб, вс</div>
                            <div class="contact__info-text"><span>Бесплатная парковка</span><a class="openmap" href="<?=base_url()?>assets/img/map/spb.jpg" target="_blank">Открыть схему проезда</a></div>
                            <div class="contact__info-email"><a href="mailto:9722299@tfdecor.ru">9722299@tfdecor.ru</a></div>
                        </div>
                        <div class="contact__info-item" data-map-info="spb[2]">
                            <div class="contact__info-phone">+7 (812) 454-69-19</div>
                            <div class="contact__info-address">ул. Заневский пост, д. 3, стр. 2</div>
                            <div class="contact__info-text">Мы работаем с 9:00 - 18:30, выходные: сб, вс, обед: с 13:00 - 14:00</div>
                            <div class="contact__info-text"><span>Бесплатная парковка</span><a class="openmap" href="<?=base_url()?>assets/img/map/spb.jpg" target="_blank">Открыть схему проезда</a></div>
                            <div class="contact__info-email"><a href="mailto:info@tfdecor.ru">info@tfdecor.ru</a></div>
                        </div>
                        <div class="contact__info-item" data-map-info="msk[0]">
                            <div class="contact__info-phone">+7 (495) 739-42-79</div>
                            <div class="contact__info-address">ул. Щербаковская, д. 3, оф. 603</div>
                            <div class="contact__info-text">Мы работаем с 9:00 - 18:30, выходные: сб, вс</div>
                            <div class="contact__info-text"><span>Бесплатная парковка</span></div>
                            <div class="contact__info-email"><a href="mailto:info_msk@tfdecor.ru">info_msk@tfdecor.ru</a></div>
                        </div>
                        <div class="contact__info-item" data-map-info="msk[1]">
                            <div class="contact__info-phone">+7 (495) 739-42-79</div>
                            <div class="contact__info-address">Семеновский пер., д. 15, офис 919</div>
                            <div class="contact__info-text">Мы работаем с 9:00 - 18:30, выходные: сб, вс</div>
                            <div class="contact__info-text"><span>Бесплатная парковка</span></div>
                            <div class="contact__info-email"><a href="mailto:info_msk@tfdecor.ru">info_msk@tfdecor.ru</a></div>
                        </div>
                        <div class="contact__info-item" data-map-info="msk[2]">
                            <div class="contact__info-phone">+7 (925) 011-50-38</div>
                            <div class="contact__info-address">ул. Газгольдерная, д. 12, стр. 4</div>
                            <div class="contact__info-text">Мы работаем с 10:00 - 18:00, выходные: сб, вс</div>
                            <div class="contact__info-text"><span>Бесплатная парковка</span><a class="openmap" href="<?=base_url()?>assets/img/map/msk.jpg" target="_blank">Открыть схему проезда</a></div>
                            <div class="contact__info-email"><a href="mailto:info_msk@tfdecor.ru">info_msk@tfdecor.ru</a></div>
                        </div>
                    </div>
                </div>
                <div class="contact__map">
                    <div class="contact__map-layout"></div>
                    <div class="contact__map-control">
                        <div class="inner is-active" data-map-control="spb">
                            <div class="btn btn_green is-active" data-map-item="0" data-map-coord="59.943075,30.512231">Офис</div>
                            <div class="btn btn_green" data-map-item="1" data-map-coord="59.943111,30.513174">сервисный центр</div>
                            <div class="btn btn_green" data-map-item="2" data-map-coord="59.943075,30.512231">склад</div>
                        </div>
                        <div class="inner" data-map-control="msk">
                            <div class="btn btn_green is-active" data-map-item="0" data-map-coord="55.782623,37.723367">Офис</div>
                            <div class="btn btn_green" data-map-item="1" data-map-coord="55.782927,37.715911">сервисный центр</div>
                            <div class="btn btn_green" data-map-item="2" data-map-coord="55.726168,37.734452">склад</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact__form">
                <div class="inner">
                    <div class="contact__form-inner">
                        <div class="contact__form-title">Напишите нам&nbsp;с&nbsp;сайта</div>
                        <form class="form form_ajax" action="<?=base_url()?>requests" method="post" novalidate autocomplete="off">
							<input type="hidden" name="page_url" value="<?= $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] ?>">
							<input type="hidden" name="form_title" value="Напишите нам с сайта">
							<div class="form__field">
                                <div class="form__input">
                                    <input type="text" name="name" data-require="true">
                                    <div class="form__placeholder">Как вас зовут?</div>
                                </div>
                            </div>
                            <div class="form__field form__field_half">
                                <div class="form__input">
                                    <input type="text" name="phone" data-require="true" data-mask="+7 999 999-99-99">
                                    <div class="form__placeholder">Телефон</div>
                                </div>
                            </div>
                            <div class="form__field form__field_half">
                                <div class="form__input">
                                    <input type="email" name="email" data-require="email">
                                    <div class="form__placeholder">Почта</div>
                                </div>
                            </div>
                            <div class="form__field">
                                <div class="form__input">
                                    <textarea name="message" data-require="true"></textarea>
                                    <div class="form__placeholder">Сообщение</div>
                                </div>
                            </div>

							<div class="form__field form__field_polithics">
								<div class="form__input">
									<label>
										<input checked type="checkbox" name="politics" data-require="true">
										<span class="form__label">Настоящим подтверждаю, что я&nbsp;ознакомлен и&nbsp;согласен с <a href="<?=base_url()?>assets/doc/politics.pdf" target="_blank">условиями политики конфиденциальности</a></span>
									</label>
								</div>
							</div>
                            <div class="form__field form__field_submit">
                                <div class="form__input">
                                    <button class="btn btn_big btn_green" type="submit">Отправить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script async src="https://maps.googleapis.com/maps/api/js?callback=initGoogleMap&amp;key=AIzaSyAn6GW5xigU8SXcGof8ywoAT6Waqg6i-fU"></script>