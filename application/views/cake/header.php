<body>
<div class="wrapper <?if(!empty($wrapper_class)) {?><?=$wrapper_class;?><?}?>">
    <header class="header">
        <div class="inner">
			<a class="header__logo" href="<?=$this->config->item('protocol') . $this->config->item('main_domain')?>">
				<img src="<?=base_url()?>assets/img/technoflot-logo.svg" alt="Технофлот">
			</a>
            <div class="header__control">

				<div class="header__text">
					<div class="header__text-item header__text-item_phone">
						<span>СПБ +7 (812) 454-69-19</span>
						<br>
						<span>МСК +7 (495) 739-42-79</span>
					</div>
					<div class="header__text-item header__text-item_email">
						<a href="mailto:info@tfdecor.ru">info@tfdecor.ru</a>
					</div>
				</div>

                <div class="header__soc">
					<a class="header__soc-item" href="https://www.facebook.com/technoflot/" target="_blank">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path fill="#FFFFFF" d="M37,24.4c-1.8,0-3.1,0.6-4.4,1.2V39c1.2,0.1,2,0.1,3.1,0.1c4.3,0,4.8-2,4.8-4.7V28C40.6,26,39.9,24.4,37,24.4L37,24.4z M13.7,45.9H5V24.8H0.7v-7.3H5v-4.4c0-5.9,2.5-9.4,9.4-9.4h5.8V11h-3.6c-2.7,0-2.9,1-2.9,2.9l0,3.6h6.6l-0.8,7.3h-5.8L13.7,45.9L13.7,45.9z M49.3,34.4c0,6.9-2,11.9-13.8,11.9c-4.3,0-6.8-0.4-11.5-1.1V5.9l8.7-1.5v13.7c1.9-0.7,4.3-1.1,6.5-1.1c8.7,0,10.1,3.9,10.1,10.2L49.3,34.4L49.3,34.4z"/></svg>
					</a>
					<a class="header__soc-item" href="https://instagram.com/technoflot" target="_blank">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path fill="#ffffff" d="M17.7,25c0-4,3.3-7.3,7.3-7.3s7.3,3.3,7.3,7.3S29,32.3,25,32.3S17.7,29,17.7,25 M13.7,25c0,6.2,5.1,11.3,11.3,11.3S36.3,31.2,36.3,25S31.2,13.7,25,13.7S13.7,18.8,13.7,25 M34.1,13.3c0,1.5,1.2,2.6,2.6,2.6c1.5,0,2.6-1.2,2.6-2.6s-1.2-2.6-2.6-2.6h0C35.3,10.6,34.1,11.8,34.1,13.3 M16.1,42.9c-2.1-0.1-3.3-0.5-4.1-0.8c-1-0.4-1.8-0.9-2.5-1.6S8.3,39,7.9,38c-0.3-0.8-0.7-1.9-0.8-4.1C7,31.6,7,30.9,7,25s0-6.6,0.1-8.9C7.2,14,7.6,12.8,7.9,12c0.4-1,0.9-1.8,1.6-2.5S11,8.3,12,7.9c0.8-0.3,1.9-0.7,4.1-0.8C18.4,7,19.1,7,25,7s6.6,0,8.9,0.1C36,7.2,37.2,7.6,38,7.9c1,0.4,1.8,0.9,2.5,1.6s1.2,1.5,1.6,2.5c0.3,0.8,0.7,1.9,0.8,4.1c0.1,2.3,0.1,3,0.1,8.9s0,6.6-0.1,8.9c-0.1,2.1-0.5,3.3-0.8,4.1c-0.4,1-0.9,1.8-1.6,2.5c-0.8,0.8-1.5,1.2-2.5,1.6c-0.8,0.3-1.9,0.7-4.1,0.8C31.6,43,30.9,43,25,43S18.4,43,16.1,42.9 M15.9,3.2c-2.3,0.1-3.9,0.5-5.3,1C9.2,4.7,7.9,5.5,6.7,6.7s-2,2.4-2.5,3.9c-0.5,1.4-0.9,3-1,5.3C3,18.3,3,19,3,25s0,6.7,0.1,9.1c0.1,2.3,0.5,3.9,1,5.3c0.6,1.4,1.3,2.7,2.5,3.9s2.4,2,3.9,2.5c1.4,0.5,3,0.9,5.3,1C18.3,46.9,19,47,25,47s6.7,0,9.1-0.1c2.3-0.1,3.9-0.5,5.3-1c1.4-0.6,2.7-1.3,3.9-2.5s2-2.4,2.5-3.9c0.5-1.4,0.9-3,1-5.3C47,31.7,47,31,47,25s0-6.7-0.1-9.1c-0.1-2.3-0.5-3.9-1-5.3c-0.6-1.4-1.3-2.7-2.5-3.9c-1.2-1.2-2.4-2-3.9-2.5c-1.4-0.5-3-0.9-5.3-1C31.7,3.1,31,3,25,3S18.3,3,15.9,3.2"/><path fill="#ffffff" d="M17.7,25c0-4,3.3-7.3,7.3-7.3s7.3,3.3,7.3,7.3S29,32.3,25,32.3S17.7,29,17.7,25 M13.7,25c0,6.2,5.1,11.3,11.3,11.3S36.3,31.2,36.3,25S31.2,13.7,25,13.7S13.7,18.8,13.7,25 M34.1,13.3c0,1.5,1.2,2.6,2.6,2.6c1.5,0,2.6-1.2,2.6-2.6s-1.2-2.6-2.6-2.6h0C35.3,10.6,34.1,11.8,34.1,13.3 M16.1,42.9c-2.1-0.1-3.3-0.5-4.1-0.8c-1-0.4-1.8-0.9-2.5-1.6S8.3,39,7.9,38c-0.3-0.8-0.7-1.9-0.8-4.1C7,31.6,7,30.9,7,25s0-6.6,0.1-8.9C7.2,14,7.6,12.8,7.9,12c0.4-1,0.9-1.8,1.6-2.5S11,8.3,12,7.9c0.8-0.3,1.9-0.7,4.1-0.8C18.4,7,19.1,7,25,7s6.6,0,8.9,0.1C36,7.2,37.2,7.6,38,7.9c1,0.4,1.8,0.9,2.5,1.6s1.2,1.5,1.6,2.5c0.3,0.8,0.7,1.9,0.8,4.1c0.1,2.3,0.1,3,0.1,8.9s0,6.6-0.1,8.9c-0.1,2.1-0.5,3.3-0.8,4.1c-0.4,1-0.9,1.8-1.6,2.5c-0.8,0.8-1.5,1.2-2.5,1.6c-0.8,0.3-1.9,0.7-4.1,0.8C31.6,43,30.9,43,25,43S18.4,43,16.1,42.9 M15.9,3.2c-2.3,0.1-3.9,0.5-5.3,1C9.2,4.7,7.9,5.5,6.7,6.7s-2,2.4-2.5,3.9c-0.5,1.4-0.9,3-1,5.3C3,18.3,3,19,3,25s0,6.7,0.1,9.1c0.1,2.3,0.5,3.9,1,5.3c0.6,1.4,1.3,2.7,2.5,3.9s2.4,2,3.9,2.5c1.4,0.5,3,0.9,5.3,1C18.3,46.9,19,47,25,47s6.7,0,9.1-0.1c2.3-0.1,3.9-0.5,5.3-1c1.4-0.6,2.7-1.3,3.9-2.5s2-2.4,2.5-3.9c0.5-1.4,0.9-3,1-5.3C47,31.7,47,31,47,25s0-6.7-0.1-9.1c-0.1-2.3-0.5-3.9-1-5.3c-0.6-1.4-1.3-2.7-2.5-3.9c-1.2-1.2-2.4-2-3.9-2.5c-1.4-0.5-3-0.9-5.3-1C31.7,3.1,31,3,25,3S18.3,3,15.9,3.2"/></svg>
					</a>
					<a class="header__soc-item" href="https://vk.com/techno_flot" target="_blank">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" ><path fill="#fff" d="M49.7,36.5c-0.1-0.1-0.1-0.2-0.2-0.3c-0.9-1.6-2.5-3.5-5-5.8l-0.1-0.1l0,0l0,0h0c-1.1-1.1-1.8-1.8-2.1-2.1c-0.5-0.7-0.7-1.4-0.4-2.1c0.2-0.5,1-1.7,2.3-3.4c0.7-0.9,1.3-1.7,1.7-2.2c3-4,4.3-6.5,3.9-7.7l-0.2-0.3c-0.1-0.2-0.4-0.3-0.8-0.4C48.5,12,48,12,47.3,12l-7.5,0.1c-0.1,0-0.3,0-0.5,0c-0.2,0.1-0.3,0.1-0.3,0.1l-0.1,0.1l-0.1,0.1c-0.1,0.1-0.2,0.1-0.3,0.3c-0.1,0.1-0.2,0.3-0.3,0.5c-0.8,2.1-1.7,4.1-2.8,5.9c-0.6,1.1-1.2,2-1.8,2.8c-0.5,0.8-1,1.4-1.4,1.7c-0.4,0.4-0.7,0.7-1,0.9c-0.3,0.2-0.5,0.3-0.7,0.3c-0.2,0-0.3-0.1-0.4-0.1c-0.2-0.2-0.4-0.4-0.6-0.6c-0.1-0.3-0.2-0.6-0.3-1c-0.1-0.4-0.1-0.8-0.1-1.1c0-0.3,0-0.7,0-1.2c0-0.5,0-0.9,0-1.1c0-0.7,0-1.4,0-2.1c0-0.8,0-1.4,0.1-1.8c0-0.5,0-0.9,0-1.4c0-0.5,0-0.9-0.1-1.2c-0.1-0.3-0.2-0.6-0.3-0.8c-0.1-0.3-0.3-0.5-0.5-0.6c-0.2-0.1-0.5-0.3-0.9-0.4c-0.9-0.2-2.1-0.3-3.5-0.3c-3.2,0-5.3,0.2-6.2,0.6c-0.4,0.2-0.7,0.5-1,0.8c-0.3,0.4-0.4,0.6-0.1,0.6c1,0.2,1.8,0.5,2.2,1.1l0.2,0.3c0.1,0.2,0.2,0.6,0.4,1.2c0.1,0.6,0.2,1.2,0.2,1.9c0.1,1.3,0.1,2.4,0,3.3c-0.1,0.9-0.2,1.6-0.2,2.1c-0.1,0.5-0.2,0.9-0.4,1.2c-0.2,0.3-0.3,0.5-0.3,0.6c-0.1,0.1-0.1,0.1-0.1,0.1c-0.2,0.1-0.5,0.1-0.7,0.1c-0.2,0-0.5-0.1-0.9-0.4c-0.3-0.2-0.7-0.6-1.1-1c-0.4-0.4-0.8-1-1.3-1.8c-0.5-0.8-1-1.7-1.5-2.7l-0.4-0.8c-0.3-0.5-0.6-1.2-1.1-2.1c-0.5-0.9-0.9-1.8-1.2-2.7c-0.1-0.4-0.3-0.6-0.6-0.8l-0.1-0.1c-0.1-0.1-0.2-0.1-0.4-0.2C9.2,12.1,9,12,8.8,12L1.7,12c-0.7,0-1.2,0.2-1.5,0.5l-0.1,0.2C0,12.8,0,12.9,0,13.1c0,0.2,0.1,0.4,0.2,0.7c1,2.4,2.2,4.8,3.4,7.1c1.2,2.3,2.3,4.1,3.2,5.5c0.9,1.4,1.8,2.7,2.8,3.9c0.9,1.2,1.6,2,1.9,2.4c0.3,0.3,0.5,0.6,0.7,0.8l0.7,0.6c0.4,0.4,1,0.9,1.8,1.5c0.8,0.6,1.7,1.2,2.7,1.7c1,0.6,2.1,1,3.4,1.4c1.3,0.4,2.6,0.5,3.8,0.4h3c0.6-0.1,1.1-0.2,1.4-0.6l0.1-0.1c0.1-0.1,0.1-0.3,0.2-0.5c0.1-0.2,0.1-0.5,0.1-0.7c0-0.7,0-1.4,0.2-2c0.1-0.6,0.3-1,0.4-1.4c0.2-0.3,0.4-0.6,0.6-0.8c0.2-0.2,0.4-0.3,0.4-0.4c0.1,0,0.2-0.1,0.2-0.1c0.4-0.1,0.9,0,1.5,0.4c0.6,0.4,1.1,0.9,1.6,1.5c0.5,0.6,1.1,1.3,1.8,2c0.7,0.7,1.3,1.3,1.8,1.7l0.5,0.3c0.3,0.2,0.8,0.4,1.4,0.6c0.6,0.2,1,0.2,1.5,0.1l6.7-0.1c0.7,0,1.2-0.1,1.5-0.3c0.4-0.2,0.6-0.5,0.7-0.7c0.1-0.3,0.1-0.6,0-0.9C49.9,36.9,49.8,36.6,49.7,36.5z"/></svg>
					</a>
				</div>
                <div class="header__search">
                    <form action="<?=base_url()?>search" method="get">
                        <div class="header__search-input">
                            <input type="text" name="query" autocomplete="off">
                        </div>
                        <div class="header__search-ico"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path  d="M45.5,41.1l-9.6-9.6c2-2.9,3.2-6.3,3.2-10.1c0-9.8-8-17.7-17.7-17.7c-9.8,0-17.7,7.9-17.7,17.7c0,9.8,8,17.7,17.7,17.7c3.7,0,7.2-1.2,10.1-3.2l9.6,9.6c1.2,1.2,3.2,1.2,4.4,0C46.7,44.3,46.7,42.3,45.5,41.1z M21.4,32.9C15,32.8,9.9,27.7,9.9,21.4c0-6.3,5.1-11.5,11.5-11.5c6.3,0,11.4,5.1,11.4,11.5C32.8,27.8,27.7,32.9,21.4,32.9z"/></svg>

                        </div>
                    </form>
                </div>

				<a class="header__call" href="tel:+78124546919">
					<div class="header__call-btn btn btn_border">Напишите нам</div>
					<div class="header__call-ico">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path d="M44.2,40.6c-0.6,1.4-2.2,2.3-3.5,3c-1.7,0.9-3.4,1.5-5.3,1.5c-2.7,0-5.1-1.1-7.5-2c-1.7-0.6-3.4-1.4-5-2.4C18,37.7,12.2,31.8,9.2,27c-1-1.6-1.7-3.3-2.4-5c-0.9-2.4-2-4.8-2-7.5c0-1.9,0.5-3.6,1.5-5.3C7,8,8,6.3,9.4,5.7c0.9-0.4,2.9-0.9,4-0.9c0.2,0,0.4,0,0.6,0.1c0.6,0.2,1.2,1.6,1.5,2.2c0.9,1.6,1.8,3.3,2.7,4.9c0.5,0.7,1.3,1.7,1.3,2.5c0,1.7-5.2,4.3-5.2,5.8c0,0.8,0.7,1.8,1.1,2.5c2.9,5.2,6.5,8.8,11.7,11.7c0.7,0.4,1.7,1.1,2.5,1.1c1.5,0,4.1-5.2,5.8-5.2c0.9,0,1.8,0.9,2.5,1.3c1.6,0.9,3.3,1.8,4.9,2.7c0.6,0.3,2,0.9,2.2,1.5c0.1,0.2,0.1,0.4,0.1,0.6C45.1,37.7,44.7,39.7,44.2,40.6z"/></svg>
					</div>
				</a>

                <div class="header__handler">
                    <div class="header__handler-text">Меню</div>
                    <div class="header__handler-ico"></div>
                </div>
            </div>
        </div>
    </header>