<div class="callback">
    <div class="inner">
        <div class="callback__title"><?=$callback_title;?></div>
        <div class="callback__subtitle">Давайте мы позвоним вам и подробно расскажем</div>
        <div class="callback__form">
            <form class="form form_ajax" action="<?=base_url()?>requests" method="post" novalidate autocomplete="off">
				<input type="hidden" name="page_url" value="<?= $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] ?>">
				<input type="hidden" name="form_title" value="<?=$callback_title;?>">
				<div class="form__field">
                    <div class="form__input">
                        <input placeholder="Ваш телефон" type="text" name="phone" data-require="true" data-mask="+7 999 999-99-99">
                    </div>
                </div>
				<div class="form__field form__field_polithics ">
                    <div class="form__input">
						<label>
							<input checked type="checkbox" name="politics" data-require="true">
							<span class="form__label">Настоящим подтверждаю, что я&nbsp;ознакомлен и&nbsp;согласен с&nbsp;<a href="<?=base_url()?>assets/doc/politics.pdf" target="_blank">условиями политики конфиденциальности</a></span>
						</label>
                    </div>
                </div>
                <div class="form__field form__field_submit">
                    <div class="form__input">
                        <button class="btn btn_green" type="submit">Перезвоните мне</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>