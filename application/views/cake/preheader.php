<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title><?=$title;?></title>
    <meta name="description" content="<?=htmlspecialchars($description);?>">
    <meta property="og:title" content="<?=htmlspecialchars($title);?>"/>
    <meta property="og:description" content="<?=htmlspecialchars($description);?>"/>
    <meta property="og:image" content="/img.jpg"/>
    <meta property="og:url" content="{{page url}}"/>
    <meta name="twitter:title" content="<?=htmlspecialchars($title);?>"/>
    <meta name="twitter:description" content="<?=htmlspecialchars($description);?>"/>
    <meta name="twitter:image" content="/img.jpg"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <link rel="apple-touch-icon" sizes="180x180" href="<?=base_url()?>assets/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=base_url()?>assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url()?>assets/img/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#2e5542">
    <meta name="theme-color" content="#2e5542">
    <link href="/assets/css/index.style.css?93230022019" rel="stylesheet">
	<script src='https://www.google.com/recaptcha/api.js?render=6LdbMooUAAAAAN-MaOxYTWnBstOLLGdTb5t-VxiC'></script>
</head>