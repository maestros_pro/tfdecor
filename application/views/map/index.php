<div class="website">
    <div class="article">
        <div class="inner">
            <div class="article__title"><?=$h1;?></div>
            <div class="article__content">
				<div class="typo">
					<p><a href="<?=$this->config->item('protocol') . $this->config->item('main_domain')?>">Главная</a></p>
					<p><a href="<?=$this->config->item('protocol') . "kitchen." . $this->config->item('main_domain')?>">Рестораны</a></p>
					<p><a href="<?=$this->config->item('protocol') . "hotel." . $this->config->item('main_domain')?>">Отели</a></p>
					<p><a href="<?=$this->config->item('protocol') . "bakery." . $this->config->item('main_domain')?>">Пекарни</a></p>
					<p><a href="<?=$this->config->item('protocol') . "retail." . $this->config->item('main_domain')?>">Ритейл</a></p>
					<p><a href="<?=$this->config->item('protocol') . "flot." . $this->config->item('main_domain')?>">Корабли</a></p>
					<p><a href="<?=base_url()?>brands">Список брендов</a></p>
					<ul>
						<!-- вывести все страницы брендов -->
						<li><a href="<?=base_url()?>brands/test">Страница бренда</a></li>
					</ul>
					<p><a href="<?=base_url()?>equipment">Список оборудования</a></p>
					<ul>
						<!-- вывести все страницы оборудования -->
						<li><a href="<?=base_url()?>equipment/show/test">Страница оборудования</a></li>
					</ul>
					<p><a href="<?=base_url()?>news">Список новостей</a></p>
					<ul>
						<!-- вывести все страницы новостей -->
						<li><a href="<?=base_url()?>news/show/test">Страница новости</a></li>
					</ul>
					<p><a href="<?=base_url()?>projects">Список проектов</a></p>
					<ul>
						<!-- вывести все страницы проектов -->
						<li><a href="<?=base_url()?>projects/show/test">Страница проекта</a></li>
					</ul>
					<p><a href="<?=base_url()?>contact">Контакты</a></p>
<!--					<p><a href="--><?//=base_url()?><!--search">Страница результатов поиска</a></p>-->
<!--					<p><a href="--><?//=base_url()?><!--error_404">Ошибка 404</a></p>-->
<!--					<p><a href="--><?//=base_url()?><!--error_503">Ошибка 503</a></p>-->
				</div>
            </div>
        </div>
    </div>
</div>