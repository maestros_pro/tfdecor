<div class="website">
    <div class="page page_projects">
        <div class="inner">
            <div class="head">
                <div class="head__title"><?=$h1;?></div>
            </div>
            <?if(!empty($projects)) {?>
                <div class="card">
                    <div class="card__list">
                        <?foreach ($projects as $item):?>
                            <div class="card__item card__item_project"><a class="card__item-inner" href="<?=base_url()?>projects/<?=$item['slug']?>">
                                    <div class="card__item-image">
										<img class="object-fit" src="<?=base_url()?>assets/uploads/projects/<?=$item['id']?>/main_thumb.jpg" alt="<?=htmlspecialchars($item['name'])?>"/>
									</div>
                                    <div class="card__item-tag"><?=$item['tag']['name']?></div>
                                    <div class="card__item-info">
                                        <div class="card__item-title"><?=$item['name']?></div>
                                    </div></a></div>
                        <?endforeach;?>
                    </div>
                    <?if(!$is_all_loaded) {?>
                        <div class="card__more">
                            <div class="btn btn_link" data-request="/requests/get_more_projects" data-count="12" data-direction="<?=$this->config->item('subdomain')?>" data-tag="<?=(isset($_GET['tag'])) ? $_GET['tag'] : "";?>">Показать ещё</div>
                        </div>
                    <?}?>
                </div>
            <?}?>
        </div>
    </div>

    <?$this->load->view("cake/emailback", array('emailback_title' => 'Хотите получать рассылку?'));?>

</div>