<div class="website">
    <div class="project">
        <div class="project__header">
            <div class="project__header-bg" style="background-image:url(<?=base_url()?>assets/uploads/projects/<?=$project['id']?>/main.jpg);"></div>
            <div class="inner">
                <div class="breadcrumbs">
                    <div class="breadcrumbs__item"><a href="<?=base_url()?>projects">Проекты</a></div>
                    <div class="breadcrumbs__item"><a href="<?=base_url()?>projects/?tag=<?=$project['tag']['slug']?>"><?=$project['tag']['name']?></a></div>
                    <div class="breadcrumbs__item"><?=$project['name']?></div>
                </div>
                <div class="project__header-title <? if (iconv_strlen($h1) >= 20 && iconv_strlen($h1) < 40 ) { ?> project__header-title_m <? } ?> <? if (iconv_strlen($h1) >= 40 && iconv_strlen($h1) < 70 ) { ?> project__header-title_s <? } ?> <? if (iconv_strlen($h1) >= 70 ) { ?> project__header-title_xs <? } ?>"><?=$h1;?></div>
                <div class="project__header-info">
					<?if(file_exists('./assets/uploads/projects/'.$project['id'].'/logo.jpg')) {?>
                    <div class="project__header-logo"><img src="<?=base_url()?>assets/uploads/projects/<?=$project['id']?>/logo.jpg" alt="Логотип <?=htmlspecialchars($project['name'])?>"></div>
					<?}?>
                </div>
            </div>
        </div>
        <div class="project__content">
            <div class="inner">
                <div class="project__content-info"><span><?=$project['description']?></span></div>
                <div class="project__stats">
                    <?if(!empty($project['area'])) {?>
                        <div class="project__stats-item">
                            <div class="project__stats-title">Площадь помещения:</div>
                            <div class="project__stats-value"><?=number_format($project['area'], 0, '.', ' ');?> м<sup>2</sup></div>
                        </div>
                    <?}?>
					<? if (!empty($project['address'])) {?>
                    <div class="project__stats-item project__stats-item_city">
                        <div class="project__stats-title">Адрес:</div>
                        <div class="project__stats-value"><?=$project['address']?></div>
                    </div>
					<?}?>
					<? if (!empty($project['seats'])) {?>
                    <div class="project__stats-item">
                        <div class="project__stats-title">Посадочных мест</div>
                        <div class="project__stats-value"><?=$project['seats']?></div>
                    </div>
					<?}?>
                </div>
            </div>
            <div class="project__gallery">
                <div class="inner">
                    <div class="project__gallery-list">
                        <?foreach ($project['gallery'] as $item):?>
                            <div class="project__gallery-item">
								<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-lazy="<?=$item['normal']?>" alt="Фото проекта">
							</div>
                        <?endforeach;?>
                    </div>
                    <div class="project__gallery-preview">
                        <?foreach ($project['gallery'] as $item):?>
                            <div>
								<div class="project__gallery-thumbnail" style="background-image: url(<?=$item['thumb']?>);" alt="Фото проекта"></div>
							</div>
                        <?endforeach;?>
                    </div>
                </div>
            </div>
            <?if(!empty($project['brands'])) {?>
            <div class="inner">
                <div class="project__subtitle">Использованное оборудование <br>в этом проекте:</div>
                <div class="project__brands">
                    <?foreach ($project['brands'] as $item):?>
                        <a class="project__brands-item" href="<?=$this->config->item('protocol') . $this->config->item('main_domain')?>brands/<?=$item['slug']?>">
							<img class="greyscale" src="<?=base_url()?>assets/uploads/brands/<?=$item['id']?>_gs.jpg" alt="<?=htmlspecialchars($item['name'])?>">
							<img class="hover" src="<?=base_url()?>assets/uploads/brands/<?=$item['id']?>.jpg" alt="<?=htmlspecialchars($item['name'])?>">
						</a>
                    <?endforeach;?>
                </div>
            </div>
            <?}?>
        </div>
    </div>

    <?$this->load->view("cake/callback", array('callback_title' => 'Понравились работы?'));?>

</div>