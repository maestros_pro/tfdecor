<?foreach ($projects as $item):?>
    <div class="card__item card__item_project"><a class="card__item-inner" href="<?=base_url()?>projects/<?=$item['slug']?>">
            <div class="card__item-image">
				<img class="object-fit" src="<?=base_url()?>assets/uploads/projects/<?=$item['id']?>/main_thumb.jpg" alt="<?=htmlspecialchars($item['name'])?>"/>
			</div>
            <div class="card__item-tag"><?=$item['tag']['name']?></div>
            <div class="card__item-info">
                <div class="card__item-title"><?=$item['name']?></div>
            </div></a></div>
<?endforeach;?>