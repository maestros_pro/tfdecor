<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="theme-color" content="#177dff">
    <meta name="msapplication-TileColor" content="#177dff">
    <meta name="msapplication-navbutton-color" content="#177dff">
    <meta name="apple-mobile-web-app-status-bar-style" content="#177dff">
    <title>'Admin tfdecor.ru'</title>
    <link href="<?=base_url();?>assets/admin/css/style.css" rel="stylesheet"></head>
<body>
<div id="app"></div>
<script src="<?=base_url();?>assets/admin/js/script.js"></script></body>
</html>