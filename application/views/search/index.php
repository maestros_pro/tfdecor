<div class="website">
    <div class="page page_search">
        <div class="inner">
            <div class="search">
                <div class="search__form">
                    <form action="<?=base_url()?>search" method="get">
                        <div class="search__form-input">
                            <input type="text" name="query" value="<?=$_GET['query']?>">
                        </div>
                        <div class="search__form-btn">
                            <button class="btn btn_border" type="submit">Найти</button>
                        </div>
                    </form>
                </div>
                <?if(isset($_GET['query'])) {?><div class="search__text">Результатов по вашему запросу: <?=count($equipment)?></div><?}?>
            </div>
            <div class="card">
                <div class="card__list">
                    <?foreach ($equipment as $item):?>
                        <div class="card__item card__item_equipment"><a class="card__item-inner" href="<?=base_url()?>equipment/<?=$item['slug']?>">
                                <div class="card__item-image">
									<img class="object-fit" src="<?=base_url()?>assets/uploads/equipment/<?=$item['id']?>/image0_thumb.jpg" alt="<?=htmlspecialchars($item['name'])?>"/>
								</div>
                                <div class="card__item-tag"><?=$item['tag']['name']?></div>
                                <div class="card__item-info">
                                    <div class="card__item-title"><?=$item['name']?></div>
                                </div></a></div>
                    <?endforeach;?>
                </div>
            </div>
        </div>
    </div>

    <?$this->load->view("cake/callback", array('callback_title' => 'Хотите узнать цену?'));?>

</div>