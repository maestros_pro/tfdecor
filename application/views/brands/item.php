<?foreach ($brands as $item):?>
    <div class="card__item card__item_brand">
		<a class="card__item-inner" href="<?=base_url()?>brands/<?=$item['slug']?>">
            <div class="card__item-image">
				<img class="greyscale" src="<?=base_url()?>assets/uploads/brands/<?=$item['id']?>_gs.jpg" alt="<?=htmlspecialchars($item['name'])?>"/>
				<img class="hover" src="<?=base_url()?>assets/uploads/brands/<?=$item['id']?>.jpg" alt="<?=htmlspecialchars($item['name'])?>"/>
			</div>
		</a>
	</div>
<?endforeach;?>