<div class="website">
    <div class="article article_brand">
        <div class="inner">
            <div class="breadcrumbs">
                <div class="breadcrumbs__item"><a href="<?=base_url()?>brands">Бренды</a></div>
                <div class="breadcrumbs__item"><?=$brand['name']?></div>
            </div>
            <div class="article__title"><?=$brand['name']?></div>
            <div class="article__brand">
                <div class="article__brand-logo"><img src="<?=base_url()?>assets/uploads/brands/<?=$brand['id']?>.jpg" alt="<?=htmlspecialchars($brand['name'])?>"></div>
                <div class="article__brand-info">
                    <div class="article__brand-text"><?=$brand['description']?></div>
                </div>
            </div>
            <div class="article__title">О бренде</div>
            <div class="article__content article__content_wide">
                <div class="<?if($has_equipment) {?>article__content-half<?}?>">
					<div class="typo">
						<?=$brand['history']?>
					</div>

                </div>
                <?if($has_equipment) {?>
                    <div class="article__content-half">
                        <p class="center"><a class="btn btn_big btn_green" href="<?=base_url()?>equipment/?brand=<?=$brand['slug']?>">Оборудование бренда</a></p>
                    </div>
                <?}?>
            </div>
        </div>
    </div>

    <?$this->load->view("cake/callback", array('callback_title' => 'Хотите узнать цену?'));?>

</div>