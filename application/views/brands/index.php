<div class="website">
    <div class="page page_brands">
        <div class="inner">
            <div class="head">
                <div class="head__title"><?=$h1;?></div>
            </div>
            <?if(!empty($brands)) {?>
            <div class="card">
                <div class="card__list">
                    <?foreach ($brands as $item):?>
                        <div class="card__item card__item_brand">
							<a class="card__item-inner" href="<?=base_url()?>brands/<?=$item['slug']?>">
                                <div class="card__item-image">
									<img class="greyscale" src="<?=base_url()?>assets/uploads/brands/<?=$item['id']?>_gs.jpg" alt="<?=htmlspecialchars($item['name'])?>"/>
									<img class="hover" src="<?=base_url()?>assets/uploads/brands/<?=$item['id']?>.jpg" alt="<?=htmlspecialchars($item['name'])?>"/>
								</div>
							</a>
						</div>
                    <?endforeach;?>
                </div>
                <?if(!$is_all_loaded) {?>
                    <div class="card__more">
                        <div class="btn btn_link" data-request="/requests/get_more_brands" data-count="12" data-tag="<?=(isset($_GET['tag'])) ? $_GET['tag'] : "";?>">Показать еще</div>
                    </div>
                <?}?>
            </div>
            <?}?>
        </div>
    </div>

    <?$this->load->view("cake/callback", array('callback_title' => 'Хотите узнать цену?'));?>

</div>