<div class="website">
    <div class="error">
        <div class="inner">
            <div class="error__info">
                <div class="error__title">Мы не всегда находим<br>то, что ищем.<br>Вот и сейчас кажется<br>как раз такой случай.</div>
                <div class="error__text">Неправильно набран адрес, или такой страницы больше<br>не существует, а возможно никогда и не существовало.</div>
            </div>
			<div class="error__code">404</div>
        </div>
    </div>
</div>