<div class="website">
    <div class="article article_equipment">
        <div class="inner">
            <div class="breadcrumbs">
                <div class="breadcrumbs__item"><a href="<?=base_url()?>equipment">Оборудование</a></div>
                <div class="breadcrumbs__item"><a href="<?=base_url()?>equipment/?brand=<?=$equipment['brand']['slug']?>"><?=$equipment['brand']['name']?></a></div>
                <div class="breadcrumbs__item"><?=$equipment['name']?></div>
            </div>
            <div class="article__title"><?=$h1;?></div>
            <div class="article__equipment">
                <div class="article__equipment-view">
                    <div class="article__equipment-preview">
                        <?$i = 0;?>
                        <?foreach($equipment['images'] as $item):?>
                            <?if($i == 0){?>
                                <div class="article__equipment-thumbnail is-active"><img src="<?=$item['thumb']?>" alt="<?=htmlspecialchars($equipment['name'])?>"></div>
                            <?} else {?>
                                <div class="article__equipment-thumbnail"><img src="<?=$item['thumb']?>" alt="<?=htmlspecialchars($equipment['name'])?>"></div>
                            <?}?>
                        <?$i++;?>
                        <?endforeach;?>
                    </div>
                    <div class="article__equipment-carousel">
                        <?foreach($equipment['images'] as $item):?>
                            <div class="article__equipment-image"><img src="<?=$item['normal']?>" alt="<?=htmlspecialchars($equipment['name'])?>"></div>
                        <?endforeach;?>
                    </div>
                </div>
                <div class="article__equipment-info">
                    <div class="article__equipment-logo"><img src="<?=base_url()?>assets/uploads/brands/<?=$equipment['brand']['id']?>_gs.jpg" alt="<?=htmlspecialchars($equipment['brand']['name'])?>"></div>
                    <div class="article__equipment-text">
                        <div class="article__equipment-title">Производитель:</div>
                        <div class="article__equipment-value"><?=$equipment['brand']['name']?></div>
                        <div class="article__equipment-title">Габаритые размеры:</div>
                        <div class="article__equipment-value"><?=$equipment['feature_dimensions']?></div>
                        <div class="article__equipment-title">Напряжение, В:</div>
                        <div class="article__equipment-value"><?=$equipment['feature_voltage']?></div>
                        <div class="article__equipment-title">Мощность, кВт:</div>
                        <div class="article__equipment-value"><?=$equipment['feature_power']?></div>
                    </div>
                </div>
            </div>
            <div class="article__title">Описание</div>
            <div class="article__content">
				<div class="typo">
					<?=$equipment['description']?>
				</div>

            </div>
        </div>
    </div>

    <?$this->load->view("cake/callback", array('callback_title' => 'Хотите узнать цену?'));?>

</div>