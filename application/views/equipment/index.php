<div class="website">
    <div class="page page_equipment">
        <div class="inner">
            <div class="head">
                <div class="head__title"><?=$h1;?></div>
            </div>
            <?if(!empty($equipment)) {?>
            <div class="card">
                <div class="card__list">
                    <?foreach($equipment as $item):?>
                        <div class="card__item card__item_equipment">
							<a class="card__item-inner" href="<?=base_url()?>equipment/<?=$item['slug']?>">
                                <div class="card__item-image">
									<img class="object-fit" src="<?=base_url()?>assets/uploads/equipment/<?=$item['id']?>/image0_thumb.jpg" alt="<?=htmlspecialchars($item['name'])?>"/>
								</div>
                                <div class="card__item-tag">
									<?//=$item['tag']['name']?>
									<?=$item['brandInfo']['name']?>
								</div>
                                <div class="card__item-info">
                                    <div class="card__item-title"><?=$item['name']?></div>
                                </div></a></div>
                    <?endforeach;?>
                </div>
                <?if(!isset($_GET['brand']) AND !$is_all_loaded){?>
                    <div class="card__more">
                        <div class="btn btn_link" data-request="/requests/get_more_equipment" data-count="12" data-tag="<?=(isset($_GET['tag'])) ? $_GET['tag'] : "";?>">Показать ещё</div>
                    </div>
                <?}?>
            </div>
            <?}?>
        </div>
    </div>

    <?$this->load->view("cake/callback", array('callback_title' => 'Хотите узнать цену?'));?>

</div>