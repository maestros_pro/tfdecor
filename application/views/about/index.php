<div class="website">
	<div class="page page_about">
		<div class="head">
			<div class="inner">
				<div class="head__title"><?=$h1;?></div>
			</div>
		</div>
		<div class="about">
			<div class="about__main">
				<div class="inner">
					<div class="about__main-info">
						<div class="about__title">Мы успешно работаем на&nbsp;рынке индустрии гостеприимства и&nbsp;питания с&nbsp;2003&nbsp;года.</div>
						<div class="about__quote">Основная задача нашей компании&nbsp;– профессиональное оснащение кухонь.</div>
						<div class="about__lisence">
							<div class="about__lisence-item">
								<div class="about__lisence-photo"><img src="<?=base_url()?>assets/img/about/lisence_1.jpg" alt="Свидетельство о допуске к работам по подготовке проектной документации"></div>
								<div class="about__lisence-text">Свидетельство о&nbsp;допуске к&nbsp;работам по&nbsp;подготовке проектной документации: СРО&nbsp;№&nbsp;0159-2012-7814120490-05 от&nbsp;«03»&nbsp;апреля 2012&nbsp;года.</div>
							</div>
							<div class="about__lisence-item">
								<div class="about__lisence-photo"><img src="<?=base_url()?>assets/img/about/lisence_2.jpg" alt="Свидетельство о допуске к пусконаладочным работам"></div>
								<div class="about__lisence-text">Свидетельство о&nbsp;допуске к&nbsp;пусконаладочным работам: СРО&nbsp;№&nbsp;0571.03-2012-7814120490-С-010 от&nbsp;«15»&nbsp;марта 2012&nbsp;года.</div>
							</div>
						</div>
					</div>
					<div class="about__more">
						<div class="about__more-image" style="background-image:url(<?=base_url()?>assets/img/about/bg.jpg)"></div>
						<div class="about__more-text">
							<p>Наша уникальность заключается в&nbsp;использовании разносторонних знаний и&nbsp;навыков, большого опыта реализованных нами проектов, умении применять разнообразныетехнические и&nbsp;технологические решения.</p>
							<p>Всё это помогает нам выполнять поставленные клиентом задачи с&nbsp;максимальной эффективностью и&nbsp;оперативно реагировать на&nbsp;меняющиеся потребности рынка.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="about__activity">
				<div class="inner">
					<div class="about__title">Направления деятельности</div>
					<div class="about__activity-list">
						<div class="about__activity-item">
							<div class="about__activity-ico"><img src="<?=base_url()?>assets/img/about/ico_1.png" alt="Проектирование"></div>
							<div class="about__activity-title">Проектирование</div>
							<div class="about__activity-text">Технологическое проектирование предприятий питания и&nbsp;пищевых производств</div>
						</div>
						<div class="about__activity-item">
							<div class="about__activity-ico"><img src="<?=base_url()?>assets/img/about/ico_2.png" alt="Поставка"></div>
							<div class="about__activity-title">Поставка</div>
							<div class="about__activity-text">Поставка технологического оборудования для&nbsp;профессиональной кухни</div>
						</div>
						<div class="about__activity-item">
							<div class="about__activity-ico"><img src="<?=base_url()?>assets/img/about/ico_3.png" alt="Производство"></div>
							<div class="about__activity-title">Производство</div>
							<div class="about__activity-text">Собственное производство изделий из&nbsp;нержавеющей стали</div>
						</div>
						<div class="about__activity-item">
							<div class="about__activity-ico"><img src="<?=base_url()?>assets/img/about/ico_4.png" alt="Монтаж"></div>
							<div class="about__activity-title">Монтаж</div>
							<div class="about__activity-text">Монтажные и&nbsp;пусконаладочные работы</div>
						</div>
						<div class="about__activity-item">
							<div class="about__activity-ico"><img src="<?=base_url()?>assets/img/about/ico_5.png" alt="Сервисный центр"></div>
							<div class="about__activity-title">Сервисный центр</div>
							<div class="about__activity-text">Гарантийное и&nbsp;постгарантийное сервисное обслуживание</div>
						</div>
						<div class="about__activity-item">
							<div class="about__activity-ico"><img src="<?=base_url()?>assets/img/about/ico_6.png" alt="Склад запасных частей"></div>
							<div class="about__activity-title">Склад запасных частей</div>
							<div class="about__activity-text">Обеспечение необходимыми запчастями из&nbsp;складского запаса</div>
						</div>
						<div class="about__activity-item">
							<div class="about__activity-ico"><img src="<?=base_url()?>assets/img/about/ico_7.png" alt="Дилерская сеть"></div>
							<div class="about__activity-title">Дилерская сеть</div>
							<div class="about__activity-text">Развитие дилерской сети в&nbsp;России и&nbsp;странах СНГ</div>
						</div>
						<div class="about__activity-item">
							<div class="about__activity-ico"><img src="<?=base_url()?>assets/img/about/ico_8.png" alt="Экспертная поддержка"></div>
							<div class="about__activity-title">Экспертная поддержка</div>
							<div class="about__activity-text">Консультации квалифицированных специалистов на&nbsp;всех этапах проекта: от&nbsp;проектирования до сервисного обслуживания</div>
						</div>
						<div class="about__activity-item">
							<div class="about__activity-ico"><img src="<?=base_url()?>assets/img/about/ico_9.png" alt="Обучение"></div>
							<div class="about__activity-title">Обучение</div>
							<div class="about__activity-text">Проведение мастер-классов, бизнес-мероприятий, обучение персонала работе на&nbsp;оборудовании</div>
						</div>
					</div>
				</div>
			</div>
			<div class="about__advantages">
				<div class="inner">
					<div class="about__title">Наши преимущества</div>
					<div class="about__advantages-list">
						<div class="about__advantages-item">Индивидуальный подход, внимание и&nbsp;уважение к&nbsp;потребностям каждого заказчика;</div>
						<div class="about__advantages-item">Сотрудничество с&nbsp;независимыми зарубежными консультантами по проектированию предприятий общественного питания;</div>
						<div class="about__advantages-item">Технологи-проектировщики с&nbsp;многолетним опытом работы;</div>
						<div class="about__advantages-item">Изготовление нестандартного оборудования из&nbsp;нержавеющей стали;</div>
						<div class="about__advantages-item about__advantages-item_img">
							<img src="<?=base_url()?>assets/img/about_adv.jpg" alt="О компании">
						</div>
						<div class="about__advantages-item">Высокий уровень качества поставляемого оборудования и&nbsp;сопутствующих услуг;</div>
						<div class="about__advantages-item">Консультации опытных специалистов и&nbsp;оперативность в&nbsp;обслуживании.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?$this->load->view("cake/callback", array('callback_title' => 'Помочь с выбором?'));?>
</div>
