<!DOCTYPE js>
<html lang="ru">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>ошибка 503</title>
    <meta property="og:title"/>
    <meta property="og:description"/>
    <meta property="og:image"/>
    <meta property="og:url"/>
    <meta name="twitter:title"/>
    <meta name="twitter:description"/>
    <meta name="twitter:image"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <link rel="apple-touch-icon" sizes="180x180" href="<?=base_url()?>assets/img/favicon/apple-touch-icon.png"/>
    <link rel="icon" type="image/png" sizes="32x32" href="<?=base_url()?>assets/img/favicon/favicon-32x32.png"/>
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url()?>assets/img/favicon/favicon-16x16.png"/>
    <meta name="msapplication-TileColor" content="#6cc74d"/>
    <meta name="theme-color" content="#6cc74d"/>
    <link href="<?=base_url()?>assets/css/index.style.css?4375a9f72e8850073937" rel="stylesheet"></head>
<body>
<div class="information">
	<a class="information__logo" href="/"><img src="<?=base_url()?>assets/img/technoflot-logo" alt="Технофлот"/></a>
    <div class="information__content">
        <div class="inner">
            <div class="information__title">ошибка 503</div>
            <div class="information__text">Пока вы&nbsp;читаете это предложение, возможно мы&nbsp;всё уже починили. Попробуйте нажать кнопку ниже, вдруг всё уже работает</div>
            <div class="information__btn"><a class="btn btn_green btn_big" href="#" onclick="location.reload()">Перезагрузить страницу</a></div>
        </div>
    </div>
</div>
<script src="<?=base_url()?>assets/js/index.js?4375a9f72e8850073937"></script></body>
</html>