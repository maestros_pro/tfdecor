<div class="website">
    <div class="page page_news">
        <div class="inner">
            <div class="head">
                <div class="head__title"><?=$h1;?></div>
            </div>
            <?if(!empty($news)) {?>
            <div class="card">
                <div class="card__list">
                    <?$i = 0;?>
                    <?foreach ($news as $item):?>
                        <?if($i == 0) {?>
                            <div class="card__item card__item_news card__item_big"><a class="card__item-inner" href="<?=base_url()?>news/<?=$item['slug']?>">
                                <div class="card__item-image">
									<img class="object-fit" src="<?=base_url()?>assets/uploads/news/<?=$item['id']?>/main_thumb.jpg" alt="<?=htmlspecialchars($item['name'])?>"/>
								</div>
                                <div class="card__item-tag"><?=$item['tag']['name']?></div>
                                <div class="card__item-info">
                                    <div class="card__item-date">
                                        <div class="card__item-date-day"><?=$item['day']?></div>
                                        <div class="card__item-date-month"><?=$item['month']?></div>
                                        <div class="card__item-date-year"><?=$item['year']?></div>
                                    </div>
                                    <div class="card__item-title"><?=$item['name']?></div>
                                    <div class="card__item-text"><?=$item['announce']?></div>
                                </div></a>
                            </div>
                        <?} else {?>
                            <div class="card__item card__item_news">
                                <a class="card__item-inner" href="<?=base_url()?>news/<?=$item['slug']?>">
                                    <div class="card__item-image">
										<img class="object-fit" src="<?=base_url()?>assets/uploads/news/<?=$item['id']?>/main_thumb.jpg" alt="<?=htmlspecialchars($item['name'])?>"/>
									</div>
                                    <div class="card__item-tag"><?=$item['tag']['name']?></div>
                                    <div class="card__item-info">
                                        <div class="card__item-date"><?=date('d.m.Y', strtotime($item['dt']))?></div>
                                        <div class="card__item-title"><?=$item['name']?></div>
                                    </div></a></div>
                        <?}?>
                    <?$i++;?>
                    <?endforeach;?>
                </div>
                <?if(!$is_all_loaded) {?>
                    <div class="card__more">
                        <div class="btn btn_link" data-request="/requests/get_more_news" data-tag="<?=(isset($_GET['tag'])) ? $_GET['tag'] : "";?>">Показать ещё</div>
                    </div>
                <?}?>
            </div>
            <?}?>
        </div>
    </div>

    <?$this->load->view("cake/emailback", array('emailback_title' => 'Хотите получать рассылку?'));?>

</div>