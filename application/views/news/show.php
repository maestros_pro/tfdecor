<div class="website">
    <div class="article article_news">
        <div class="inner">
            <div class="breadcrumbs">
                <div class="breadcrumbs__item"><a href="<?=base_url()?>news">Новости</a></div>
                <div class="breadcrumbs__item"><a href="<?=base_url()?>news/?tag=<?=$new['tag']['slug']?>"><?=$new['tag']['name']?></a></div>
                <div class="breadcrumbs__item"><?=$new['name']?></div>
            </div>
            <div class="article__title"><?=$h1;?></div>
            <div class="article__kv">
                <div class="article__date">
                    <div class="article__date-col">
                        <div class="article__date-day"><?=$new['day']?></div>
                    </div>
                    <div class="article__date-col">
                        <div class="article__date-month"><?=$new['month']?></div>
                        <div class="article__date-year"><?=$new['year']?></div>
                    </div>
                </div>
				<img src="<?=base_url()?>assets/uploads/news/<?=$new['id']?>/main.jpg" alt="<?=htmlspecialchars($new['name'])?>">
            </div>
            <div class="article__announce"><?=$new['announce']?></div>
            <div class="article__content">
				<div class="typo">
					<?=$new['description']?>
				</div>

                <div class="article__carousel">
                    <?foreach ($new['gallery'] as $item):?>
					<div>
                        <div class="article__carousel-item" data-src="<?=$item['normal']?>" style="background-image: url(<?=$item['thumb']?>);">
<!--							<img src="--><?//=$item['thumb']?><!--">-->
							<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="Фото новости">
						</div>
					</div>
                    <?endforeach;?>
                </div>
            </div>
        </div>
    </div>
	<? if (count($random) > 0) {?>
    <div class="news">
        <div class="inner">
            <div class="news__head">
                <div class="news__title">Другие наши новости:</div>
            </div>
            <div class="card">
                <div class="card__list">
                    <?foreach ($random as $item):?>
                        <div class="card__item card__item_news">
                            <a class="card__item-inner" href="<?=base_url()?>news/<?=$item['slug']?>">
                                <div class="card__item-image">
									<img class="object-fit" src="<?=base_url()?>assets/uploads/news/<?=$item['id']?>/main_thumb.jpg" alt="<?=htmlspecialchars($item['name'])?>"/>
								</div>
                                <div class="card__item-tag"><?=$item['tag']['name']?></div>
                                <div class="card__item-info">
                                    <div class="card__item-date"><?=date('d.m.Y', strtotime($item['dt']))?></div>
                                    <div class="card__item-title"><?=$item['name']?></div>
                                </div></a></div>
                    <?endforeach;?>
                </div>
            </div>
        </div>
    </div>
	<? } ?>

    <?$this->load->view("cake/emailback", array('emailback_title' => 'Хотите получать рассылку?'));?>

</div>