<?foreach ($news as $item):?>
<div class="card__item card__item_news">
    <a class="card__item-inner" href="<?=base_url()?>news/<?=$item['slug']?>">
        <div class="card__item-image">
			<img class="object-fit" src="<?=base_url()?>assets/uploads/news/<?=$item['id']?>/main_thumb.jpg" alt="<?=htmlspecialchars($item['name'])?>"/>
		</div>
        <div class="card__item-tag"><?=$item['tag']['name']?></div>
        <div class="card__item-info">
            <div class="card__item-date"><?=date('d.m.Y', strtotime($item['dt']))?></div>
            <div class="card__item-title"><?=$item['name']?></div>
        </div></a></div>
<?endforeach;?>