<pre style="display: none;"><?= $part = $this->config->item('subdomain')?></pre>

<div class="website">
    <div class="welcome welcome_part welcome_<?=$part?>">
		<div class="welcome__bg"></div>
        <div class="welcome__info">
            <div class="inner">
                <div class="welcome__info-title">
					<?=$h1;?>
                </div>

				<div class="welcome__info-menu">
					<a class="welcome__info-menu-item" href="<?=$this->config->item('protocol') . $this->config->item('main_domain')?><?if ($show_eq_link) {?>equipment<? } else { ?>brands<?}?>">Каталог оборудования</a>
					<a class="welcome__info-menu-item" href="<?=$this->config->item('protocol') . $this->config->item('main_domain')?>news">Новости</a>
				</div>

                <div class="welcome__info-part">

					<? if ($part == 'kitchen') {?>
						<div class="welcome__info-part-text">Правильно спроектированная кухня&nbsp;–не&nbsp;просто место для&nbsp;приготовления пищи, а&nbsp;платформа, на&nbsp;базе которой воплощаются в&nbsp;жизнь крупные кулинарные проекты.</div>
					<? } elseif ($part == 'bakery') {?>
						<div class="welcome__info-part-text">Чего только не&nbsp;вылечат булочки из&nbsp;угловой пекарни…<br>Карлос Руис Сафон «Игра&nbsp;ангела».</div>
					<? } elseif ($part == 'hotel') {?>
						<div class="welcome__info-part-text">Отель задает стиль ресторана, а&nbsp;под этот стиль оборудует технологический процесс и&nbsp;набирает поставщиков.</div>
					<? } elseif ($part == 'retail') {?>
						<div class="welcome__info-part-text">Максимальная отдача прибыли с&nbsp;каждого метра производственного и&nbsp;торгового пространства</div>
					<? } elseif ($part == 'flot') {?>
						<div class="welcome__info-part-text">Максимальная отдача прибыли с&nbsp;каждого метра производственного и&nbsp;торгового пространства</div>
					<? }?>
                </div>
            </div>
        </div>
    </div>
    <div class="projects projects_part">
        <div class="inner">
            <div class="projects__title">Наши проекты</div>
            <?if(!empty($p1)) {?>
            <div class="card__list projects__list projects__list_1">
                <?foreach ($p1 as $item):?>
                    <div class="card__item card__item_project"><a class="card__item-inner" href="<?=base_url()?>projects/<?=$item['slug']?>" data-animate-fade>
                            <div class="card__item-image">
								<img class="object-fit" src="<?=base_url()?>assets/uploads/projects/<?=$item['id']?>/main_thumb.jpg" alt="<?=htmlspecialchars($item['name'])?>">
							</div>
                            <div class="card__item-tag"><?=$item['tag']['name']?></div>
                            <div class="card__item-info">
                                <div class="card__item-title"><?=$item['name']?></div>
                            </div></a></div>
                <?endforeach;?>
            </div>
            <?}?>
        </div>
        <div class="projects__insert projects__insert_kitchen">
            <div class="inner">
                <div class="projects__insert-text">Мы&nbsp;оказываем полный комплекс услуг по&nbsp;оснащению пекарен, кондитерских, пиццерий, и&nbsp;прочего.</div>
            </div>
            <div class="projects__insert-image" data-animate-class="is-animate">
				<img src="<?=base_url()?>assets/img/partition/insert-<?=$this->config->item('subdomain')?>.png" alt="<?=$this->config->item('subdomain')?>">
			</div>
        </div>
        <div class="inner">
            <?if(!empty($p2)) {?>
            <div class="card__list projects__list projects__list_2">
                <?foreach ($p2 as $item):?>
                    <div class="card__item card__item_project"><a class="card__item-inner" href="<?=base_url()?>projects/<?=$item['slug']?>" data-animate-fade>
                            <div class="card__item-image">
								<img class="object-fit" src="<?=base_url()?>assets/uploads/projects/<?=$item['id']?>/main_thumb.jpg" alt="<?=htmlspecialchars($item['name'])?>">
							</div>
                            <div class="card__item-tag"><?=$item['tag']['name']?></div>
                            <div class="card__item-info">
                                <div class="card__item-title"><?=$item['name']?></div>
                            </div></a></div>
                <?endforeach;?>
            </div>
            <?}?>
            <div class="card__more"><a class="btn btn_link" href="<?=base_url()?>projects">Показать все</a></div>
        </div>
    </div>

    <?$this->load->view("cake/callback", array('callback_title' => 'Понравились работы?'));?>

</div>