<div class="website">
    <div class="welcome welcome_main">
		<div class="welcome__textline">
			<img src="<?=base_url()?>assets//img/technoflot-text.svg" alt="technoflot">
		</div>
        <?if(!empty($carousel)){?>
        <div class="welcome__carousel">
            <div class="welcome__carousel-list">
                <?foreach($carousel as $item):?>
                    <div class="welcome__carousel-item">
                        <div class="welcome__carousel-image _backgroundimage" style="background-image: url(<?=$item['thumbnail']?>);">
							<img data-lazy="<?=$item['background']?>" src="<?=$item['thumbnail']?>" alt="<?=htmlspecialchars($item['name'])?>"/>
						</div>
                        <div class="welcome__carousel-info">
                            <div class="welcome__carousel-count"><?=$item['counter']?></div>
                            <div class="welcome__carousel-title"><a href="<?=$item['link']?>"><?=$item['name']?></a></div>
                            <? if (!empty($item['address'])) {?>
								<div class="welcome__carousel-placement"><?=$item['address']?></div>
							<? }?>
                        </div>

						<? if (!empty($item['next'])) {?>
                        <div class="welcome__carousel-preview _gonext">
                            <div class="welcome__carousel-thumbnail _backgroundimage">
								<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-lazy="<?=$item['next']?>" alt="Следующий проект"/>
							</div>
                        </div>
						<? }?>
                    </div>
                <?endforeach;?>
            </div>
            <div class="welcome__carousel-control">
                <div class="btn welcome__carousel-btn welcome__carousel-btn_prev _goprev">Предыдущий проект</div>
                <div class="btn welcome__carousel-btn welcome__carousel-btn_next _gonext">Следующий проект</div>
            </div>
        </div>
        <?}?>
        <div class="welcome__info">
            <div class="inner">
                <div class="welcome__info-title">Проектируем </div>
                <div class="welcome__info-text">
                    <div class="ribbon">
                        <div class="ribbon__content">кулинарные<br>пространства <span>16</span> лет</div>
                    </div>
                </div>
                <div class="welcome__info-more"><a href="<?=base_url()?>contact">узнайте о нас больше</a></div>
            </div>
        </div>
    </div>
    <div class="partition">
        <div class="inner">
            <div class="partition__catalog">
                <div class="partition__catalog-title">
					<a href="<?=$this->config->item('protocol') . $this->config->item('main_domain')?>brands">
						Технологическое оборудование для&nbsp;профессиональной кухни
					</a>
				</div>

                <div class="partition__catalog-list">
                    <div class="partition__catalog-group">
                        <div class="partition__catalog-item partition__catalog-item_kitchen">
							<a class="partition__catalog-inner" <?if($menu_show['kitchen']) {?> href="<?=$this->config->item('protocol') . "kitchen." . $this->config->item('main_domain')?>" <? } ?> data-animate-fade>
                                <div class="partition__catalog-img">
								</div>
                                <div class="partition__catalog-info">
                                    <div class="partition__catalog-name">Рестораны</div>
                                    <div class="partition__catalog-text">Правильно спроектированная кухня&nbsp;– не&nbsp;просто место для&nbsp;приготовления пищи, а&nbsp;платформа, на&nbsp;базе которой воплощаются в&nbsp;жизнь крупные кулинарные проекты.</div>
                                </div>
							</a>
						</div>
                        <div class="partition__catalog-item partition__catalog-item_bakery">
							<a class="partition__catalog-inner" <?if($menu_show['bakery']) {?> href="<?=$this->config->item('protocol') . "bakery." . $this->config->item('main_domain')?>" <? } ?> data-animate-fade>
                                <div class="partition__catalog-img"></div>
                                <div class="partition__catalog-info">
                                    <div class="partition__catalog-name">Пекарни</div>
                                    <div class="partition__catalog-text"> Чего только не&nbsp;вылечат булочки из&nbsp;угловой пекарни…<br> Карлос Руис Сафон «Игра ангела».</div>
                                </div>
							</a>
						</div>
                    </div>
                    <div class="partition__catalog-group">
                        <div class="partition__catalog-item partition__catalog-item_hotel">
							<a class="partition__catalog-inner" <?if($menu_show['hotel']) {?> href="<?=$this->config->item('protocol') . "hotel." . $this->config->item('main_domain')?>" <? } ?> data-animate-fade>
                                <div class="partition__catalog-img"></div>
                                <div class="partition__catalog-info">
                                    <div class="partition__catalog-name">Отели</div>
                                    <div class="partition__catalog-text">Отель задает стиль ресторана, а&nbsp;под этот стиль оборудует технологический процесс и&nbsp;набирает поставщиков.</div>
                                </div>
							</a>
						</div>
                        <div class="partition__catalog-item partition__catalog-item_retail">
							<a class="partition__catalog-inner" <?if($menu_show['retail']) {?> href="<?=$this->config->item('protocol') . "retail." . $this->config->item('main_domain')?>" <? } ?> data-animate-fade>
                                <div class="partition__catalog-img"></div>
                                <div class="partition__catalog-info">
                                    <div class="partition__catalog-name">Ритейл</div>
                                    <div class="partition__catalog-text"></div>
                                </div>
							</a>
						</div>
                        <div class="partition__catalog-item partition__catalog-item_flot">
							<a class="partition__catalog-inner" <?if($menu_show['flot']) {?> href="<?=$this->config->item('protocol') . "flot." . $this->config->item('main_domain')?>" <? } ?> data-animate-fade data-animate-delay="300">
                                <div class="partition__catalog-img"></div>
                                <div class="partition__catalog-info">
                                    <div class="partition__catalog-name">Корабли</div>
                                    <div class="partition__catalog-text"></div>
                                </div>
							</a>
						</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="partition__complex">
            <div class="inner">
                <div class="partition__complex-title">Мы оказываем полный комплекс услуг<br>по оснащению предприятий<br>общественного питания.</div>
                <div class="partition__complex-list">
                    <div class="partition__complex-item">проектирование предприятий питания и&nbsp;пищевых производств</div>
                    <div class="partition__complex-item">поставка технологического оборудования для&nbsp;профессиональной кухни</div>
                    <div class="partition__complex-item">производство изделий из&nbsp;нержавеющей стали, монтаж и&nbsp;обслуживание</div>
                    <div class="partition__complex-item">обеспечение необходимыми запчастями из&nbsp;складского запаса</div>
                    <div class="partition__complex-item">обучение, консультации и&nbsp;проведение мастер&#8209;классов</div>
                </div>
            </div>
        </div>
        <div class="partition__statictic">
            <div class="partition__statictic-image" style="background-image:url(<?=base_url()?>assets/img/statistic.jpg);"></div>
            <div class="partition__statictic-info">
                <div class="inner">
                    <div class="partition__statictic-title">Технофлот</div>
                    <div class="partition__statictic-subtitle">сегодня – это:</div>
                    <div class="partition__statictic-list">
                        <div class="partition__statictic-item">
                            <div class="partition__statictic-value">><span data-animate-num="150">0</span></div>
                            <div class="partition__statictic-text">ведущих мировых производителей</div>
                        </div>
                        <div class="partition__statictic-item">
                            <div class="partition__statictic-value">><span data-animate-num="200" data-animate-delay="300">0</span></div>
                            <div class="partition__statictic-text">дилеров в&nbsp;России и&nbsp;странах&nbsp;СНГ</div>
                        </div>
                        <div class="partition__statictic-item">
                            <div class="partition__statictic-value">><span data-animate-num="200">0</span></div>
                            <div class="partition__statictic-text">сотрудников в&nbsp;штате</div>
                        </div>
                        <div class="partition__statictic-item">
                            <div class="partition__statictic-value">><span data-animate-num="4000" data-animate-delay="300">0</span></div>
                            <div class="partition__statictic-text">оснащённых объектов</div>
                        </div>
                        <div class="partition__statictic-item">
                            <div class="partition__statictic-value"><span data-animate-num="4500">0</span> м<sup>2</sup></div>
                            <div class="partition__statictic-text">складских помещений в&nbsp;Санкт&#8209;Петербурге и&nbsp;Москве</div>
                        </div>
                    </div>
                    <div class="partition__statictic-btn">
                        <div class="btn btn_green" data-popup-open=".popup_feedback">оставить заявку</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?if(!empty($brands)){?>
    <div class="partners">
        <div class="partners__list">
            <?foreach ($brands as $item):?>

                <div class="partners__item">
					<span class="partners__item-inner">
						<a class="partners__logo" href="<?=base_url()?>brands/<?=$item['slug']?>">
							<img class="partners__logo-image" src="<?=base_url()?>assets/uploads/brands/<?=$item['id']?>_gs.jpg" alt="<?=htmlspecialchars($item['name'])?>">
							<img class="partners__logo-hover" src="<?=base_url()?>assets/uploads/brands/<?=$item['id']?>.jpg" alt="<?=htmlspecialchars($item['name'])?>">
						</a>
					</span>
				</div>
            <?endforeach;?>
        </div>
    </div>
    <?}?>
    <?if(!empty($news)){?>
    <div class="news news_home">
        <div class="inner">
            <div class="news__head">
                <div class="news__title">Новости</div>
                <div class="news__control">
                    <div class="news__control-more"><a href="<?=base_url()?>news">Архив новостей</a></div>
                </div>
            </div>
            <div class="card">
                <div class="card__list">
                    <?$i = 0;?>
                    <?foreach ($news as $item):?>

                    <?if($i == 0){?>
                            <div class="card__item card__item_news card__item_big"><a class="card__item-inner" href="<?=base_url()?>news/<?=$item['slug']?>">
                                    <div class="card__item-image">
										<img class="object-fit" src="<?=base_url()?>assets/uploads/news/<?=$item['id']?>/main_thumb.jpg" alt="<?=htmlspecialchars($item['name'])?>"/>
									</div>
                                    <div class="card__item-tag"><?=$item['tag']['name']?></div>
                                    <div class="card__item-info">
                                        <div class="card__item-date">
                                            <div class="card__item-date-day"><?=$item['day']?></div>
                                            <div class="card__item-date-month"><?=$item['month']?></div>
                                            <div class="card__item-date-year"><?=$item['year']?></div>
                                        </div>
                                        <div class="card__item-title"><?=$item['name']?></div>
                                        <div class="card__item-text"><?=$item['announce']?></div>
                                    </div></a></div>
                    <?} else {?>
                            <div class="card__item card__item_news"><a class="card__item-inner" href="<?=base_url()?>news/<?=$item['slug']?>">
                                    <div class="card__item-image">
										<img class="object-fit" src="<?=base_url()?>assets/uploads/news/<?=$item['id']?>/main_thumb.jpg" alt="<?=htmlspecialchars($item['name'])?>"/>
									</div>
                                    <div class="card__item-tag"><?=$item['tag']['name']?></div>
                                    <div class="card__item-info">
                                        <div class="card__item-date"><?=$item['date']?></div>
                                        <div class="card__item-title"><?=$item['name']?></div>
                                    </div></a></div>
                    <?}?>

                    <?$i++;?>
                    <?endforeach;?>
                </div>
            </div>
        </div>
    </div>
    <?}?>

    <?$this->load->view("cake/callback", array('callback_title' => 'Помочь с выбором?'));?>

</div>